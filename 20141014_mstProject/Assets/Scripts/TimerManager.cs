﻿//---------------------------------------------------------------------------------------
// GameシーンのTimer関連処理
// 作成者：kasai tatushi
// 最終更新：2014/10/20
//---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------
// ライブラリ宣言
//--------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

//==========================================================================
// TimerManagerクラス
//==========================================================================
public class TimerManager : MonoBehaviour {

    //----------------------------------
    // 定数
    //----------------------------------
    public const float TimeLimit = 600.0f;

    // タイマー格納(現段階では600秒で遷移)
    public float m_fTime = 0;

    // フェードオブジェクト格納用
    private GameObject Fade;

    //タイムスプライト関係
    public GameObject TimeObj;      //時間生成の素
    public GameObject CameraObj;    //生成した時間スプライトを置いておく場所
    private SpriteRenderer[] NumberSprite = new SpriteRenderer[10]; //数字スプライト
    private SpriteRenderer[] SpriteTime = new SpriteRenderer[3];    //表示スプライト

    //=====================================
	// 初期化
    //=====================================
	void Start () 
    {
        // 時間の初期化
        m_fTime = TimeLimit;

        // フェードオブジェクト取得
        Fade = GameObject.Find("Fade");

        for (int i = 0; i < 10;i++ )
        {
            Vector3 pos = new Vector3(0, 0, 10);
            GameObject obj = Instantiate(TimeObj) as GameObject;

            // 子に設定する
            obj.transform.parent = transform;
            obj.transform.localPosition = pos;
            obj.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            obj.transform.name = "time_" + i;

            CharName sc = obj.GetComponent<CharName>();
            NumberSprite[i] = sc.SetNumber(i);
        }

        int nTime = (int)m_fTime;
        for (int i = 2; i >=0; i--)
        {
            Vector3 pos = new Vector3(6.0f + (float)i, 4.5f, 3);
            GameObject obj = Instantiate(TimeObj) as GameObject;

            // 子に設定する
            obj.transform.parent = CameraObj.transform;
            obj.transform.localPosition = pos;
            obj.transform.localScale = new Vector3(3.0f, 3.0f, 1);
            obj.transform.name = "time_" + i;

            CharName sc = obj.GetComponent<CharName>();
            SpriteTime[i] = sc.SetNumber(nTime%10);
            nTime /= 10;
        }
	}

    //=====================================	
	// 更新
    //=====================================
	void Update () 
    {
        // 60フレームに達したら時間を減らす
        // Time.frameCountでフレーム数取得
        if (m_fTime >= 0.0f)
        {
            m_fTime -= Time.deltaTime;

            // 時間が0以下になったら遷移
            if (m_fTime <= 0.0f)
            {
                Fade.GetComponent<FadeControll>().StartChangeScene("Result");
            }


            //タイム表示
            DrawTime();
         }
	}

    //=====================================
    // 描画
    //=====================================
    /*void OnGUI()
    {
        TimerLabel.text = "Time";
        Timer.text = m_fTime.ToString();
    }*/

    //=====================================	
    // 時間スプライト更新
    //=====================================
    private void DrawTime()
    {
        int nTime = (int)m_fTime;
        for (int i = 2; i >= 0; i--)
        {
            SpriteTime[i].sprite = NumberSprite[nTime % 10].sprite;
            nTime /= 10;
        }
    }
}
