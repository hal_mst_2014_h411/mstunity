﻿using UnityEngine;

public class FadeControll : MonoBehaviour {
	
	public const int STATE_NONE = 0;
	public const int STATE_IN = 1;
	public const int STATE_OUT = 2;
	static private float DefaultTime = 1.0f;

	public SpriteRenderer sprite;
	private float addAlpha;
	public int state;
	public float fadeTime = DefaultTime;

	private string changeScene;
	private int changeType = 0;
	
	public void StartChangeScene(string str){
		if(state == STATE_NONE){
			changeScene = str;
			state = STATE_OUT;
			changeType = 0;
		}
	}

	public void StartChangeSceneAdd(string str){
		if(state == STATE_NONE){
			changeScene = str;
			state = STATE_OUT;
			changeType = 1;
		}
	}
	
	void Start(){
		Color c = sprite.color;
		c.a = 1.0f;
		sprite.color = c;
		state = STATE_IN;
		addAlpha = fadeTime / Time.deltaTime;
	}
	
	void Update(){
		switch(state){
		case STATE_NONE:
			break;
			
		case STATE_IN:
			sprite.color -= new Color(0.0f, 0.0f, 0.0f, Time.deltaTime / fadeTime);
			if(sprite.color.a <= 0.0f){
				sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, 0.0f);
				state = STATE_NONE;
			}
			break;
			
		case STATE_OUT:
			sprite.color += new Color(0.0f, 0.0f, 0.0f, Time.deltaTime / fadeTime);
			if(sprite.color.a >= 1.0f){
				sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, 1.0f);
				state = STATE_IN;
				if(changeType == 0) {
					Application.LoadLevel(changeScene);
				}
				else {
					Application.LoadLevelAdditive(changeScene);
				}
			}
			break;
		}

		// デバッグ用(F1)
		if(Input.GetKeyDown(KeyCode.F1)){
			StartChangeScene("Title");
		}
	}
}

