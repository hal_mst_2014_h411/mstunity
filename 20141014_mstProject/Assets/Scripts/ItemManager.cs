﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemManager : MonoBehaviour {

	static public int ItemMax = 3;

	public int[] itemNumArray = new int[ItemMax];	//	アイテムの個数の配列

	private int StartItemNum = 50;

	// Use this for initialization
	void Start () {
		for(int i = 0; i < ItemMax; i++){
			itemNumArray[i] = StartItemNum;
		}
	}
}
