﻿//---------------------------------------------------------------------------------------
// 
// 作成者：
// 最終更新：
//---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------
// ライブラリ宣言
//--------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using Collections;

struct RankingData
{
    public string sName;
    public int[] aName;
    public int nScore;
    public int[] aScore;
}

//==========================================================================
// RankingControllerクラス
//==========================================================================
public class RankingControll : MonoBehaviour {

    //定数
    private const float INIT_SPRITE_POS_X = -6.0f;  //名前表示初期
    private const float INIT_SPRITE_POS_Y = 3.0f;   //名前表示初期
    public const int MAX_SCORENUM = 10;             //スコア最大桁
    private const int MAX_RANKING_NUM = 10;         //ランキング表示人数

    //変数
    public FadeControll fade;                       //フェード
    public GameObject cameraObject;                 //オブジェクトの格納場所
    public GameObject mojiObject;                   //オブジェクトの生成素
    public static int m_nRankIn = -1;               //ランクインした人の順位
    private LinkedList<SpriteRenderer> m_atagRankIn = new LinkedList<SpriteRenderer>(); //点滅するオブジェを格納するリスト
    private GameObject m_goBG;                      //BGオブジェ
    private Vector2 m_vecBgPos;                     //BGオブジェのテクスチャ座標
    private RankingData[] m_atagData = new RankingData[MAX_RANKING_NUM];    //ランキングデータ格納
    private bool[] m_bFade = new bool[2];           //点滅処理用フラグ 0がランクインフェードフラグ　1がEnterプッシュフェードフラグ
    public SpriteRenderer m_srEnter;
	public AudioSource audiosource;
	public VirtualInput input;

    //=====================================
	// Use this for initialization
    //=====================================
	void Start () {
        //データロード
        for (int j = 0; j < MAX_RANKING_NUM; j++)
        {
            m_atagData[j].nScore = -1;
        }
        // StreamReader の新しいインスタンスを生成する
        System.IO.StreamReader cReader = (
            new System.IO.StreamReader(@"ranking.csv", System.Text.Encoding.Default)
        );

        // 読み込んだ結果をすべて格納するための変数を宣言する
        string stResult = string.Empty;

        // 読み込みできる文字がなくなるまで繰り返す
        int i = 0;
        while (cReader.Peek() >= 0)
        {
            // ファイルを 1 行ずつ読み込む
            string[] stBuffer = cReader.ReadLine().Split(',');
            // 読み込んだものを
            m_atagData[i].sName = stBuffer[0];
            m_atagData[i].nScore = int.Parse(stBuffer[1]);
            //名前（画像用）
            m_atagData[i].aName = new int[RankInControll.MAX_NAME];
            for (int j = 0; j < RankInControll.MAX_NAME; j++)
            {
                m_atagData[i].aName[j] = int.Parse(stBuffer[j+2]);
            }

            //スコア（画像用）
            m_atagData[i].aScore = new int[MAX_SCORENUM];
            for (int j = 0; j < MAX_SCORENUM; j++)
            {
                m_atagData[i].aScore[j] = int.Parse(stBuffer[j + RankInControll.MAX_NAME + 2]);
            }
            i++;
        }

        // cReader を閉じる (正しくは オブジェクトの破棄を保証する を参照)
        cReader.Close();


        //画像表示
        //文字
        Vector3 pos = new Vector3(INIT_SPRITE_POS_X, INIT_SPRITE_POS_Y, 0);
        for (int j = 0; j < 10; j++)
        {
            int nNum = 0;
            if(j == 0)
            {
                pos.x = INIT_SPRITE_POS_X;
            }
            else if (j == 1 || j == 2)
            {
                pos.x = INIT_SPRITE_POS_X-0.125f;
            }
            else
            {
                pos.x = INIT_SPRITE_POS_X-0.25f;
            }

            for (nNum = 0; nNum < RankInControll.MAX_NAME; nNum++)
            {
                GameObject obj = Instantiate(mojiObject) as GameObject;
                
                // 子に設定する
                obj.transform.parent = cameraObject.transform;
                obj.transform.localPosition = pos;
                if (j == 0)
                {
                    obj.transform.localScale = new Vector3(3, 3, 3);
                }
                else if (j == 1 || j == 2)
                {
                    obj.transform.localScale = new Vector3(2.5f, 2.5f, 2.5f);
                }
                else
                {
                    obj.transform.localScale = new Vector3(2,2,2);
                }
                    
                obj.transform.name = "name_" + j + "_" + nNum;
                CharName sc = obj.GetComponent<CharName>();
                if (m_atagData[j].aName[nNum] != -1)
                {
                    if(j == m_nRankIn)
                    {
                        m_atagRankIn.InsertLast(sc.SetChar(m_atagData[j].aName[nNum]));
                    }
                    else
                    {
                        sc.SetChar(m_atagData[j].aName[nNum]);
                    }
                }

                //位置をずらす
                if (j == 0)
                {
                    pos.x += 0.9f;
                }
                else if (j == 1 || j == 2)
                {
                    pos.x += (0.9f*(2.5f/3.0f));
                }
                else
                {
                    pos.x += 0.6f;
                }
            }

            //数字
            pos.x = 8.0f;
            bool bDollFlag = false;
            for (int k = MAX_SCORENUM-1; k >= 0; k--)
            {
                if (m_atagData[j].aScore[k] != -1)
                {
                    GameObject obj = Instantiate(mojiObject) as GameObject;

                    // 子に設定する
                    obj.transform.parent = cameraObject.transform;
                    obj.transform.localPosition = pos;
                    if (j == 0)
                    {
                        obj.transform.localScale = new Vector3(3, 3, 3);
                    }
                    else if (j == 1 || j == 2)
                    {
                        obj.transform.localScale = new Vector3(2.5f, 2.5f, 2.5f);
                    }
                    else
                    {
                        obj.transform.localScale = new Vector3(2, 2, 2);
                    }
                    obj.transform.name = "score_" + j + "_" + nNum;

                    CharName sc = obj.GetComponent<CharName>();
                   
                    //ランクインした場合・・・
                    if (j == m_nRankIn)
                    {
                        m_atagRankIn.InsertLast(sc.SetNumber(m_atagData[j].aScore[k]));
                    }
                    else
                    {
                        sc.SetNumber(m_atagData[j].aScore[k]);
                    }
                   
                    //nNum++;

                    //コンマ付ける処理
                    if (k % 3 == 0 && k != 9)
                    {
                        //コンマを付ける
                        GameObject conmaObj = Instantiate(mojiObject) as GameObject;

                        Vector3 conmaPos = pos;
                        if (j == 0)
                        {
                            conmaPos.x += 0.6f;
                            conmaPos.y -= 0.1f;
                        }
                        else if (j == 1 || j == 2)
                        {
                            conmaPos.x += 0.45f;
                            conmaPos.y -= 0.1f;
                        }
                        else
                        {
                            conmaPos.x += 0.35f;
                            conmaPos.y -= 0.1f;
                        }
                        // 子に設定する
                        conmaObj.transform.parent = cameraObject.transform;
                        conmaObj.transform.localPosition = conmaPos;
                        if (j == 0)
                        {
                            conmaObj.transform.localScale = new Vector3(3, 3, 3);
                        }
                        else if (j == 1 || j == 2)
                        {
                            conmaObj.transform.localScale = new Vector3(2.5f, 2.5f, 2.5f);
                        }
                        else
                        {
                            conmaObj.transform.localScale = new Vector3(2, 2, 2);
                        }
                        conmaObj.transform.name = "konma_" + j + "_" + k;
                        CharName conma = conmaObj.GetComponent<CharName>();
                        if (j == m_nRankIn)
                        {
                            m_atagRankIn.InsertLast(conma.SetChar(28));
                        }
                        else
                        {
                            conma.SetChar(28);
                        }
                        
                        
                    }
                }
                else if (!bDollFlag)
                {
                    //ドルマーク付ける
                    GameObject obj = Instantiate(mojiObject) as GameObject;

                    // 子に設定する
                    obj.transform.parent = cameraObject.transform;
                    obj.transform.localPosition = pos;
                    if (j == 0)
                    {
                        obj.transform.localScale = new Vector3(3, 3, 3);
                    }
                    else if (j == 1 || j == 2)
                    {
                        obj.transform.localScale = new Vector3(2.5f, 2.5f, 2.5f);
                    }
                    else
                    {
                        obj.transform.localScale = new Vector3(2, 2, 2);
                    }
                    obj.transform.name = "doll_" + j + "_" + nNum;

                    CharName sc = obj.GetComponent<CharName>();

                    //ランクインした場合・・・
                    if (j == m_nRankIn)
                    {
                        SpriteRenderer sr = sc.SetChar(27);
                        m_atagRankIn.InsertLast(sr);
                    }
                    else
                    {
                        SpriteRenderer sr = sc.SetChar(27);
                    }

                    //nNum++;
                    bDollFlag = true;
                }
                
                //位置をずらす
                if (j == 0)
                {
                    pos.x -= 0.9f;
                }
                else if (j == 1 || j == 2)
                {
                    pos.x -= (0.9f * (2.5f / 3.0f));
                }
                else
                {
                    pos.x -= 0.6f;
                }

                nNum++;
            }

            //ドルマーク付ける
            if (!bDollFlag)
            {
                GameObject obj = Instantiate(mojiObject) as GameObject;

                // 子に設定する
                obj.transform.parent = cameraObject.transform;
                obj.transform.localPosition = pos;
                if (j == 0)
                {
                    obj.transform.localScale = new Vector3(3, 3, 3);
                }
                else if (j == 1 || j == 2)
                {
                    obj.transform.localScale = new Vector3(2.5f, 2.5f, 2.5f);
                }
                else
                {
                    obj.transform.localScale = new Vector3(2, 2, 2);
                }
                obj.transform.name = "doll_" + j + "_" + nNum;

                CharName sc = obj.GetComponent<CharName>();

                //ランクインした場合・・・
                if (j == m_nRankIn)
                {
                    m_atagRankIn.InsertLast(sc.SetChar(27));
                }
                else
                {
                    sc.SetChar(27);
                }

            }

            //位置をずらす
            if (j == 0)
            {
                pos.y -= 1.0f;
            }
            else if (j == 1 || j == 2)
            {
                pos.y -= (1.0f * (2.5f / 3.0f));
            }
            else
            {
                pos.y -= 0.7f;
            }
        }

        // 背景の色設定
        m_goBG = GameObject.Find("bg");
        Color color = new Color(1, 1, 1, 0.5f);
        m_goBG.renderer.material.SetColor("_Color", color);

        // 背景の座標設定
        m_vecBgPos = new Vector2(0, 0);

        // Enterキーの色設定
        m_srEnter.color = new Color(1.0f, 0.0f, 0.0f, 1.0f);

        m_bFade[0] = true;
        m_bFade[1] = true;
	}

    //=====================================
	// Update is called once per frame
    //=====================================
	void Update () {
        //	シーンの変更
        if (input.IsPushAnyKey())
        {
			if(fade.state == FadeControll.STATE_NONE){
				audiosource.PlayOneShot(audiosource.clip);
			}
            fade.StartChangeScene("Title");
        }

        //目立たせる処理
        if(m_nRankIn != -1)
        {
            if (m_bFade[0])
            {
                LinkedList<SpriteRenderer>.Node tagData = m_atagRankIn.First;
                for (int i = 0; i < m_atagRankIn.Count; i++)
                {
                    tagData.Value.color -= new Color(0.0f, 0.0f, 0.0f, 0.025f);
                    tagData = tagData.Next;
                }
                if (m_atagRankIn.First.Value.color.a <= 0.0f)
                {
                    m_bFade[0] = false;
                }
            }
            else
            {
                LinkedList<SpriteRenderer>.Node tagData = m_atagRankIn.First;
                for (int i = 0; i < m_atagRankIn.Count; i++)
                {
                    tagData.Value.color += new Color(0.0f, 0.0f, 0.0f, 0.025f);
                    tagData = tagData.Next;
                }
                if (m_atagRankIn.First.Value.color.a >= 1.0f)
                {
                    m_bFade[0] = true;
                }
            }
        }

        //Enter表示の点滅処理
        if (m_bFade[1])
        {
            m_srEnter.color -= new Color(0.0f, 0.0f, 0.0f, 0.025f);
            if (m_srEnter.color.a <= 0.2f)
            {
                m_bFade[1] = false;
            }
        }
        else
        {
            m_srEnter.color += new Color(0.0f, 0.0f, 0.0f, 0.025f);
            if (m_srEnter.color.a >= 1.0f)
            {
                m_bFade[1] = true;
            }
        }

        //背景動作処理
        m_vecBgPos += new Vector2(0.0005f,-0.0005f);
        m_goBG.renderer.material.SetTextureOffset("_MainTex", m_vecBgPos);

	}
}
