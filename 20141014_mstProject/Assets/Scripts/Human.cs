﻿
using UnityEngine;
using System.Collections;

public class Human : MonoBehaviour {

	public Animator animator;
	public float speed;
	public GameObject airShip = null;
	public Town town = null;
	private int jumpCount = 0;

	// Use this for initialization
	void Start () {
		speed = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {

		if(town && town.landFlag){
			if(airShip){
				speed += 0.1f;

				//	角度を飛行船に向ける
				Vector3 rot;
				rot = Quaternion.LookRotation(airShip.transform.position - transform.position).eulerAngles;
				rot.x = 0.0f;
				rot.z = 0.0f;
				transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(rot), 2 * Time.deltaTime);
			}
		}

		transform.position += transform.forward * speed * Time.deltaTime * transform.localScale.x;
		speed *= 0.9f;

		//	アニメーターの設定
		animator.SetFloat("Speed", speed);
	}
}
