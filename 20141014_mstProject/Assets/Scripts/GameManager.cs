﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	//	ゲームのステート
	public enum State{
		Start = 0,
		Count,
		Main,
		Fade,
		Change,
	};

	public GameControll game;
	public GameFade		gameFade;			//	ゲーム用フェード
	public AirShip 		airShip = null;		//	飛行船の参照
	public State		state;			//	ゲームのステート
	public TextMesh[]	itemLabels;		//	所持品の数
	public ItemManager	itemManager;
	
	public TextMesh 	countDownLabel;	// カウントダウンのラベル
	public SEPlayer sePlayer;

	private float startCount = 0.0f;
	private float count = 0.0f;
	private int countCounter = 3;
	private Color InitColor;
		
	void Start(){
		InitColor = countDownLabel.color;
		Color color = countDownLabel.color;
		color.a = 0.0f;
		countDownLabel.color = color;
		InitColor.a = 0.0f;
	}
	// Update is called once per frame
	void Update () {

		// スタート
		if(state == State.Start){
			if(count >= 5.0f){
				state = State.Count;
				countDownLabel.gameObject.SetActive(true);
				sePlayer.Play(SEPlayer.SE.CountDown);
			}
			count += Time.deltaTime;
		}
		// カウントダウン
		else if(state == State.Count){

			if(countCounter > 0){
				CountDownFunc(countCounter.ToString());
			}
			else if(countCounter == 0){
				CountDownFunc("GO");
				sePlayer.Play(SEPlayer.SE.GameEnd);
			}
			else{
				countDownLabel.gameObject.SetActive(false);
				countDownLabel.color = new Color(0.0f, 0.0f, 0.0f, 0.0f);
				state = State.Main;
                if (Application.loadedLevelName.ToString() == "Demo")
                {
                    GameObject.Find("DemoText").GetComponent<PlayLoad>().PlayStart(true);
                    Debug.Log("Start");
                }
			}
			startCount += Time.deltaTime;
		}
		//	メインステート
		else if(state == State.Main){

		}
		//	着陸ステート
		else if(state == State.Change){
			if(gameFade.state == GameFade.State.In){
				game.ChangeGameScene(GameControll.Scene.Trade);
				state = State.Main;
			}
		}
		//	演出ステート
		else if(state == State.Fade){
		}

		// 全てのステートに共通
		//	所持品の変更
		for(int i = 0; i < itemLabels.Length; i++){
			itemLabels[i].text = "x " + itemManager.itemNumArray[i];
		}
	}

	void CountDownFunc(string str){
		countDownLabel.text = str;
		countDownLabel.transform.localScale -= new Vector3(20.0f, 20.0f, 0.0f) * Time.deltaTime;
		countDownLabel.color += new Color(0.0f, 0.0f, 0.0f, 2.0f) * Time.deltaTime;
		if(countDownLabel.transform.localScale.x <= 1.0f){
			countDownLabel.transform.localScale = Vector3.one;
			countDownLabel.color = new Color(InitColor.r, InitColor.g, InitColor.b, 1.0f);
		}
		if(startCount > 1.0f){
			startCount = 0.0f;
			countCounter --;

			if(countCounter >= 0){
				countDownLabel.transform.localScale = new Vector3(10.0f, 10.0f, 1.0f);
				countDownLabel.color = InitColor;
				sePlayer.Play(SEPlayer.SE.CountDown);
			}
		}
	}
}
