﻿using UnityEngine;
using System.Collections;

public static class Common{

	public static string WithComma(this object self)
	{
		return "$" + string.Format("{0:#,##0}", self);
	}
}
