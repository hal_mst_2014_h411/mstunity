﻿//---------------------------------------------------------------------------------------
// 
// 作成者：
// 最終更新：
//---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------
// ライブラリ宣言
//--------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

//==========================================================================
// ScoreAnimationクラス
//==========================================================================
public class scoreAnimation : MonoBehaviour {
    //----------------------------------
    // 定数
    //----------------------------------
    private const float fpositionUp = 0.1f;
    private const float fAlpha = 0.8f;
    private const int ObjMax = 100;

    //----------------------------------
    // 変数
    //----------------------------------
    public GameObject ScoreAnimation;
    private GameObject[] Obj;

    //=====================================
	// 初期化
    //=====================================
	void Start () 
    {
        // 配列生成
        Obj = new GameObject[ObjMax];
	}

    //=====================================
	// 更新
    //=====================================
	void Update () 
    {
        // アニメーション処理
        AnimationUpdate();
	}

    //=====================================
    // アニメーション開始設定
    // 引数：位置、文字サイズ, 色, スコア
    // 戻り値：なし
    //=====================================
    public void AnimationStart(Vector3 pos, int nfontSize, Color color, int score)
    {
        for (int nCnt = 0; nCnt < ObjMax; nCnt++)
        {
            // 生成されていない部分に代入
            if (Obj[nCnt] == null)
            {
                // スコアアニメーション用guitextインスタンス生成
                Obj[nCnt] = (GameObject)Instantiate(ScoreAnimation, pos, new Quaternion(0.0f, 0.0f, 0.0f, 1.0f));

				// 親代入
				Obj[nCnt].transform.parent = this.transform;

                // 文字サイズ設定
                Obj[nCnt].guiText.fontSize = nfontSize;

                // 色設定
                Obj[nCnt].guiText.color = color;

                // スコア格納(文字)
                Obj[nCnt].guiText.text = "$" + score.ToString();

                break;
            }
        }
    }

    //=====================================
    // アニメーション処理
    //=====================================
    void AnimationUpdate()
    {
        // 生成されているものをチェックし、アニメーション処理
        for (int nCnt = 0; nCnt < ObjMax; nCnt++)
        {
            // guitextが生成されていれば処理
            if (Obj[nCnt] != null)
            {
                Vector3 postionY;
                Color color;

                // 文字を上方向に上昇
                postionY = Obj[nCnt].transform.position;
                postionY.y += fpositionUp * Time.deltaTime;
                Obj[nCnt].transform.position = postionY;

                // α値を下げて、徐々に透明に
                color = Obj[nCnt].guiText.color;
                color.a -= fAlpha * Time.deltaTime;
                Obj[nCnt].guiText.color = color;

                // 文字が見えなくなったら破棄
                if (color.a <= 0)
                {
                    Destroy(Obj[nCnt]);
                    Obj[nCnt] = null;
                }
            }
        }
    }
}
