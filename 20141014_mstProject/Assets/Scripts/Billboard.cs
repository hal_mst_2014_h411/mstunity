﻿using UnityEngine;
using System.Collections;

public class Billboard : MonoBehaviour {

	private Camera targetCamera;

	// Use this for initialization
	void Start () {
		if(!targetCamera){
			targetCamera = Camera.main.camera;
			if(!targetCamera){
				targetCamera = GameObject.Find("CharacterCamera").GetComponent<Camera>();
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		transform.LookAt(targetCamera.transform.position);
	}
}
