﻿using UnityEngine;
using System.Collections;

public class GameFade : MonoBehaviour {

	public enum State{
		None = 0,
		In,
		Out,
	};

	public State state;
	public SpriteRenderer sprite;
	public float addAlpha;
	public GameControll game;

	// Use this for initialization
	void Start () {
		Color c = sprite.color;

		if(state == State.In){
			c.a = 1.0f;
			sprite.color = c;
		}
		else if(state == State.Out || state == State.None){
			c.a = 0.0f;
			sprite.color = c;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
		switch(state){
		case State.None:
			break;
			
		case State.In:
			sprite.color -= new Color(0.0f, 0.0f, 0.0f, addAlpha);
			if(sprite.color.a <= 0.0f){
				sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, 0.0f);
				state = State.None;
			}
			break;
			
		case State.Out:
			sprite.color += new Color(0.0f, 0.0f, 0.0f, addAlpha);
			if(sprite.color.a >= 1.0f){
				sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, 1.0f);
				state = State.In;
			}
			break;
		}
	}

	public void SetFade(){
		state = State.Out;
	}
}
