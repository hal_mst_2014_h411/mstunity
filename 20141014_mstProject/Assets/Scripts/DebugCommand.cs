﻿using UnityEngine;
using System.Collections;

public class DebugCommand : MonoBehaviour {

	public ItemManager itemManager;
	public Gauge gauge;
	public TimerManager timer;
	public AirShip airShip;

	//========================================
	//  1:アイテム最大
	//  2:ゲージ最大
	//  3:所持金最大
	//  4:タイマー最大
	//  5:傾きなどのデバッグ表示ON/OFF
	// F1:タイトルに飛ぶ
	//========================================
	// Update is called once per frame
	void Update () {
	
		if(Input.GetKeyDown(KeyCode.Alpha1)){
			for(int i = 0; i < ItemManager.ItemMax; i++){
				itemManager.itemNumArray[i] = 9999;
			}
		}
		if(Input.GetKeyDown(KeyCode.Alpha2)){
			gauge.AddGauge(Gauge.MAX_GAGE);
		}
		if(Input.GetKeyDown(KeyCode.Alpha3)){
			ResultData.sales = 999999999;
			ScoreData.money =  999999999;
		}
		if(Input.GetKeyDown(KeyCode.Alpha4)){
			timer.m_fTime = TimerManager.TimeLimit;
		}
		if(Input.GetKeyDown(KeyCode.Alpha5)){
			airShip.debugFlag = !airShip.debugFlag;
		}
	}
}
