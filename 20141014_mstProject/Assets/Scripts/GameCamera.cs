﻿using UnityEngine;
using System.Collections;

public class GameCamera : MonoBehaviour {

	public AirShip airShip;
	public GameManager gameManager;


	// Use this for initialization
	void Start () {
		Vector3 pos = airShip.transform.localPosition;
		transform.Translate(0.0f, 0.0f, (100.0f - airShip.transform.position.z) + 100.0f);
		transform.LookAt(airShip.transform.position);
	}
	
	// Update is called once per frame
	void Update () {
		if(airShip){

			if(gameManager.state == GameManager.State.Start){
				transform.Translate(0.0f, 0.0f, (130.0f) / 5.0f * Time.deltaTime);
			}
			else if(gameManager.state == GameManager.State.Count){
				transform.localEulerAngles *= 0.95f;//10.0f * Time.deltaTime;
				transform.localPosition *= 0.98f;//10.0f * Time.deltaTime;
			}
			//	メイン画面
			else if(gameManager.state == GameManager.State.Main){
				if(airShip.focusTown){
					transform.localPosition += new Vector3(0.0f, 12.0f, -12.0f) * Time.deltaTime;
					transform.localEulerAngles += new Vector3(18.0f, 0.0f, 0.0f) * Time.deltaTime;
				}

				transform.localEulerAngles *= 0.95f;//10.0f * Time.deltaTime;
				transform.localPosition *= 0.98f;//10.0f * Time.deltaTime;
			}
			//	着陸画面
			else if(gameManager.state == GameManager.State.Change){
				transform.localPosition += (new Vector3(0.0f, 0.1f, -0.1f) - transform.localPosition)*0.03f;
			}
		}
	}
}
