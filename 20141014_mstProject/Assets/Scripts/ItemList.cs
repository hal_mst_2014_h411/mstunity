﻿using UnityEngine;
using System.Collections;

public class ItemList : MonoBehaviour {

	private static int ItemListNum = 10;	//	リストの数

	public ItemListLabel itemLabelBase;
	public ItemManager itemManager;

	private ItemListLabel[] itemLabelArray = new ItemListLabel[ItemListNum];

	// Use this for initialization
	void Start () {
		//	ラベルのクローンを生成
		for(int i = 0; i < ItemListNum; i++){
			itemLabelArray[i] = Instantiate(itemLabelBase) as ItemListLabel;
			itemLabelArray[i].transform.parent = gameObject.transform;
		}
		//	ベースのラベルを非表示にする
		itemLabelBase.gameObject.SetActive(false);
		Init ();

		gameObject.SetActive(false);
	}

	//	アイテムリストを更新
	void OnEnable(){
		Init ();
	}

	//	リストの初期化(表示の更新)
	void Init(){
		int count = 0;
		//	生成されていなければ終了
		if(itemLabelArray[0]){
			for(int i = 0; i < itemManager.itemNumArray.Length; i++){
				if(itemManager.itemNumArray[i] <= 0){
					itemLabelArray[i].gameObject.SetActive(false);
				}
				else{
					itemLabelArray[i].gameObject.SetActive(true);
					itemLabelArray[i].itemName.text = TradeCalculate.m_aMerchandiseData[i].sName + "\t" +
										itemManager.itemNumArray[i].ToString();
					itemLabelArray[i].transform.localPosition = 
						new Vector3(0.0f,
						            itemLabelBase.transform.localPosition.y - (count * 0.7f),
						            itemLabelBase.transform.localPosition.z);
					count ++;
				}
			}
		}
	}
}
