﻿//---------------------------------------------------------------------------------------
// 
// 作成者：
// 最終更新：
//---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------
// ライブラリ宣言
//--------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

//==========================================================================
// DiscoveryMessageクラス
//==========================================================================
public class DiscoveryMessage : MonoBehaviour {
    //----------------------------------
    // 定数
    //----------------------------------
    private const float BASE_ALPHA_MAX = 0.5f;                           // 背景のベースオブジェクトのα最大値
    private const int MESSAGE_MAX = 1;                                   // 保存できるメッセージの最大値

    //----------------------------------
    // 列挙体
    //----------------------------------
    enum STATE : int
    {
        NONE = 0,
        IN = 1,
        OUT = 2
    };

    //----------------------------------
    // 構造体
    //----------------------------------
    struct DiscoveryMessageData
    {
        public GameObject MessageObj;
        public float Speed;
        public int AnimationState;
    }

    //----------------------------------
    // 変数
    //----------------------------------
    // プレハブ
    public GameObject Messagetext;

    // オブジェクト取得
    private GameObject Base;
    private DiscoveryMessageData[] DiscoverMessage;

    private bool MessagePlayCheckFlag = false;
    private bool MessageStopAnimationFlag = false;
    private bool MessageDestoryFlag = false;

    public int StopAnimationNumber = 0;

    // 文字のサイズ
    private int MojiMax = (int)(Screen.height * 0.1f);

    //=====================================
	// 初期化
    //=====================================
	void Start () 
    {
        // 背景のベースオブジェクトの設定
        Base = GameObject.Find("base");
        Base.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 0.0f);

        // メッセージオブジェクト生成
        DiscoverMessage = new DiscoveryMessageData[MESSAGE_MAX];

        // メッセージ処理開始
        MessagePlayCheckFlag = false;

        // ストップアニメーション開始
        MessageStopAnimationFlag = false;

        StopAnimationNumber = 0;
    }

    //=====================================
	// 更新
    //=====================================
	void Update () 
    {
        //
        DebugTest2();

        // メッセージ破棄処理をするか確認
        if (MessageDestoryFlag == true)
        {
            MessageDestroy();
        }
        else
        {
            // メッセージの生成確認
            MessageCreateCheck();

            // 文字を止めてアニメーションするか、そのまま流す
            if (MessageStopAnimationFlag == false)
            {
                // メッセージ更新処理
                MessageUpdate();
            }
            else
            {
                // ストップアニメーション処理
                MessageStopAnimation();
            }
        }
	}

    //=====================================
    // メッセージ生成
    //=====================================
    public void MessageCreate(string Message, float MessageSpeed)
    {
        for (int nCnt = 0; nCnt < MESSAGE_MAX; nCnt++)
        {
            // guitextが生成されていなければ新しく生成する
            if (DiscoverMessage[nCnt].MessageObj == null)
            {
                // メッセージ処理開始用フラグ
                if (MessagePlayCheckFlag == false)
                {
                    MessagePlayCheckFlag = true;
                    Base.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 0.5f);
                }

                // 生成
                DiscoverMessage[nCnt].MessageObj = (GameObject)Instantiate(Messagetext, new Vector3(1.5f, 0.55f, 0.0f), new Quaternion(0.0f, 0.0f, 0.0f, 1.0f));

				// 親代入
				DiscoverMessage[nCnt].MessageObj.transform.parent = this.transform;

                // メッセージ内容
                DiscoverMessage[nCnt].MessageObj.guiText.text = Message;

                // 文字サイズ
                DiscoverMessage[nCnt].MessageObj.guiText.fontSize = MojiMax;

                // 流す速さ
                DiscoverMessage[nCnt].Speed = MessageSpeed;

                break;
            }
        }
    }

    //=====================================
    // メッセージ生成確認
    //=====================================
    void MessageCreateCheck()
    {
        int MessageUseChake = 0;

        // 配列の中身チェック
        for (int nCnt = 0; nCnt < MESSAGE_MAX; nCnt++)
        {
            if (DiscoverMessage[nCnt].MessageObj == null)
            {
                // メッセージが全て空か確認用
                MessageUseChake++;
            }
        }

        // メッセージが作られてなければBass背景を透明に
        if (MessageUseChake == MESSAGE_MAX)
        {
            MessagePlayCheckFlag = false;
        }
    }

    //=====================================
    // メッセージ更新処理
    //=====================================
    void MessageUpdate()
    {
        // メッセージ処理されていないなら背景のベースを透明にしていく
        if (MessagePlayCheckFlag == false &&
            Base.GetComponent<SpriteRenderer>().color.a >= 0)
        {
            Color color = Base.GetComponent<SpriteRenderer>().color;
            color.a -= 1.0f * Time.deltaTime;
            Base.GetComponent<SpriteRenderer>().color = color;
        }
        // 文字処理
        else if (MessagePlayCheckFlag == true)
        {
            for (int nCnt = 0; nCnt < MESSAGE_MAX; nCnt++)
            {
                Vector3 Postition = DiscoverMessage[nCnt].MessageObj.transform.position;
                Postition.x -= DiscoverMessage[nCnt].Speed * Time.deltaTime;
                DiscoverMessage[nCnt].MessageObj.transform.position = Postition;

                // 一定座標になったら止めてアニメーション処理
                if (DiscoverMessage[nCnt].MessageObj.transform.position.x <= 0.46f &&
                    DiscoverMessage[nCnt].MessageObj.transform.position.x >= 0.42f)
                {
                    MessageStopAnimationFlag = true;
                    StopAnimationNumber = nCnt;
                    DiscoverMessage[nCnt].AnimationState = (int)STATE.OUT;
                }

                // 端までいったら破棄
                if (DiscoverMessage[nCnt].MessageObj.transform.position.x <= -1.0f)
                {
                    DiscoverMessage[nCnt].Speed = 0;
                    Destroy(DiscoverMessage[nCnt].MessageObj);
                    DiscoverMessage[nCnt].MessageObj = null;
                }
            }
        }
    }

    //=====================================
    // ストップアニメーション開始処理
    //=====================================
    void MessageStopAnimation()
    {
        Color color;

        switch(DiscoverMessage[StopAnimationNumber].AnimationState)
        {
            case (int)STATE.IN:
                color = DiscoverMessage[StopAnimationNumber].MessageObj.guiText.color;
                color.a += 1.0f * Time.deltaTime;
                DiscoverMessage[StopAnimationNumber].MessageObj.guiText.color = color;


                if (color.a >= 1.0f)
                {
                    DiscoverMessage[StopAnimationNumber].AnimationState = (int)STATE.NONE;
                    MessageStopAnimationFlag = false;

                }
                break;

            case (int)STATE.OUT:
                color = DiscoverMessage[StopAnimationNumber].MessageObj.guiText.color;
                color.a -= 1.0f * Time.deltaTime;
                DiscoverMessage[StopAnimationNumber].MessageObj.guiText.color = color;

                if(color.a <= 0)
                {
                    DiscoverMessage[StopAnimationNumber].AnimationState = (int)STATE.IN;
                }
                break;

            default:
                break;
        }
    }

    //=====================================
    // メッセージ破棄処理
    //=====================================
    void MessageDestroy()
    {
        Color color;
        int nNullCount = 0;

        // 文字透明化、破棄処理
        for (int nCnt = 0; nCnt < MESSAGE_MAX; nCnt++)
        {
            if (DiscoverMessage[nCnt].MessageObj != null)
            {
                color = DiscoverMessage[nCnt].MessageObj.guiText.color;
                color.a -= 1.5f * Time.deltaTime;
                DiscoverMessage[nCnt].MessageObj.guiText.color = color;

                if (color.a <= 0.0f)
                {
                    DiscoverMessage[nCnt].Speed = 0;
                    Destroy(DiscoverMessage[nCnt].MessageObj);
                    DiscoverMessage[nCnt].MessageObj = null;
                }
            }
            else
            {
                nNullCount++;
            }
        }

        // 背景透明化
        if (Base.GetComponent<SpriteRenderer>().color.a > 0.0f)
        {
            color = Base.GetComponent<SpriteRenderer>().color;
            color.a -= 1.0f * Time.deltaTime;
            Base.GetComponent<SpriteRenderer>().color = color;
        }
        // 破棄処理終了
        else if (Base.GetComponent<SpriteRenderer>().color.a <= 0.0f && nNullCount == MESSAGE_MAX)
        {
            MessageStopAnimationFlag = false;
            MessageDestoryFlag = false;
        }
    }

    //=====================================
    // メッセージ破棄処理フラグ設定
    //=====================================
    public void SetMessageDestroyFlag(bool Flag)
    {
        MessageDestoryFlag = Flag;
    }

    //=====================================
    // メッセージ破棄処理フラグ取得
    //=====================================
    public bool GetMessageDestroyFlag()
    {
        return (MessageDestoryFlag);
    }

    //=====================================
    // デバック用
    // スプライト変更、値の確認
    // 後で削除予定
    //=====================================
    void DebugTest2()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            MessageCreate("test", 1.0f);
            Debug.Log("test");
            Debug.Log(Base.GetComponent<SpriteRenderer>().color.a);
            Debug.Log(MessagePlayCheckFlag);
        }

        if (DiscoverMessage[0].MessageObj != null)
        {
            Debug.Log(StopAnimationNumber);
        }

        if(Input.GetKeyDown(KeyCode.Alpha0))
        {
            SetMessageDestroyFlag(true);
        }
    }
}
