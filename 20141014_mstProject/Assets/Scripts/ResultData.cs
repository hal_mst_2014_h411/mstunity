﻿using UnityEngine;
using System.Collections;

public class ResultData : MonoBehaviour {
	
	static public int sales;		//	売上
	static public int pay;			//	支出
	static public int satisfy;		//	満足度
	static public int damage;		//	被害額

	//	数値を初期化する
	static public void Init(){
		sales = 0;
		pay = 0;
		satisfy = 0;
		damage = 0;
	}
}
