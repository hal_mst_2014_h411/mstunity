﻿//---------------------------------------------------------------------------------------
// 
//---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------
// ライブラリ宣言
//--------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

//==========================================================================
// AirShipクラス
//==========================================================================
public class AirShip : MonoBehaviour {

    //----------------------------------
    // 構造体
    //----------------------------------
    struct InTownCheckData
    {
        public GameObject oldfocusTown;
        public float outTime;
        public bool InTownFlag;
    }

	//	いっぺんに売れる数
	static private int[][] getNum = {
		new[]{0,		0,		0},
		new[]{10,		0,		0},
		new[]{1,		5,		1},
		new[]{1,		5,		1},
		new[]{10,		5,		1},
		new[]{10,		2,		10},
	};
	static public float SuperTime = 20.0f;

    //----------------------------------
    // 変数
    //----------------------------------
    private GameObject Meter;
	public float moveZ;
	public float moveX;
	public float moveY;
	public float rotY;
	public float rotZ;
	public GameControll game;
	public bool landFlag;			//	着陸フラグ
	public Animator animator;
	public float revisionHeight;
	public float flyTolerance;	//高さの許容誤差
	public float flyForce;	//高さの変更時の力
	public FieldItem ItemBase;
	public ItemManager itemManager;
	public GameManager gameManager;
	public Gauge gauge;
	public AudioSource audiosource;
	public bool debugFlag;
	//public GameObject	gaugeFont;
	public VirtualInput input;

	public GameObject focusTown = null;
	//public AudioSource audioSource;

	public SEPlayer sePlayer;

    private InTownCheckData InTownCheck;    // 入った街に再入場の判断に使用
	private float superCount;
	private bool superFlag;

    //=====================================
	// 初期化
    //=====================================
	void Start () 
    {
        Meter = GameObject.Find("meter");

		audiosource.Play();

        // 再入場データ関連初期化
        InTownCheck.oldfocusTown = null;
        InTownCheck.outTime = 0;
        InTownCheck.InTownFlag = false;
		//gaugeFont.SetActive(false);
	}

    //=====================================
	// 更新
    //=====================================
	void Update () 
    {	
 
		// ゲージマックス時
		/*if(gauge.GetGauge() >= Gauge.MAX_GAGE && !superFlag){
			superCount = SuperTime;
			superFlag = true;
			gaugeFont.SetActive(true);
		}*/
		float mulMove = 1.0f;
		// スーパー状態
		if(gauge.GetUseFlag()){
			mulMove = 3.0f;
		}
		if(gameManager.state == GameManager.State.Main){		
			//---上昇下降操作なしテスト---

			if(Application.loadedLevelName != "Demo"){
				//	移動
				if (input.GetAxis( VirtualInput.AXIS_STICK.X) <= -0.03f)
				{
					rigidbody.AddRelativeForce(new Vector3(-moveX, 0.0f, 0.0f) * Time.deltaTime * (0.3f + Input.GetAxis("stickX") * -1.0f));
					rigidbody.AddTorque(new Vector3(0.0f, -rotY*mulMove, rotZ) * Time.deltaTime * (0.3f + Input.GetAxis("stickX") * -1.0f));
				}
				if (input.GetAxis(VirtualInput.AXIS_STICK.X) >= 0.03f)
				{
					rigidbody.AddRelativeForce(new Vector3(moveX, 0.0f, 0.0f) * Time.deltaTime * (0.3f + Input.GetAxis("stickX") * 1.0f));
					rigidbody.AddTorque(new Vector3(0.0f, rotY*mulMove, -rotZ) * Time.deltaTime * (0.3f + Input.GetAxis("stickX") * 1.0f));
				}


				//	上下移動
				if (input.GetAxis(VirtualInput.AXIS_STICK.Y) <= -0.03f)
				{
					rigidbody.AddRelativeForce(new Vector3(0.0f, 0.0f, moveZ*mulMove) * Time.deltaTime);// * (0.2f + Input.GetAxis("stickY") * -1.0f));

	                }


				if (input.GetAxis(VirtualInput.AXIS_STICK.Y) >= 0.03f)
				{
					rigidbody.AddRelativeForce(new Vector3(0.0f, 0.0f, -moveZ*mulMove) * Time.deltaTime);// * (0.2f + Input.GetAxis("stickY") * 1.0f));
				}

/*
				if (Input.GetKey("up"))
				{
					rigidbody.AddRelativeForce(new Vector3(0.0f, 0.0f, moveZ*mulMove) * Time.deltaTime);
				}
				if (Input.GetKey("down"))
				{
					rigidbody.AddRelativeForce(new Vector3(0.0f, 0.0f, -moveZ*mulMove) * Time.deltaTime);
				}
 */ 
			}
			//地形に這う
			float mapPosition = Terrain.activeTerrain.SampleHeight(transform.position);	//地形の座標
			if( transform.position.y < mapPosition + revisionHeight - flyTolerance ){	//低すぎる
				rigidbody.AddRelativeForce(new Vector3(0.0f, flyForce * Time.deltaTime, 0.0f));

			}
			if( mapPosition + revisionHeight + flyTolerance < transform.position.y ){	//高すぎる
				rigidbody.AddRelativeForce(new Vector3(0.0f, -flyForce * Time.deltaTime, 0.0f));

			}
		}

		float animeSpeed = ((rigidbody.velocity.magnitude / 10.0f) * 10.5f * Time.deltaTime) + 0.05f;
		if(animeSpeed >= 1.5f){
			animeSpeed = 1.5f;
		}
		else if(animeSpeed <= -1.5f){
			animeSpeed = -1.5f;
		}
		animator.speed = animeSpeed;

		audiosource.pitch = (animeSpeed * 2.0f);

		// スーパーモード
		if(superFlag){
			superCount -= Time.deltaTime;

			if(superCount <= 0.0f ){
				//gauge.SetGauge(0.0f);
				superCount = 0.0f;
				superFlag = false;
				//gaugeFont.SetActive(false);
			}
		}
	}

    //=====================================
    //
    //=====================================
	private void OnTriggerEnter(Collider col) 
    {
		if(col.gameObject.CompareTag("Town"))
        {
            if (!focusTown)
            {
                focusTown = col.gameObject;
                InTownCheck.oldfocusTown = focusTown;
                landFlag = true;
                //Debug.Log("ゲームに遷移");
                FieldMessage.SetMessage("町発見", 3);
			}
		}
		if(col.gameObject.CompareTag("Coin"))
		{
			// お金増やす
			Coin coin = col.gameObject.GetComponent<Coin>();
			int plice = coin.price;
			ResultData.sales += plice;
			ScoreData.money += plice;

			Destroy(col.gameObject);
			// SE再生
			sePlayer.Play(SEPlayer.SE.AirshipCoin);
		}
	}

    //=====================================
    //
    //=====================================
	private void OnTriggerExit(Collider col) 
    {
		if(col.gameObject.CompareTag("Town"))
        {
            InTownCheck.InTownFlag = false;
            InTownCheck.outTime = 0;
            InTownCheck.oldfocusTown = null;
			focusTown = null;
			landFlag = false;
		}
	}

    //=====================================
    //
    //=====================================
	private void OnCollisionEnter(Collision collision)
	{
		ScoreData.money -= (int)rigidbody.velocity.magnitude * 1000;
		ResultData.damage += (int)rigidbody.velocity.magnitude * 1000;
		//	SE再生
		sePlayer.Play(SEPlayer.SE.AirshipCol);
	}

    //=====================================
	//	着陸判定

    //=====================================
	public bool IsLand()
    {
		return landFlag;
	}

    //=====================================
    // 抜けた時間保存
    //=====================================
    public void SetOutTime(float outTime)
    {
        InTownCheck.outTime = outTime;
    }

    //=====================================
    // 街に入ったフラグを設定
    //=====================================
    public void SetInTownFlag(bool InTownFlag)
    {
        InTownCheck.InTownFlag = InTownFlag;
    }

    //=====================================
    // 再入場確認
    //=====================================
    public bool AgainInTownCheck()
    {
        // 入場したことがあるなら
        if (InTownCheck.InTownFlag)
        {
            // 約3秒経過したら
            if (InTownCheck.outTime >= 0.1f)
            {
                return (true);
            }
            else
            {
                InTownCheck.outTime += Time.deltaTime;
                return (false);
            }
        }
        return (true);
    }

    //=====================================
    // 描画
    //=====================================
    void OnGUI()
    {
		if(debugFlag){
			GUI.backgroundColor = new Color(0, 0, 0, 255);
	        GUI.Label(new Rect(0, 140, 200, 185), "傾きY：" + Input.GetAxis("stickY"));
	        GUI.Label(new Rect(0, 155, 200, 185), "傾きX：" + Input.GetAxis("stickX"));
	        GUI.Label(new Rect(0, 170, 200, 185), "傾き左右：" + Input.GetAxis("axis3"));
	        GUI.Label(new Rect(0, 185, 200, 200), "傾き前後：" + Input.GetAxis("axis6"));
	        GUI.Label(new Rect(0, 200, 200, 215), "傾き係数：" + Time.deltaTime * (0.3f + Input.GetAxis("axis3")));
			GUI.Label(new Rect(0, 215, 200, 215), "地形の高さ：" + Terrain.activeTerrain.SampleHeight(transform.position));
			GUI.Label(new Rect(0, 230, 200, 215), "モデルの高さ：" + transform.position.y );
		}
	}

	// アイテム出荷処理
	public void CreateSaleItem(AreaCollider.Type areaType){
		bool getItemFlag = false;

		for(int i = 0; i < ItemManager.ItemMax; i++){
			//	アイテムがあれば売る
			int result = itemManager.itemNumArray[i] - getNum[(int)areaType][i];
			
			if(result < 0){
				result = itemManager.itemNumArray[i];
			}
			else{
				result = getNum[(int)areaType][i];
			}

			for(int j = 0; j < result; j++){
				FieldItem item = Instantiate(ItemBase) as FieldItem;
				item.transform.parent = transform;
				item.transform.localPosition = Vector3.zero;
				item.gameObject.SetActive(true);
				item.areaType = areaType;
				item.sprite.sprite = game.itemSprite[i];
				item.itemType = i;
				itemManager.itemNumArray[i] --;

				getItemFlag = true;
			}
		}

		if(getItemFlag && (superCount <= 0.0f)){
			// ゲージを増加
			gauge.AddGauge(10.0f);
			//	SE再生
			sePlayer.Play(SEPlayer.SE.AirshipSale);
		}
	}
}
