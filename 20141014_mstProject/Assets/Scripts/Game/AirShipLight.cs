﻿using UnityEngine;
using System.Collections;

public class AirShipLight : MonoBehaviour {

	// Use this for initialization
	void Start () {
		//gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 scale = transform.localScale;
		scale.y += 1.0f;
		if(scale.y >= 20.0f){
			scale.y = 20.0f;
		}
		transform.localScale = scale;

		Vector3 pos = transform.localPosition;
		pos.y = -scale.y;
		transform.localPosition = pos;
	}

	public void Reset(){
		transform.localScale = new Vector3(3.0f, 1.0f, 3.0f);
		transform.localPosition = Vector3.zero;
	}
}
