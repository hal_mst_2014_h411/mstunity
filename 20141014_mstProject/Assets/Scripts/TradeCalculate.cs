﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;
using Collections;

//===============================================
//構造体
//===============================================
//商品データ
public struct merchandise
{
	///<summary>商品ID</summary>
	public int nId;
	///<summary>商品名</summary>
	public string sName;
	///<summary>仕入れ値</summary>
	public int nCost;
	///<summary>商品の種類</summary>
	public int nKind;
    ///<summary>商品の種類名称</summary>
    public string nKindName;
	///<summary>商品の希少度</summary>
	public int nRarity;
	///<summary>商品個数</summary>
	public int nNumber;
	///<summary>配列の最後を示す（最後の配列の場合はは、データを入れずにここにfalseを入れる。他はtrueを入れる）</summary>
	public bool bEnd;
}

//地域データ
struct city
{
	///<summary>地域ID</summary>
	public int nId;
	///<summary>地域に販売されている商品</summary>
	public merchandise[] tagMerchandise;
}

public class TradeCalculate : MonoBehaviour
{
	public static int m_nMoney;                 //合計金額
	private bool m_bTrade;                      //売買制御
	public static merchandise[] m_aMerchandiseData;    //商品データ
	private city m_tagCityData;                  //地域データ
	//倉庫にある商品
	private LinkedList<merchandise> m_atagData = new LinkedList<merchandise>();
	
	//デバック用変数
	private bool m_bDraw;
	private GUIStyle style;
	
	// Use this for initialization
	void Start () {
		m_bDraw = false;
		style = new GUIStyle();
		style.normal.textColor = Color.red;
	}
	
	// Update is called once per frame
	void Update () {
		//売買中ならば
		if (m_bTrade)
		{
			//売買します
			int i = 0;
			//商品があるなら
			while (m_tagCityData.tagMerchandise[i].bEnd)
			{
				//限界まで買います
				for (int j =m_tagCityData.tagMerchandise[i].nNumber ;j != 0 ;j-- )
				{
					//j番目の商品が買えるなら
					if (m_nMoney > m_tagCityData.tagMerchandise[i].nNumber*j)
					{
						//買う
						m_nMoney -= m_tagCityData.tagMerchandise[i].nNumber * j;
						merchandise data;
						data = m_tagCityData.tagMerchandise[i];
						data.nNumber = j;
						m_atagData.InsertLast(data);
						break;
					}
				}
				i++;
			}
			m_bTrade = false;
		}
		
		if (Input.GetKeyDown(KeyCode.D))
		{
			if(m_bDraw)
			{
				m_bDraw = false;
			}
			else
			{
				m_bDraw = true;
			}
		}
	}
	
	/// <summary>保持金額の初期化</summary>
	public void InitMoney() { m_nMoney = 100000;}
	
	/// <summary>売買開始</summary>
	public void StartTrade() { m_bTrade = true; }
	
	/// <summary>売買終了</summary>
	public void EndTrade() { m_bTrade = false; }
	
	//////////////////////////////////////////////////////
	/// <summary>商品データ読み出し</summary>
	/// <returns>データ読み出し成功したか</returns>
	//////////////////////////////////////////////////////
	public bool ReadAllMerchandiseData()
	{
		
		System.IO.StreamReader cFile = new System.IO.StreamReader(@"MerchandiseData.csv", System.Text.Encoding.Default);
		
		//１行目は飛ばす
		if (cFile.Peek() >= 0)
		{
			// ファイルを 1 行読み込む
			string stBuffer;
			stBuffer = cFile.ReadLine();
		}
		
		// 読み込みできる文字がなくなるまで繰り返す
		LinkedList<merchandise> dataBuff = new LinkedList<merchandise>();
		while (cFile.Peek() >= 0)
		{
			// ファイルを 1 行ずつ読み込む
			string[] stBuffer = cFile.ReadLine().Split(',');
			merchandise data = new merchandise();
			data.nId = int.Parse(stBuffer[0]);
			data.sName = stBuffer[1];
			data.nCost = int.Parse(stBuffer[2]);
			data.nKind = int.Parse(stBuffer[3]);
			data.nRarity = int.Parse(stBuffer[4]);
			dataBuff.InsertLast(data);
		}
		
		// cReader を閉じる (正しくは オブジェクトの破棄を保証する を参照)
		cFile.Close();
		
		//配列に入れる
		m_aMerchandiseData = new merchandise[dataBuff.Count+1];
		LinkedList<merchandise>.Node tagData = dataBuff.First;
		int i;
		for (i = 0; i < dataBuff.Count;i++ )
		{
			m_aMerchandiseData[i] = tagData.Value;
			m_aMerchandiseData[i].bEnd = true;
			tagData = tagData.Next;
		}
		m_aMerchandiseData[i].bEnd = false;
		
		//連結リスト削除
		while (dataBuff.Count != 0)
		{
			dataBuff.EraseFirst();
		}
		
		return true;
	}
	
	/////////////////////////////////////////////////
	/// <summary>地域データ読み出し</summary>
	/// <remarks name="sCityName">都市名</remarks>
	/// <returns>データ読み出し成功したか</returns>
	/////////////////////////////////////////////////
	public bool ReadCityData(string sCityName)
	{
		
		System.IO.StreamReader cFile = new System.IO.StreamReader(@"CityData.csv", System.Text.Encoding.Default);
		
		// 読み込みできる文字がなくなるまで繰り返す
		bool bFlag = false;
		while (cFile.Peek() >= 0)
		{
			bFlag = false;
			// ファイルを 1 行ずつ読み込む
			string[] stBuffer = cFile.ReadLine().Split(',');
			//文字列を比較する
			int iCompare = string.Compare(stBuffer[0], sCityName);
			//もし、読み込んだデータと入力都市名が同じなら
			if (iCompare == 0)
			{
				//データ読み込み
				//販売id
				if (cFile.Peek() >= 0)
				{
					stBuffer = cFile.ReadLine().Split(',');
					m_tagCityData.tagMerchandise = new merchandise[stBuffer.Length];
					for (int i = 0; i < stBuffer.Length; i++)
					{
						if (i == stBuffer.Length - 1)
						{
							m_tagCityData.tagMerchandise[i].bEnd = false;
						}
						else
						{
							m_tagCityData.tagMerchandise[i].nId = int.Parse(stBuffer[i + 1]);
							m_tagCityData.tagMerchandise[i].bEnd = true;
						}
					}
				}
				//販売個数
				if (cFile.Peek() >= 0)
				{
					stBuffer = cFile.ReadLine().Split(',');
					for (int i = 0; i < stBuffer.Length-1; i++)
					{
						m_tagCityData.tagMerchandise[i].nNumber = int.Parse(stBuffer[i + 1]);
					}
				}
				//ニーズ
				if (cFile.Peek() >= 0)
				{
				}
				bFlag = true;
				break;
			}
		}
		
		// cReader を閉じる (正しくは オブジェクトの破棄を保証する を参照)
		cFile.Close();
		
		//同一都市名がなかったら
		if(!bFlag)
		{
			//エラーリターン
			return false;
		}
		
		//変数代入
		if (m_aMerchandiseData != null)
		{
			int i,j;
			j = 0;
			i = 0;
			while (m_aMerchandiseData[i].bEnd)
			{
				if (m_tagCityData.tagMerchandise[j].nId == m_aMerchandiseData[i].nId)
				{
					m_tagCityData.tagMerchandise[j].nKind = m_aMerchandiseData[i].nKind;
					m_tagCityData.tagMerchandise[j].sName = m_aMerchandiseData[i].sName;
					m_tagCityData.tagMerchandise[j].nRarity = m_aMerchandiseData[i].nRarity;
					m_tagCityData.tagMerchandise[j].nCost = m_aMerchandiseData[i].nCost;
					m_tagCityData.tagMerchandise[j].nNumber = 100;
					m_tagCityData.tagMerchandise[j].bEnd = true;
					j++;
				}
				if (!m_tagCityData.tagMerchandise[j].bEnd)
				{
					break;
				}
				i++;
			}
		}
		return true;
	}
	
	//表示
	public void OnGUI()
	{
		//GUI.Label(new Rect(0, 0, 150, 50), "合計金額：" + m_nMoney, style);
        //GUI.Label(new Rect(0, 10, 150, 50), "スクリーン幅　：" + Screen.width, style);
        //GUI.Label(new Rect(0, 20, 150, 50), "スクリーン高さ：" + Screen.height, style);
		
		/*if(m_bDraw)
		{
			int i = 0;
			while (m_tagCityData.tagMerchandise[i].bEnd)
			{
				GUI.Label(new Rect(0, 10 * (i + 1), 150, 50), m_tagCityData.tagMerchandise[i].sName + "：" + m_tagCityData.tagMerchandise[i].nCost);
				i++;
			}
			int j = 0;
			while (m_aMerchandiseData[j].bEnd)
			{
				GUI.Label(new Rect(0, 30+10 * (i + j), 150, 50), m_aMerchandiseData[j].sName + "：" + m_aMerchandiseData[j].nCost, style);
				j++;
			}
		}*/
	}
	
	/// <summary>全リセット</summary>
	public void End()
	{
		while (m_atagData.Count != 0)
		{
			m_atagData.EraseFirst();
		}
	}
}
