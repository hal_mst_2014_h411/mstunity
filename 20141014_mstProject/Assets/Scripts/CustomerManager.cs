﻿//---------------------------------------------------------------------------------------
// 
// 作成者：
// 最終更新：
//---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------
// ライブラリ宣言
//--------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

//==========================================================================
// CustomerManagerクラス
//==========================================================================
public class CustomerManager : MonoBehaviour {
    
    //----------------------------------
    // 定数
    //----------------------------------
    // 顧客数
    private const int CustomerMax = 5;

    // テクスチャパス(スプライト)
    private const string CUSTOMER_TEXTURE = "Textures/manzoku";

    // 増減値の最大値、最小値
    private const int VALUE_MAX = 500;
    private const int VALUE_MIN = 0;

    //----------------------------------
    // 列挙型
    //----------------------------------
    // 満足度値
    enum CustomerLankLimit : int
    {
        VeryGood = 500,
        Good = 400,
        Normal = 300,
        Bad = 200,
        VeryBad = 100
    };

    // 満足度ランク
    enum CustomerLank : int
    {
        VeryGood = 4,
        Good = 3,
        Normal = 2,
        Bad = 1,
        VeryBad = 0
    };

    //----------------------------------
    // 構造体
    //----------------------------------
    // 顧客
    struct CustomerList
    {
        public GameObject Customer;    // オブジェクト
        public float fCustomerValue;   // 満足度数値
        public int nCustomerLank;      // 満足度ランク
        public int nOldCustomerLank;   // 前の満足度ランク
    }

    //----------------------------------
    // 変数
    //----------------------------------
    // CustomerList
    private CustomerList[] m_CustomerList;

    //=====================================
	// 初期化
    //=====================================
	void Start () 
    {
        // 構造体を複数生成
        m_CustomerList = new CustomerList[CustomerMax];

        // 中身の初期化
        for(int nCnt = 0; nCnt < CustomerMax; nCnt++)
        {
            m_CustomerList[nCnt].fCustomerValue = (float)CustomerLankLimit.VeryGood;
            m_CustomerList[nCnt].nCustomerLank = (int)CustomerLank.VeryGood;
            m_CustomerList[nCnt].nOldCustomerLank = m_CustomerList[nCnt].nCustomerLank;
        }

        // オブジェクト保存
        m_CustomerList[0].Customer = GameObject.Find("Customer1");
        m_CustomerList[1].Customer = GameObject.Find("Customer2");
        m_CustomerList[2].Customer = GameObject.Find("Customer3");
        m_CustomerList[3].Customer = GameObject.Find("Customer4");
        m_CustomerList[4].Customer = GameObject.Find("Customer5");

        // スプライト画像設定
        for (int nCnt = 0; nCnt < CustomerMax; nCnt++)
        {
            m_CustomerList[nCnt].Customer.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(CUSTOMER_TEXTURE + m_CustomerList[nCnt].nCustomerLank.ToString());
        }
	}

    //=====================================
	// 更新
    //=====================================
	void Update () 
    {
        // デバック用関数
        DebugTest();

        // 顧客満足度ランク変更
        CustomerLankChange();

        // スプライト変更
        CustomerSpriteChange();

	}

    //=====================================
    // 顧客満足度数増加
    // 引数：顧客番号, 増値
    // 戻り値：なし
    //=====================================
    public void CustomerValueAdd(int nCustomerNumber, float fValue)
    {
        m_CustomerList[nCustomerNumber].fCustomerValue += fValue;

        // 最大値を超えていたら最大値に設定
        if (m_CustomerList[nCustomerNumber].fCustomerValue >= VALUE_MAX)
        {
            m_CustomerList[nCustomerNumber].fCustomerValue = VALUE_MAX;
        }
    }

    //=====================================
    // 顧客満足度数減算
    // 引数：顧客番号, 減値
    // 戻り値：なし
    //=====================================
    public void CustomerValueSub(int nCustomerNumber, float fValue)
    {
        m_CustomerList[nCustomerNumber].fCustomerValue -= fValue;

        // 最小値を下回っていたら最小値に設定
        if (m_CustomerList[nCustomerNumber].fCustomerValue <= VALUE_MIN)
        {
            m_CustomerList[nCustomerNumber].fCustomerValue = VALUE_MIN;
        }
    }

    //=====================================
    // 顧客満足ランク確認、変更
    //=====================================
    void CustomerLankChange()
    {
        for (int nCnt = 0; nCnt < CustomerMax; nCnt++)
        {
            // 顧客満足度値によって顧客満足ランクを変更する
            // すごく良い
            if ((int)m_CustomerList[nCnt].fCustomerValue <= (int)CustomerLankLimit.VeryGood &&
                (int)m_CustomerList[nCnt].fCustomerValue > (int)CustomerLankLimit.Good)
            {
                m_CustomerList[nCnt].nCustomerLank = (int)CustomerLank.VeryGood;
            }
            // 良い
            else if ((int)m_CustomerList[nCnt].fCustomerValue <= (int)CustomerLankLimit.Good &&
                (int)m_CustomerList[nCnt].fCustomerValue > (int)CustomerLankLimit.Normal)
            {
                m_CustomerList[nCnt].nCustomerLank = (int)CustomerLank.Good;
            }
            // 普通
            else if ((int)m_CustomerList[nCnt].fCustomerValue <= (int)CustomerLankLimit.Normal &&
                (int)m_CustomerList[nCnt].fCustomerValue > (int)CustomerLankLimit.Bad)
            {
                m_CustomerList[nCnt].nCustomerLank = (int)CustomerLank.Normal;
            }
            // 悪い
            else if ((int)m_CustomerList[nCnt].fCustomerValue <= (int)CustomerLankLimit.Bad &&
                (int)m_CustomerList[nCnt].fCustomerValue > (int)CustomerLankLimit.VeryBad)
            {
                m_CustomerList[nCnt].nCustomerLank = (int)CustomerLank.Bad;
            }
            // 帰る
            else if ((int)m_CustomerList[nCnt].fCustomerValue <= (int)CustomerLankLimit.VeryBad)
            {
                m_CustomerList[nCnt].nCustomerLank = (int)CustomerLank.VeryBad;
            }
        }
    }

    //=====================================
    // スプライト変更
    //=====================================
    void CustomerSpriteChange()
    {
        for (int nCnt = 0; nCnt < CustomerMax; nCnt++)
        {
            // 前回のランクから変化があればスプライト変更
            if (m_CustomerList[nCnt].nCustomerLank != m_CustomerList[nCnt].nOldCustomerLank)
            {
                m_CustomerList[nCnt].Customer.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(CUSTOMER_TEXTURE + m_CustomerList[nCnt].nCustomerLank.ToString());
                m_CustomerList[nCnt].nOldCustomerLank = m_CustomerList[nCnt].nCustomerLank;
            }
        }
    }

    //=====================================
    // デバック用
    // スプライト変更、値の確認
    // 後で削除予定
    //=====================================
    void DebugTest()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            this.CustomerValueAdd(0, 10);
        }

        if (Input.GetKeyDown(KeyCode.F2))
        {
            this.CustomerValueSub(0, 10);
        }

        //Debug.Log(m_CustomerList[0].fCustomerValue);
    }
}
