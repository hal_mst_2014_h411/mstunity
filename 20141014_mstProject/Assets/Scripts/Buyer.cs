﻿using UnityEngine;
using System.Collections;

public class Buyer: MonoBehaviour {

	//	バイヤータイプ
	public enum Type{
		Buyer00 = 0,
		Buyer01,
		Buyer02,
		Buyer03,
		Buyer04,
		MAX,
	};
	
	//	かんがえかたの文字
	static private string[] Pesrons = {
		"買いたい",
		"売り買いしたい",
		"売りたい",
		"何もしたくない",
		"売り買い最強",
	};

	//	考え方（購入係数, 販売係数）
	static public int[][] Status = {
		new[]{30, 	1},
		new[]{20, 	20},
		new[]{5, 	50},
		new[]{1, 	1},
		new[]{100,	100},
	};

	//	バイヤーの名前
	static private string[] BuyerName = {
		"食料品中心",
		"工業品中心",
		"装飾品中心",
		"無能",
		"最強",
	};

	//	バイヤーのアイテムのレベル(0～2)
	static public int[][] saleRank = {
		new[]{2, 0, 0},
		new[]{0, 2, 0},
		new[]{0, 0, 2},
		new[]{1, 1, 1},
		new[]{2, 2, 2},
	};

	public TradeManager tradeManager;
	public Type type;
	public SpriteRenderer sprite;
	public SpriteRenderer buyerSprite;
	public TextMesh buyerName;
	public TextMesh personLabel;
	public BuyerItemRank rankBase;

	private BuyerItemRank[] itemRanks = new BuyerItemRank[3];

	// Use this for initialization
	void Start () {

	}

	public void Init(){
		for(int i = 0; i < ItemManager.ItemMax; i++){
			Vector3 basePos = rankBase.transform.localPosition;
			itemRanks[i] = Instantiate(rankBase) as BuyerItemRank;
			itemRanks[i].transform.parent = transform;
			itemRanks[i].transform.localScale = Vector3.one;
			itemRanks[i].transform.localPosition = new Vector3(basePos.x, basePos.y + (i*-0.6f), basePos.z);
		}
		
		rankBase.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Reset(Type buyerType){
		type = buyerType;
		buyerName.text = BuyerName[(int)type];
		personLabel.text = Pesrons[(int)type];
		buyerSprite.sprite = tradeManager.buyerSprite[(int)type];

		// アイテムのランクを能力によって変更
		for(int i = 0; i < ItemManager.ItemMax; i++){
			itemRanks[i].Reset(i, saleRank[(int)type][i]);
		}
	}
}
