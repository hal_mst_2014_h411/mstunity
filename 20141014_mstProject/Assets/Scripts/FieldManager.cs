﻿//---------------------------------------------------------------------------------------
// 2014/12/04更新内容メモ
// 制限用判定プレハブを生成するように追加しました
// フィール生成と同様にステージごとに分けれるように設定しています
// 担当：笠井 
//
// 2014/12/06更新内容メモ
// 透明の壁を設置できるように追加しました
// 担当：笠井
//---------------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

public class FieldManager : MonoBehaviour {

	public AirShip		airShip;

	public int selectIdx;
	public GameObject[] fields;			//	フィールド
    public GameObject[] fieldLimits;    //  フィールドごとの制限値判定
    public GameObject[] fieldLimitLines;//  フィールド制限値のオブジェ用
	public Vector3[]	StartPosition;	//	プレイヤー初期位置

	// Use this for initialization
	void Start () {
		GameObject field = Instantiate(fields[selectIdx]) as GameObject;
		field.transform.parent = transform;

        GameObject fieldLimit = Instantiate(fieldLimits[selectIdx]) as GameObject;
        fieldLimit.transform.parent = transform;

        GameObject fieldLimitLine = Instantiate(fieldLimitLines[selectIdx]) as GameObject;
        fieldLimitLine.transform.parent = transform;

		airShip.transform.localPosition = StartPosition[selectIdx];
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
