﻿using UnityEngine;
using System.Collections;
using System;

//デバック用インポート
using System.Text;
using System.IO;

public class SpeedMeter : MonoBehaviour {

    public AirShip player;                          //プレイヤーオブジェクト

    private float m_fRot;                           //メーターの針の現在の角度
    private Vector3 m_oldPos;                       //プレイヤーの古い位置
    private const int HEIKIN = 100;                 //針をなめらかに動かすための定数
    private float[] m_fBuff = new float[HEIKIN];    //針をなめらかに動かすための配列
    private int m_nCount;                           //カウント
    

    //デバック用変数
    private GUIStyle style;
    private float m_fbuff;

	// Use this for initialization
	void Start () {
        m_fRot = 5.0f;
        m_oldPos = player.transform.localPosition;
        
        for (int i = 0; i < HEIKIN; i++)
        {
            m_fBuff[i] = 0;
        }
        m_nCount = 0;
        
        //デバック用変数初期化
        style = new GUIStyle();
        style.normal.textColor = Color.black;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 pos = player.transform.localPosition;
        //プレイヤーの移動距離計算
        float distance = (float)Math.Sqrt((pos.x - m_oldPos.x)*(pos.x - m_oldPos.x)+(pos.y - m_oldPos.y)*(pos.y - m_oldPos.y)+(pos.z - m_oldPos.z)*(pos.z - m_oldPos.z));
        //移動距離調整
        distance *= 600.0f;
        distance = Chousei(distance);
        
        //回転させる角度を計算
        float fRot = -distance - m_fRot + 5.0f;
        fRot *= 0.1f;
        m_fRot += fRot;
        m_fbuff = m_fRot;//デバック
        if(m_fRot < -190.0f)
        {
            m_fRot = -190.0f;
        }
        //角度入力
        transform.eulerAngles = new Vector3(0, 0, m_fRot);
        //更新処理
        m_oldPos = pos;
	}

    //表示
    public void OnGUI()
    {
        //GUI.Label(new Rect(0, 0, 150, 50), "角度：" + m_fbuff, style);
    }

    /// <summary>
    /// 針をなめらかに動かすための処理
    /// </summary>
    /// <param name="fbuff">飛行船のスピード</param>
    /// <returns>針を動かすためのスピード数値</returns>
    private float Chousei(float fbuff)
    {
        m_fBuff[m_nCount] = fbuff;
        float buff;
        buff = 0;
        for (int i = 0; i < HEIKIN; i++)
        {
            buff += m_fBuff[i];
        }
        buff /= (float)HEIKIN;
        m_nCount++;
        m_nCount %= HEIKIN;

        return buff;
    }
}
