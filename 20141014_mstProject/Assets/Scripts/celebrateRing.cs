﻿using UnityEngine;
using System.Collections;

public class celebrateRing : MonoBehaviour {
	private Mesh mesh;
	private int HeightPlaneNum = 1;	//固定
	public int RowPlaneNum;
	public float innerRad;
	public float outerRad;
	public float circleAnimationTime;

   
	// Use this for initialization
	void Start () {

		int MaxIndex = RowPlaneNum * 2 * 3;	//面の数 * 1面の3角形の数 * 3角形の頂点数
		int MaxVertex = HeightPlaneNum * RowPlaneNum * 4;

		mesh = new Mesh();
		Vector3[] newVertices = new Vector3[MaxVertex];
		Vector2[] newUV = new Vector2[MaxVertex];
		Color[] newColor = new Color[MaxVertex];

		int[] newTrianglesIndex = new int[MaxIndex];

		float oneDeg = (2 * Mathf.PI) / 360.0f * (360.0f / RowPlaneNum);
		// 頂点座標の指定.
		for (int i = 0; i < MaxVertex; i++)
		{
			int tmpI = i / 2;
			float x, y;

			x = Mathf.Sin((float)tmpI * oneDeg);
			y = Mathf.Cos((float)tmpI * oneDeg);

			if (i % 2 == 0)
			{
				newVertices[i] = new Vector3(x * innerRad, y * innerRad, 0);
			}
			else
			{
				newVertices[i] = new Vector3(x * outerRad, y * outerRad, 0);
			}
		}
       
		// UVの指定 (頂点数と同じ数を指定すること).
		float oneUV_X = 1.0f / RowPlaneNum;

		for (int i = 0; i < MaxVertex; i++)
		{
			int tmpI = i / 2;
			if (i % 2 == 0)
			{
				newUV[i] = new Vector2( oneUV_X * tmpI, 0.0f);
			}
			else
			{
				newUV[i] = new Vector2( oneUV_X * tmpI, 1.0f);
			}
		}
       
		// 三角形ごとの頂点インデックスを指定.
		int maxTriangleNum = HeightPlaneNum * RowPlaneNum * 2;
		int indexNum = 0;
		for (int i = 0; i < maxTriangleNum; i++)
		{
			if (i % 2 == 0)
			{
				newTrianglesIndex[indexNum] = i;
				newTrianglesIndex[indexNum + 1] = i+1;
				newTrianglesIndex[indexNum + 2] = i+2;
			}
			else
			{
				newTrianglesIndex[indexNum] = i;
				newTrianglesIndex[indexNum + 1] = i+2;
				newTrianglesIndex[indexNum + 2] = i+1;
			}
			indexNum += 3;
		}
       
		mesh.vertices  = newVertices;
		mesh.uv        = newUV;
		mesh.triangles = newTrianglesIndex;

		newColor[0] =
		newColor[1] =
		newColor[2] =
		newColor[3] = new Color(1, 1, 1, 1);

		mesh.colors = newColor;
       
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
       
		GetComponent<MeshFilter>().sharedMesh = mesh;
		GetComponent<MeshFilter>().sharedMesh.name = "myMesh";	

	}
	
	// Update is called once per frame
	void Update () {
		Vector2[] uv = GetComponent<MeshFilter>().sharedMesh.uv;
		for (int i = 0; i < uv.Length; i++)
		{
			uv[i].x += circleAnimationTime * Time.deltaTime;
		}
		GetComponent<MeshFilter>().sharedMesh.uv = uv;
	
	}
}
