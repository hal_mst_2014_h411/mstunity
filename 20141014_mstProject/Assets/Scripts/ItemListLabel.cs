﻿using UnityEngine;
using System.Collections;

public class ItemListLabel : MonoBehaviour {

	public TextMesh itemName;		//	名前
	public TextMesh num;		//	個数
	public TextMesh price;		//	値段
}
