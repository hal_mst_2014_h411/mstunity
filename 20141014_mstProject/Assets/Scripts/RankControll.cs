﻿using UnityEngine;
using System.Collections;

public class RankControll : MonoBehaviour {

	public TextMesh rankLabel;
	public FadeControll fade;

	// Use this for initialization
	void Start () {
		string rank = "A";
		int grossSales = ResultData.sales - ResultData.pay - ResultData.damage;

		if(grossSales < 1000){
			rank = "C";
		}
		else if(grossSales < 10000){
			rank = "B";
		}
		else if(grossSales < 100000){
			rank = "A";
		}
		else{
			rank = "S";
		}

		rankLabel.text = "ランク " + rank;
	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetKeyDown(KeyCode.Return)){
			fade.StartChangeScene("Result");
		}
	}
}
