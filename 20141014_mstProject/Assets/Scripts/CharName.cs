﻿using UnityEngine;
using System.Collections;

public class CharName : MonoBehaviour {

    public SpriteRenderer SetChar(int idx)
    {
        string name = "moji_" + idx;
        SpriteRenderer sr = gameObject.GetComponent<SpriteRenderer>();
        //Debug.Log(sr);
        Sprite[] sprites = Resources.LoadAll<Sprite>("moji");
        Sprite sp = System.Array.Find<Sprite>(sprites, (sprite) => sprite.name.Equals(name));
        sr.sprite = sp;
        //Debug.Log(sp);
        return sr;
    }

    public SpriteRenderer SetNumber(int idx)
    {
        string name = "suji_" + idx;
        SpriteRenderer sr = gameObject.GetComponent<SpriteRenderer>();
        //Debug.Log(sr);
        Sprite[] sprites = Resources.LoadAll<Sprite>("suji");
        Sprite sp = System.Array.Find<Sprite>(sprites, (sprite) => sprite.name.Equals(name));
        sr.sprite = sp;
        return sr;
    }
}
