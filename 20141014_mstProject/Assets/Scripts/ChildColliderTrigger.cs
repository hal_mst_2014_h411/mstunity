﻿using UnityEngine;
using System.Collections;

public class ChildColliderTrigger : MonoBehaviour {

	private GameObject parent;
	
	// Use this for initialization
	void Awake () {
		parent = gameObject.transform.parent.gameObject;
	}

	void Update(){
		renderer.material.mainTextureOffset += new Vector2(0.0f, 1.0f) * Time.deltaTime;
		if(renderer.material.mainTextureOffset.y >= 1.0f){
			renderer.material.mainTextureOffset -= new Vector2(0.0f, 1.0f);
		}
	}

	void OnTriggerEnter(Collider collider)
	{
		parent.SendMessage("RedirectedOnTriggerEnter", collider);
	}
	
	void OnTriggerStay(Collider collider)
	{
		parent.SendMessage("RedirectedOnTriggerStay", collider);
	}

	void OnTriggerExit(Collider collider)
	{
		parent.SendMessage("RedirectedOnTriggerExit", collider);
	}
}
