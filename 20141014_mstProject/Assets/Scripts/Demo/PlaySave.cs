﻿//---------------------------------------------------------------------------------------
// 
//---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------
// ライブラリ宣言
//--------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;            // テキスト関連処理

//==========================================================================
// PlaySaveクラス
//==========================================================================
public class PlaySave : MonoBehaviour {
    //----------------------------------
    // 定数
    //----------------------------------
    private const int SAVE_FRAME_MAX = 1200;

    //----------------------------------
    // 構造体
    //----------------------------------
    struct SaveKey
    {
        public bool LeftKey;
        public bool RightKey;
        public bool UpKey;
        public bool DownKey;
    };

    struct SpeedAdd
    {
        public float AxisX;
        public float AxisY;
    };

    //----------------------------------
    // 変数
    //----------------------------------
    private SaveKey[] SaveKeyData;
    private SpeedAdd[] SpeedAddData;

    private int FrameCount = 0;

    private string PathName = "";

    private bool StartPlayDataSaveFlag = false;

    //=====================================
	// 初期化
    //=====================================
	void Start () 
    {
        SaveKeyData = new SaveKey[SAVE_FRAME_MAX];
        SpeedAddData = new SpeedAdd[SAVE_FRAME_MAX];

        FrameCount = 0;

        PathName = "Assets/Resources/DemoText/DemoData.txt";

        StartPlayDataSaveFlag = false;
	}

    //=====================================
	// 更新
    //=====================================
	void Update () 
    {
        PlaySaveDebug();

        if (StartPlayDataSaveFlag)
        {
            if (FrameCount < SAVE_FRAME_MAX)
            {
                // 左右
                SaveKeyData[FrameCount].LeftKey = ControllerManager.CheckUnderAxis(ControllerManager.AXIS_STICK.X, 0.03f);
                SaveKeyData[FrameCount].RightKey = ControllerManager.CheckOverAxis(ControllerManager.AXIS_STICK.X, 0.03f);

                // 上下
                SaveKeyData[FrameCount].UpKey = ControllerManager.CheckUnderAxis(ControllerManager.AXIS_STICK.Y, 0.03f);
                SaveKeyData[FrameCount].DownKey = ControllerManager.CheckOverAxis(ControllerManager.AXIS_STICK.Y, 0.03f);
                 
                // 軸値保存
                SpeedAddData[FrameCount].AxisX = ControllerManager.GetAxis(ControllerManager.AXIS_STICK.X);
                SpeedAddData[FrameCount].AxisY = ControllerManager.GetAxis(ControllerManager.AXIS_STICK.Y);
                
                /*
                SaveKeyData[FrameCount].LeftKey = false;
                SaveKeyData[FrameCount].RightKey = false;

                // 上下
                SaveKeyData[FrameCount].UpKey = true;
                SaveKeyData[FrameCount].DownKey = false;

                // 軸値保存
                SpeedAddData[FrameCount].AxisX = 0.1f;
                SpeedAddData[FrameCount].AxisY = -0.1f;
                 */ 
 
                FrameCount++;
            }
            else if (FrameCount >= SAVE_FRAME_MAX)
            {
                DataWrite();
                StartPlayDataSaveFlag = false;
                Debug.Log("SaveEnd");
            }
        }
	}

    //=====================================
    // 保存したデータ書き出し
    //=====================================
    private void DataWrite()
    {
        FileStream File;
        //StreamWriter Writer;
        BinaryWriter Writer;

        // ファイル処理設定
        File = new FileStream(PathName, FileMode.Create, FileAccess.Write);
        //Writer = new StreamWriter(File, Encoding.UTF8);
        Writer = new BinaryWriter(File, Encoding.UTF8);

        // ファイルに書き込み開始
        for (int nCnt = 0; nCnt < SAVE_FRAME_MAX; nCnt++)
        {
            Writer.Write(SaveKeyData[nCnt].LeftKey);
            Writer.Write(SaveKeyData[nCnt].RightKey);
            Writer.Write(SaveKeyData[nCnt].UpKey);
            Writer.Write(SaveKeyData[nCnt].DownKey);

            Writer.Write((double)SpeedAddData[nCnt].AxisX);
            Writer.Write((double)SpeedAddData[nCnt].AxisY);

            /*
            Writer.WriteLine(SaveKeyData[nCnt].LeftKey);
            Writer.WriteLine(SaveKeyData[nCnt].RightKey);
            Writer.WriteLine(SaveKeyData[nCnt].UpKey);
            Writer.WriteLine(SaveKeyData[nCnt].DownKey);
             */
        }

        // ファイル書き込み処理終了
        Writer.Close();
        File.Close();
    }

    //=====================================
    // テキストファイル書き込み開始
    //=====================================
    public void StartPlayDataSave(bool StartFlag)
    {
        StartPlayDataSaveFlag = StartFlag;
    }

    //=====================================
    // デバック
    //=====================================
    private void PlaySaveDebug()
    {
        if (Input.GetKeyDown(KeyCode.Alpha9))
        {
            StartPlayDataSave(true);
            Debug.Log("SaveStart");
        }
    }
}
