﻿//---------------------------------------------------------------------------------------
// 
//---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------
// ライブラリ宣言
//--------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using System.IO;            // テキスト関連処理

//==========================================================================
// PlayLoadクラス
//==========================================================================
public class PlayLoad : MonoBehaviour {
    //----------------------------------
    // 定数
    //----------------------------------
    private const int PLAY_FRAME_MAX = 1200;    // 再生フレーム最大値

    //----------------------------------
    // 構造体
    //----------------------------------
    struct PlayKey
    {
        public bool LeftKey;
        public bool RightKey;
        public bool UpKey;
        public bool DownKey;
    };

    struct PlaySpeedAdd
    {
        public float AxisX;
        public float AxisY;
    };

    //----------------------------------
    // 変数
    //----------------------------------
    private PlayKey[] PlayKeyData;              // キー判定
    private PlaySpeedAdd[] PlaySpeedAddData;    // 軸値

    private int FrameCount = 0;                 // 再生フレーム数

    private string PathName = "";               // テキストパス

    private bool PlayStartFlag = false;         // 再生フラグ

    //=====================================
	// 初期化
    //=====================================
	void Start () 
    {
        // テキストデータ保存用
        PlayKeyData = new PlayKey[PLAY_FRAME_MAX];
        PlaySpeedAddData = new PlaySpeedAdd[PLAY_FRAME_MAX];

        // 保存されたデータ読み込み
        DatarReader();
	}

    //=====================================
	// 更新
    //=====================================
	void Update () 
    {
        // デモプレイ処理
        DemoPlayAirShip();
	}

    //=====================================
    // デモプレイ処理
    //=====================================
    private void DemoPlayAirShip()
    {
        if (PlayStartFlag)
        {
            if (FrameCount < PLAY_FRAME_MAX)
            {
                if (PlayKeyData[FrameCount].LeftKey)
                {
                    GameObject.Find("AirShip").rigidbody.AddRelativeForce(new Vector3(-0.0f, 0.0f, 0.0f) * Time.deltaTime * (0.3f + PlaySpeedAddData[FrameCount].AxisX * -1.0f));
                    GameObject.Find("AirShip").rigidbody.AddTorque(new Vector3(0.0f, -24.0f, 0.0f) * Time.deltaTime * (0.3f + PlaySpeedAddData[FrameCount].AxisX * -1.0f));
                }

                if (PlayKeyData[FrameCount].RightKey)
                {
                    GameObject.Find("AirShip").rigidbody.AddRelativeForce(new Vector3(0.0f, 0.0f, 0.0f) * Time.deltaTime * (0.3f + PlaySpeedAddData[FrameCount].AxisX * 1.0f));
                    GameObject.Find("AirShip").rigidbody.AddTorque(new Vector3(0.0f, 24.0f, -0.0f) * Time.deltaTime * (0.3f + PlaySpeedAddData[FrameCount].AxisX * 1.0f));
                }

                //	上下移動
                if (PlayKeyData[FrameCount].UpKey)
                {
                    GameObject.Find("AirShip").rigidbody.AddRelativeForce(new Vector3(0.0f, 0.0f, 240.0f) * Time.deltaTime);// (0.2f + Input.GetAxis("stickY") * -1.0f));

                }

                if (PlayKeyData[FrameCount].DownKey)
                {
                    GameObject.Find("AirShip").rigidbody.AddRelativeForce(new Vector3(0.0f, 0.0f, -240.0f) * Time.deltaTime);// * (0.2f + Input.GetAxis("stickY") * 1.0f));
                }
                FrameCount++;
            }
            else if (FrameCount >= PLAY_FRAME_MAX)
            {
                PlayStartFlag = false;
            }
        }
    }

    //=====================================
    // 保存したデータ読み込み
    //=====================================
    private void DatarReader()
    {
        FileStream File;
        BinaryReader Reader;
        int nCnt = 0;

        PathName = "Assets/Resources/DemoText/DemoData.txt";

        // ファイル読み込み設定
        File = new FileStream(PathName, FileMode.Open, FileAccess.Read);
        Reader = new BinaryReader(File);

        // 読み込み開始
        while (nCnt < PLAY_FRAME_MAX)
        {
            PlayKeyData[nCnt].LeftKey = Reader.ReadBoolean();
            PlayKeyData[nCnt].RightKey = Reader.ReadBoolean();
            PlayKeyData[nCnt].UpKey = Reader.ReadBoolean();
            PlayKeyData[nCnt].DownKey = Reader.ReadBoolean();

            PlaySpeedAddData[nCnt].AxisX = (float)Reader.ReadDouble();
            PlaySpeedAddData[nCnt].AxisY = (float)Reader.ReadDouble();
            nCnt++;
        }

        // 読み込み終了
        Reader.Close();
        File.Close();
    }


    //=====================================
    // デモ用AirShip処理開始
    //=====================================
    public void PlayStart(bool StartFlag)
    {
        PlayStartFlag = StartFlag;
    }
}
