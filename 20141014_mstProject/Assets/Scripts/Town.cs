﻿using UnityEngine;
using System.Collections;

public class Town : MonoBehaviour {

	public enum Type{
		None = 0,
		Australia,
		Japan,
		Singapore,
		China,
		Dobai,
	}

	//	買い取る値段の係数
	static private int[][] buyLevel = {
		new[]{0,		0,		0},
		new[]{10,		0,		0},
		new[]{10,		10,		10},
		new[]{0,		5,		10},
		new[]{10,		2,		10},
		new[]{10,		2,		10},
	};

	public bool landFlag = false;		//	着陸フラグ
	public string townName;				//	町の名前
	public Type type;
	public float landCount;

	//public AirShipLight airshipLight;
	private ItemManager itemManager;
	private DiscoveryMessage message;
	private float spanBay;
	private SEPlayer sePlayer;

	static private float GetSpan = 0.5f;	// 売れる秒数

	// Use this for initialization
	void Start () {
		itemManager = GameObject.Find("ItemManager").GetComponent<ItemManager>();
		message = GameObject.Find("DiscoveryMessage").GetComponent<DiscoveryMessage>();
		sePlayer = GameObject.Find("SEPlayer").GetComponent<SEPlayer>();
		//airshipLight = GameObject.Find("AirShipLight").GetComponent<AirShipLight>();
	}
	
	// Update is called once per frame
	void Update () {

		if(landFlag){
			if((int)landCount == 3){
				sePlayer.Play(SEPlayer.SE.Trade);
				message.MessageCreate("搬入中！！", 1.0f);
				//airshipLight.gameObject.SetActive(true);
			}

			if(landCount >= 3.0f){
				if(spanBay >= GetSpan){
					for(int i = 0; i < ItemManager.ItemMax; i++){
						itemManager.itemNumArray[i] += buyLevel[(int)type][i];
						ResultData.pay += buyLevel[(int)type][i] * 100;
						ScoreData.money -= buyLevel[(int)type][i] * 100;
						spanBay = 0.0f;
					}
				}
			}
			landCount += Time.deltaTime;
		}
		else {
			if(landCount >= 3.0f){
				message.SetMessageDestroyFlag(true);
			}
			/*if(airshipLight.gameObject.activeSelf){
				airshipLight.Reset();
				airshipLight.gameObject.SetActive(false);
			}*/
			landCount = 0;
		}

		spanBay += Time.deltaTime;
	}

	private void OnTriggerEnter(Collider col) {
		if(col.gameObject.CompareTag("AirShip")){
			//disMes
			sePlayer.Play(SEPlayer.SE.InTown);
			landFlag = true;
		}
	}

	private void OnTriggerExit(Collider col) {
		if(col.gameObject.CompareTag("AirShip")){
			landFlag = false;
			spanBay = 0.0f;
			/*if(airshipLight.gameObject.activeSelf){
				airshipLight.Reset();
				airshipLight.gameObject.SetActive(false);
			}*/
		}
	}
}
