﻿using UnityEngine;
using System.Collections;

public class FieldItem : MonoBehaviour {

	public Coin coinBase;
	public AreaCollider.Type areaType;
	public int itemType;
	public SpriteRenderer sprite;

	//	売る時の係数
	static private int[][] itemLevel = {
		new[]{0,		0,		0},
		new[]{1,		1,		1},
		new[]{1,		5,		1},
		new[]{1,		5,		1},
		new[]{100,		500,	100},
		new[]{10,		24,		10},
	};

	// Use this for initialization
	void Start () {
		rigidbody.AddForce(new Vector3(Random.Range(-500, 500), 1500.0f, Random.Range(-500, 500)));
	}
	
	// Update is called once per frame
	void Update () {
		rigidbody.AddForce(new Vector3(0.0f, -4000.0f, 0.0f) * Time.deltaTime);

		if(transform.localPosition.y <= -10.0f){
			SetItem();
		}
	}
	
	private void OnTriggerEnter(Collider col) {
		if(col.gameObject.tag == "Terrain"){
			SetItem();
		}
	}

	private void SetItem(){
		Coin coin = Instantiate(coinBase) as Coin;
		coin.gameObject.transform.localScale = new Vector3(1.0f, 0.1f, 1.0f);
		coin.gameObject.transform.parent = transform.parent;
		coin.gameObject.gameObject.transform.localPosition = transform.localPosition;
		coin.gameObject.SetActive(true);
		coin.price = itemLevel[(int)areaType][itemType] * 100;
		
		Destroy(gameObject);
	}
}
