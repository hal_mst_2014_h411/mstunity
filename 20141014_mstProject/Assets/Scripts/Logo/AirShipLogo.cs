﻿//---------------------------------------------------------------------------------------
// 
// 作成者：
// 最終更新：
//---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------
// ライブラリ宣言
//--------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

//==========================================================================
// AirShipLogoクラス
//==========================================================================
public class AirShipLogo : MonoBehaviour {
    //----------------------------------
    // 変数
    //----------------------------------
    public float moveZ;
    public float moveX;
    public float moveY;
    public float rotY;
    public float rotZ;
    public FadeControll fade;
    public GameObject m_goLogo;

    //=====================================
	// Use this for initialization
    //=====================================
	void Start () {
	
	}

    //=====================================
	// Update is called once per frame
    //=====================================
	void Update () {
        if (Input.GetKey("up"))
        {
            rigidbody.AddRelativeForce(new Vector3(0.0f, 0.0f, moveZ) * Time.deltaTime);
        }
        if (Input.GetKey("down"))
        {
            rigidbody.AddRelativeForce(new Vector3(0.0f, 0.0f, -moveZ) * Time.deltaTime);
        }

        transform.localPosition += new Vector3(0.05f,0,0);
        if (m_goLogo.transform.localPosition.x <= 0.0f)
        {
            m_goLogo.transform.localPosition += new Vector3(0.05f, 0, 0);
        }
        if (transform.localPosition.x >= 27.0f)
        {
            fade.StartChangeScene("Title");
        }
	}
}
