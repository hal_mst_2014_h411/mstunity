﻿using UnityEngine;
using System.Collections;

public class AreaCollider : MonoBehaviour {

	public enum Type{
		None = 0,		//	無し（当たってない）
		Australia,		//	オーストラリア
		Japan,			//	日本
		Singapore,
		China,
		Dobai,
		MAX,			//	最大値
	};

	public Type areaType;
	public AirShip airship;
	public bool landFlag = false;
	static private float saleSpan = 1.0f;

	private float saleCount = 0.0f;

	// Use this for initialization
	void Start () {
		airship = GameObject.Find("AirShip").GetComponent<AirShip>();
	}
	
	// Update is called once per frame
	void Update () {

		if(saleCount >= saleSpan){
			if(landFlag){
				airship.CreateSaleItem(areaType);
			}
			saleCount = 0.0f;
		}

		saleCount += Time.deltaTime;
	}

	void RedirectedOnTriggerEnter (Collider collider)
	{
		if(collider.tag == "AirShip"){
			landFlag = true;
		}

		if(collider.tag == "Town"){
			collider.GetComponent<Town>().type = (Town.Type)areaType;
		}
	}
	
	void RedirectedOnTriggerStay (Collider collider)
	{
		if(collider.tag == "AirShip"){
			landFlag = true;
		}
	}

	void RedirectedOnTriggerExit (Collider collider)
	{
		landFlag = false;
		//saleCount = 0.0f;
	}
}
