﻿//---------------------------------------------------------------------------------------
// 
// 作成者：
// 最終更新：
//---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------
// ライブラリ宣言
//--------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

//==========================================================================
// ResultControllクラス
//==========================================================================
public class ResultControll : MonoBehaviour {

	public FadeControll fade;
	
	public TextMesh grossSalesLabel;	//	総売上高
	public TextMesh[] labels;			//	文字の配列
	public int endTime;					//	１つのステート終わるフレーム数
	public SpriteRenderer rankSprite;
	public Sprite[] rankSprites;
	public AudioSource audiosource;
	public AudioClip seMoney;
	public AudioClip seResult;
	public AudioClip seUp;
	public VirtualInput input;

	private int[] resultNums = new int[4];			//	リザルトの値
	private int nowNum;
	private int state;
	private bool endFlag = false;
	private float rankCount;			//	ランク表示用カウント

    //=====================================
	// Use this for initialization
    //=====================================
	void Start () {
		state = 0;
		rankCount = 0;
	
		//	データを配列に格納
		resultNums[0] = ResultData.sales;
		resultNums[1] = ResultData.pay;
		resultNums[2] = ResultData.satisfy;
		resultNums[3] = ResultData.damage;

		//	総売り上げ
		int grossSales = ResultData.sales - ResultData.pay - ResultData.damage;
		grossSalesLabel.text = Common.WithComma(grossSales);
		grossSalesLabel.gameObject.SetActive(false);
		grossSalesLabel.transform.localScale = new Vector3(10.0f, 10.0f, 1.0f);

		//	ランク
		rankSprite.gameObject.SetActive(false);
		rankSprite.transform.localScale = new Vector3(10.0f, 10.0f, 1.0f);
		//	スコアによって文字変更
		if(grossSales < 1000){
			rankSprite.sprite = rankSprites[2];
		}
		else if(grossSales < 10000){
			rankSprite.sprite = rankSprites[1];
		}
		else{
			rankSprite.sprite = rankSprites[0];
		}
	}

    //=====================================
	// Update is called once per frame
    //=====================================
	void Update () {

		if(state < 4){
			CalcMoney();
			audiosource.PlayOneShot(seUp);
		}
		else if(state == 4){
			grossSalesLabel.gameObject.SetActive(true);
			grossSalesLabel.transform.localScale -= new Vector3(2.0f, 2.0f, 0.0f) * Time.deltaTime;
			if((grossSalesLabel.transform.localScale.x <= 1.0f) || (input.IsPushAnyKey())){
				grossSalesLabel.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
				state = 5;
				audiosource.PlayOneShot(seMoney);
			}
		}
		else if(state == 5){
			if(!endFlag){
				if(rankCount > 0.5f){
					rankSprite.gameObject.SetActive(true);
					rankSprite.transform.localScale -= new Vector3(25.0f, 25.0f, 0.0f) * Time.deltaTime;
					if ((rankSprite.transform.localScale.x <= 1.0f) || (input.IsPushAnyKey()))
					{
						rankSprite.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
						endFlag = true;
						audiosource.PlayOneShot(seResult);
					}
				}
				rankCount += Time.deltaTime;
			}
			else{
				//	シーンの変更
				if (input.IsPushAnyKey())
				{

                    if (judgment())
					{
						fade.StartChangeScene("RankIn");
					}
					else
					{
						fade.StartChangeScene("Ranking");
					}
				}
			}
		}
	}

    //=====================================
	//	カウントアップ用計算
    //=====================================
	private void CalcMoney(){
		if(nowNum >= resultNums[state]){
			labels[state].text = Common.WithComma(resultNums[state]);
			state ++;
			nowNum = 0;
			audiosource.PlayOneShot(seMoney);
		}
		else{
			labels[state].text = Common.WithComma(nowNum);
			nowNum += (int)((float)(resultNums[state] / endTime) * Time.deltaTime);
			if(input.IsPushAnyKey()){
				nowNum = resultNums[state];
				audiosource.PlayOneShot(seMoney);
			}
		}
	}

    //=====================================
    //
    //=====================================
    private bool judgment()
    {
        //データ全部呼び出し
        // StreamReader の新しいインスタンスを生成する
        System.IO.StreamReader cReader = (
            new System.IO.StreamReader(@"ranking.csv", System.Text.Encoding.Default)
        );

        // 読み込んだ結果をすべて格納するための変数を宣言する
        string stResult = string.Empty;
        RankingData[] data = new RankingData[10];

        // 読み込みできる文字がなくなるまで繰り返す
        int i = 0;
        while (cReader.Peek() >= 0)
        {
            // ファイルを 1 行ずつ読み込む
            string[] stBuffer = cReader.ReadLine().Split(',');
            // 読み込んだものを
            data[i].sName = stBuffer[0];
            data[i].nScore = int.Parse(stBuffer[1]);

            i++;
        }

        // cReader を閉じる (正しくは オブジェクトの破棄を保証する を参照)
        cReader.Close();

        //どこに入るのかを検討
        for (int j = 9; j >= 0; j--)
        {
            //実験用変数
			TradeCalculate.m_nMoney = ResultData.sales - ResultData.pay - ResultData.damage;
            //
            if (data[j].nScore < TradeCalculate.m_nMoney)
            {
                RankInControll.m_nScore = TradeCalculate.m_nMoney;
                return true;
            }
        }

        return false;
    }
}
