﻿using UnityEngine;
using System.Collections;

public class SEPlayer : MonoBehaviour {

	public enum SE{
		InTown = 0,
		Trade,
		AirshipCol,
		AirshipSale,
		AirshipCoin,
		GameEnd,
		CurMove,
		CountDown,
		MAX,
	}

	public AudioSource audiosource;
	public AudioClip[] seArray;

	public void Play(SE seType){
		audiosource.PlayOneShot(seArray[(int)seType]);
	}
}
