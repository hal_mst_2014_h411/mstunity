﻿using UnityEngine;
using System.Collections;
using System.Reflection;

public class GameControll : MonoBehaviour {
	
	//	定数
	public enum Scene{
		Main = 0,
		Trade,
	};

	public GameObject 	gameSceneParent;	//	ゲームシーンのオブジェクトの親
    public Gauge gauge;

	public FadeControll fade;
	public TradeCalculate trade;			//	売買実験（宮崎）

	public Sprite[] itemSprite;

	public VirtualInput input;

	private Scene scene;

	private int StartMoney = 100000;

	// Use this for initialization
	void Start () {
		//	リザルトデータの初期化
		ResultData.Init();
		//	スコアデータの初期化
		ScoreData.Init();
		ScoreData.money = StartMoney;

		GameObject town = GameObject.FindWithTag("Town");

		trade.InitMoney();
		trade.ReadAllMerchandiseData();
		trade.ReadCityData("日本");
		trade.StartTrade();

	}
	
	// Update is called once per frame
	void Update () {

		if(scene == Scene.Main){
			if(Input.GetKeyDown(KeyCode.Space)){
				trade.End();
				fade.StartChangeScene("Result");
			}
			//---デバッグリッセトコマンド---
			if (Input.GetKeyDown(KeyCode.F1) &&
				Input.GetKeyDown(KeyCode.F4))
			{
				fade.StartChangeScene("Title");
			}
            if (Input.GetKeyDown(KeyCode.Q))
            {
                ScoreData.money -= 1000;
            }
            if (Input.GetKeyDown(KeyCode.W))
            {
                ScoreData.money += 10000;
            }

		}
		else if(scene == Scene.Trade){

		}
		
        if (input.IsPushKey( VirtualInput.KEY.KEY_2 ))
        {
            gauge.SetUseFlag(true);
        }
	}

	//	ゲーム内シーンの変更
	public void ChangeGameScene(Scene type){
		if(type == Scene.Main){
			gameSceneParent.SetActive(true);
			scene = Scene.Main;
		}
		else if(type == Scene.Trade){
			gameSceneParent.SetActive(false);
			scene = Scene.Trade;
		}
	}
}
