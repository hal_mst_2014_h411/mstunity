﻿//---------------------------------------------------------------------------------------
// 入力デバイスの統合処理
// 作成者：kenmochi takafumi
// 最終更新：2014/12/23
// 補足：使い方はInspectorウインドゥでアサインして下さい
//       enumはVirtualInput.宣言名で呼べます
//		 軸の入力はキーボードを優先します(フラグ管理で切替あり)
//---------------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;


public class VirtualInput : MonoBehaviour
{
	public const bool PriorityKeyboard = true;	//軸の入力優先フラグ

	//パブリック変数
	public float ValueCorrespondToKeyInput = 0.5f;	//キー入力時の対応する軸量 
	public int InputInterval = 10;	//キー入力を弾く間隔
	public int MaxTrigger = 3;	//トリガー処理を行う最大回数
	public float AxisBorderForInput = 0.45f;	//軸の情報をデジタルデータにする際のしきい値

	//----------------------------------
	// 列挙体
	//----------------------------------
	public enum KEY
	{
		KEY_0 = 0,
		KEY_1,
		KEY_2,
		KEY_3,
		MAX_KEY
	};

	public enum AXIS_STICK
	{
		X = 0,
		Y,
		MAX_AXIS
	};

	public enum AXIS_STICK_CODE
	{
		X_PLUS = 0,
		X_MINUS,
		Y_PLUS,
		Y_MINUS,
		MAX_AXIS_CODE
	}

	//プライベート変数
	//キーボードとの対応
	private KeyCode[] CorrespondKey = new KeyCode[ (int)KEY.MAX_KEY ]{
		KeyCode.Z,
		KeyCode.X,
		KeyCode.C,
		KeyCode.V
	};
	private KeyCode[] AxisCorrespondKey = new KeyCode[(int)AXIS_STICK_CODE.MAX_AXIS_CODE]{
		KeyCode.RightArrow,
		KeyCode.LeftArrow,
		KeyCode.UpArrow,
		KeyCode.DownArrow
	};


	//コントローラとの対応
	private ControllerManager.KEY[] CorrespondController = new ControllerManager.KEY[(int)KEY.MAX_KEY]{
		ControllerManager.KEY.MARU,
		ControllerManager.KEY.BATU,
		ControllerManager.KEY.SANKAKU,
		ControllerManager.KEY.SIKAKU
	};

	//軸の検出閾値
	private const float AxisBorder = 0.3f;

	//キー入力を弾く最低間隔
	private int MinInputInterval = 2;	//2フレーム

	//キー入力
	//入力値の履歴
	private int[] PushTime = new int[(int)KEY.MAX_KEY];

	//入力値を返す値[現在の値][1フレーム前の値]
	private bool[] IsPush = new bool[(int)KEY.MAX_KEY];
	private bool[] IsOldPush = new bool[(int)KEY.MAX_KEY];

	//生の入力情報を所持する変数配列
	private bool[] IsOriginalPush = new bool[(int)KEY.MAX_KEY];
	private bool[] IsOriginalOldPush = new bool[(int)KEY.MAX_KEY];

	//トリガー処理を行った回数
	private int[] TriggerCount = new int[(int)KEY.MAX_KEY];

	//AXISの処理
	//軸の値
	private float[] AxisValue = new float[(int)AXIS_STICK.MAX_AXIS];
	//軸のキーのデジタル情報への変換
	//入力値の履歴
	private int[] AxisPushTime = new int[(int)AXIS_STICK_CODE.MAX_AXIS_CODE];
	//入力値を返す値[現在の値][1フレーム前の値]
	private bool[] IsAxisPush = new bool[(int)AXIS_STICK_CODE.MAX_AXIS_CODE];
	private bool[] IsAxisOldPush = new bool[(int)AXIS_STICK_CODE.MAX_AXIS_CODE];

	//生の入力情報を所持する変数配列
	private bool[] IsAxisOriginalPush = new bool[(int)AXIS_STICK_CODE.MAX_AXIS_CODE];
	private bool[] IsAxisOriginalOldPush = new bool[(int)AXIS_STICK_CODE.MAX_AXIS_CODE];

	//トリガー処理を行った回数
	private int[] AxisTriggerCount = new int[(int)AXIS_STICK_CODE.MAX_AXIS_CODE];

	// Use this for initialization
	void Start()
	{
		for (int i = 0; i < (int)KEY.MAX_KEY; i++)
		{
			PushTime[i] = 0;
			IsPush[i] = false;
			IsOldPush[i] = false;
			IsOriginalPush[i] = false;
			IsOriginalOldPush[i] = false;
			TriggerCount[i] = 0;
		}
		for (int i = 0; i < (int)AXIS_STICK.MAX_AXIS; i++)
		{
			AxisValue[i] = 0;
		}
		for (int i = 0; i < (int)AXIS_STICK_CODE.MAX_AXIS_CODE; i++)
		{
			AxisPushTime[i] = 0;
			IsAxisPush[i] = false;
			IsAxisOldPush[i] = false;
			IsAxisOriginalPush[i] = false;
			IsAxisOriginalOldPush[i] = false;
			AxisTriggerCount[i] = 0;
		}

	}

	// Update is called once per frame
	void Update()
	{
		//キー入力処理
		for (int i = 0; i < (int)KEY.MAX_KEY; i++)
		{
			IsOldPush[i] = IsPush[i];
			IsPush[i] = false;
			IsOriginalOldPush[i] = IsOriginalPush[i];
			IsOriginalPush[i] = false;
			if (ControllerManager.CheckKey(CorrespondController[i]) || Input.GetKeyDown(CorrespondKey[i]))
			{
				PushTime[i]++;	//キー入力時間を加算
				IsOriginalPush[i] = true;
			}
			else
			{
				//キー入力状態を初期化
				PushTime[i] = 0;
				TriggerCount[i] = 0;
			}

			//キー入力判定
			if (PushTime[i] > 0)
			{
				//トリガーカウントが最大数以下なら
				if (TriggerCount[i] < MaxTrigger)
				{
					//キーの入力インターバル
					if (PushTime[i] % InputInterval == 0 || PushTime[i] == 1)
					{
						IsPush[i] = true;
						TriggerCount[i]++;
					}
				}
				else
				{
					//キーの最小入力インターバル
					if (PushTime[i] % MinInputInterval == 0)
					{
						IsPush[i] = true;
						PushTime[i] = 0;
					}

				}

			}
			else
			{
				IsPush[i] = false;
				PushTime[i] = 0;
				TriggerCount[i] = 0;
			}
		}

		//軸情報の取得
		bool[] bPushKey = new bool[(int)AXIS_STICK_CODE.MAX_AXIS_CODE];
		for( int i = 0; i < (int)AXIS_STICK_CODE.MAX_AXIS_CODE; i++ ){
			bPushKey[i] = Input.GetKeyDown( AxisCorrespondKey[i] );
		}
		//禁則処理
		if( bPushKey[(int)AXIS_STICK_CODE.X_PLUS] || bPushKey[(int)AXIS_STICK_CODE.X_MINUS] ){
			bPushKey[(int)AXIS_STICK_CODE.X_PLUS] = 
			bPushKey[(int)AXIS_STICK_CODE.X_MINUS] = false;
		}
		if( bPushKey[(int)AXIS_STICK_CODE.Y_PLUS] || bPushKey[(int)AXIS_STICK_CODE.Y_MINUS] ){
			bPushKey[(int)AXIS_STICK_CODE.Y_PLUS] = 
			bPushKey[(int)AXIS_STICK_CODE.Y_MINUS] = false;
		}

		if (PriorityKeyboard == true)
		{
			//先にコントローラーの値を取得
			AxisValue[0] = ControllerManager.GetAxis(ControllerManager.AXIS_STICK.X);
			AxisValue[1] = ControllerManager.GetAxis(ControllerManager.AXIS_STICK.Y);

			//キーボードの値を設定
			if (bPushKey[(int)AXIS_STICK_CODE.X_PLUS])
			{
				AxisValue[0] = ValueCorrespondToKeyInput;
			}
			if (bPushKey[(int)AXIS_STICK_CODE.X_MINUS])
			{
				AxisValue[0] = -ValueCorrespondToKeyInput;
			}
			if (bPushKey[(int)AXIS_STICK_CODE.Y_PLUS])
			{
				AxisValue[1] = ValueCorrespondToKeyInput;
			}
			if (bPushKey[(int)AXIS_STICK_CODE.Y_MINUS])
			{
				AxisValue[1] = -ValueCorrespondToKeyInput;
			}

		}
		else
		{
			//先にキーボードの値を設定
			if (bPushKey[(int)AXIS_STICK_CODE.X_PLUS])
			{
				AxisValue[0] = ValueCorrespondToKeyInput;
			}
			if (bPushKey[(int)AXIS_STICK_CODE.X_MINUS])
			{
				AxisValue[0] = -ValueCorrespondToKeyInput;
			}
			if (bPushKey[(int)AXIS_STICK_CODE.Y_PLUS])
			{
				AxisValue[1] = ValueCorrespondToKeyInput;
			}
			if (bPushKey[(int)AXIS_STICK_CODE.Y_MINUS])
			{
				AxisValue[1] = -ValueCorrespondToKeyInput;
			}

			//コントローラーの値を取得
			AxisValue[0] = ControllerManager.GetAxis(ControllerManager.AXIS_STICK.X);
			AxisValue[1] = ControllerManager.GetAxis(ControllerManager.AXIS_STICK.Y);

		}
		//Axis入力処理
		for (int i = 0; i < (int)AXIS_STICK_CODE.MAX_AXIS_CODE; i++)
		{
			IsAxisOldPush[i] = IsAxisPush[i];
			IsAxisPush[i] = false;
			IsAxisOriginalOldPush[i] = IsAxisOriginalPush[i];
			IsAxisOriginalPush[i] = false;
		}
		if (AxisValue[0] > AxisBorderForInput)
		{
			AxisPushTime[(int)AXIS_STICK_CODE.X_PLUS]++;	//キー入力時間を加算
			IsAxisOriginalPush[(int)AXIS_STICK_CODE.X_PLUS] = true;
		}
		else
		{
			//キー入力状態を初期化
			AxisPushTime[(int)AXIS_STICK_CODE.X_PLUS] = 0;
			AxisTriggerCount[(int)AXIS_STICK_CODE.X_PLUS] = 0;
		}
		if (AxisValue[0] < -AxisBorderForInput)
		{
			AxisPushTime[(int)AXIS_STICK_CODE.X_MINUS]++;	//キー入力時間を加算
			IsAxisOriginalPush[(int)AXIS_STICK_CODE.X_MINUS] = true;
		}
		else
		{
			//キー入力状態を初期化
			AxisPushTime[(int)AXIS_STICK_CODE.X_MINUS] = 0;
			AxisTriggerCount[(int)AXIS_STICK_CODE.X_MINUS] = 0;
		}

		if (AxisValue[1] > AxisBorderForInput)
		{
			AxisPushTime[(int)AXIS_STICK_CODE.Y_PLUS]++;	//キー入力時間を加算
			IsAxisOriginalPush[(int)AXIS_STICK_CODE.Y_PLUS] = true;
		}
		else
		{
			//キー入力状態を初期化
			AxisPushTime[(int)AXIS_STICK_CODE.Y_PLUS] = 0;
			AxisTriggerCount[(int)AXIS_STICK_CODE.Y_PLUS] = 0;
		}

		if (AxisValue[1] < -AxisBorderForInput)
		{
			AxisPushTime[(int)AXIS_STICK_CODE.Y_MINUS]++;	//キー入力時間を加算
			IsAxisOriginalPush[(int)AXIS_STICK_CODE.Y_MINUS] = true;
		}

		else
		{
			//キー入力状態を初期化
			AxisPushTime[(int)AXIS_STICK_CODE.Y_MINUS] = 0;
			AxisTriggerCount[(int)AXIS_STICK_CODE.Y_MINUS] = 0;
		}

		for (int i = 0; i < (int)AXIS_STICK_CODE.MAX_AXIS_CODE; i++)
		{
			//キー入力判定
			if (AxisPushTime[i] > 0)
			{
				//トリガーカウントが最大数以下なら
				if (AxisTriggerCount[i] < MaxTrigger)
				{
					//キーの入力インターバル
					if (AxisPushTime[i] % InputInterval == 0 || AxisPushTime[i] == 1 )
					{
						IsAxisPush[i] = true;
						AxisTriggerCount[i]++;
					}
				}
				else
				{
					//キーの最小入力インターバル
					if (AxisPushTime[i] % MinInputInterval == 0)
					{
						IsAxisPush[i] = true;
						AxisPushTime[i] = 0;
					}

				}

			}
			else
			{
				IsAxisPush[i] = false;
				AxisPushTime[i] = 0;
				AxisTriggerCount[i] = 0;
			}
		}
	}

	//=====================================
	// キー押下取得
	// 引数：キーコード
	// 戻り値：true：入力されている false：入力されていない
	//=====================================
	public bool IsPushKey( KEY key )
	{
		if (IsPush[(int)key] == true)
		{
			return true;
		}
		return false;
	}

	//=====================================
	// キーのどれか押下取得
	// 戻り値：true：入力されている false：入力されていない
	//=====================================
	public bool IsPushAnyKey()
	{
		for (int i = 0; i < (int)KEY.MAX_KEY; i++)
		{
			if (IsPush[i] == true)
			{
				return true;
			}
		}
		return false;
	}

	//=====================================
	// キー押下瞬間取得
	// 引数：キーコード
	// 戻り値：true：入力されている false：入力されていない
	//=====================================
	public bool IsTriggerKey(KEY key)
	{
		if ((IsOriginalPush[(int)key] == true) && (IsOriginalOldPush[(int)key] == false))
		{
			return true;
		}
		return false;
	}

	//=====================================
	// キー開放瞬間取得
	// 引数：キーコード
	// 戻り値：bool
	//=====================================
	public bool IsReleaseKey( KEY key)
	{
		if ((IsOriginalPush[(int)key] == false) && (IsOriginalOldPush[(int)key] == true))
		{
			return true;
		}
		return false;
	}

	//=====================================
	// Axisの押下取得
	// 引数：キーコード
	// 戻り値：true：入力されている false：入力されていない
	//=====================================
	public bool IsPushAxis(AXIS_STICK_CODE code)
	{
		if (IsAxisPush[(int)code] == true)
		{
			return true;
		}
		return false;
	}

	//=====================================
	// Axisののどれか押下取得
	// 戻り値：true：入力されている false：入力されていない
	//=====================================
	public bool IsPushAnyAxis()
	{
		for (int i = 0; i < (int)AXIS_STICK_CODE.MAX_AXIS_CODE; i++)
		{
			if (IsAxisPush[i] == true)
			{
				return true;
			}
		}
		return false;
	}

	//=====================================
	// Axisの押下瞬間取得
	// 引数：キーコード
	// 戻り値：true：入力されている false：入力されていない
	//=====================================
	public bool IsTriggerAxis(AXIS_STICK_CODE code)
	{
		if ((IsAxisOriginalPush[(int)code] == true) && (IsAxisOriginalOldPush[(int)code] == false))
		{
			return true;
		}
		return false;
	}

	//=====================================
	// Axisの開放瞬間取得
	// 引数：キーコード
	// 戻り値：bool
	//=====================================
	public bool IsReleaseAxis(AXIS_STICK_CODE code)
	{
		if ((IsAxisOriginalPush[(int)code] == false) && (IsAxisOriginalOldPush[(int)code] == true))
		{
			return true;
		}
		return false;
	}

	//=====================================
	// 軸入力確認
	// 引数：軸、しきい値
	// 戻り値：true：入力されている false：入力されていない
	//=====================================
	public bool IsAxis(AXIS_STICK_CODE code, float Limit = AxisBorder)
	{
		if (code == AXIS_STICK_CODE.X_PLUS && AxisValue[0] >= Limit)
		{
			return (true);
		}
		if (code == AXIS_STICK_CODE.X_MINUS && AxisValue[0] <= -Limit)
		{
			return (true);
		}

		if (code == AXIS_STICK_CODE.Y_PLUS && AxisValue[1] >= Limit)
		{
			return (true);
		}
		if (code == AXIS_STICK_CODE.Y_PLUS && AxisValue[1] <= -Limit)
		{
			return (true);
		}

		return (false);
	}

	//=====================================
	// 軸値取得
	// 引数：軸
	// 戻り値：軸値
	//=====================================
	public float GetAxis(AXIS_STICK AxisStick)
	{
		if (AxisStick == AXIS_STICK.X)
		{
			return AxisValue[0];
		}
		else if (AxisStick == AXIS_STICK.Y)
		{
			return AxisValue[1];
		}

		// どれも当てはまらなければ0を返す
		return (0);
	}
	
    
}
