﻿//---------------------------------------------------------------------------------------
// 高度計処理
// 作成者：kasai tatushi
// 最終更新：2014/11/04
//---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------
// ライブラリ宣言
//--------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

//==========================================================================
// Altimeterクラス
//==========================================================================
public class Altimeter : MonoBehaviour
{
    //----------------------------------
    // 変数
    //----------------------------------
    private GameObject AirShip;
    private GameObject MeterHeight;

    private int nOldAirShipHeight = 0;
    private float fOldAirShipHeight = 0;
    private Vector2 Offset;
    private bool bMeterUpFlag = false;
    private bool bMeterDownFlag = false;

    //=====================================
    // 初期化
    //=====================================
	void Start () 
    {
        // 飛行船オブジェクト取得
        AirShip = GameObject.Find("AirShip");

        // メータオブジェクト取得
        MeterHeight = GameObject.Find("Height");
 
        // 前の高度保存(int型)
        nOldAirShipHeight = (int)AirShip.transform.position.y;

        // 前の高度保存(float型)
        fOldAirShipHeight = AirShip.transform.position.y;

        // guitextに座標値代入
        MeterHeight.guiText.text = ((int)AirShip.transform.position.y * 10).ToString();

        // Offset用変数
        Offset = new Vector2(0.0f, 0.0f);
	}

    //=====================================
    // 更新処理
    //=====================================
	void Update () 
    {
        if(nOldAirShipHeight < (int)AirShip.transform.position.y)
        {
            // 前の高度保存
            nOldAirShipHeight = (int)AirShip.transform.position.y;
            
            // guitextに座標値代入
            MeterHeight.guiText.text = ((int)AirShip.transform.position.y * 10).ToString();
        }
        else if (nOldAirShipHeight > (int)AirShip.transform.position.y)
        {
            // 前の高度保存
            nOldAirShipHeight = (int)AirShip.transform.position.y;

            // guitextに座標値代入
            MeterHeight.guiText.text = ((int)AirShip.transform.position.y * 10).ToString();
        }

        // 前の高さから変化がなければOffset処理終わり
        if (fOldAirShipHeight == AirShip.transform.position.y)
        {
            SetMeterUpFlag(false);

            SetMeterDownFlag(false);
        }
        else
        {
            // 前の高度保存
            fOldAirShipHeight = AirShip.transform.position.y;
        }

        // Offset処理
        if (bMeterUpFlag)
        {
            MeterUp();
        }
        else if (bMeterDownFlag)
        {
            MeterDown();
        }
	}

    //=====================================
    // 描画
    //=====================================
    void OnGUI()
    {
    }

    //=====================================
    // メーターUP
    //=====================================
    public void MeterUp()
    {
        Offset.y += 1.0f * Time.deltaTime;
        renderer.material.SetTextureOffset("_MainTex", Offset);
    }

    //=====================================
    // メーターDown
    //=====================================
    public void MeterDown()
    {
        Offset.y -= 1.0f * Time.deltaTime;
        renderer.material.SetTextureOffset("_MainTex", Offset);
    }

    //=====================================
    // メーターUPフラグ設定
    //=====================================
    public void SetMeterUpFlag(bool bFlag)
    {
        bMeterUpFlag = bFlag;
    }

    //=====================================
    // メータDownフラグ設定
    //=====================================
    public void SetMeterDownFlag(bool bFlag)
    {
        bMeterDownFlag = bFlag;
    }
}
