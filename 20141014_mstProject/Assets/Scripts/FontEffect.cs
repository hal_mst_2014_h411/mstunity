﻿using UnityEngine;
using System.Collections;

public class FontEffect : MonoBehaviour {

	public TextMesh text;
	private float count;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

		if(count < 0.0f){
			text.color = new Color(1.0f, 0.0f, 0.0f, 1.0f);
			count += Time.deltaTime;
		}
		else if(count > 0.1f){
			text.color = new Color(1.0f, 1.0f, 0.0f, 1.0f);
		}
	}
}
