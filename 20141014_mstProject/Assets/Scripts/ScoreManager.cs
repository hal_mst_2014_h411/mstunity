﻿//---------------------------------------------------------------------------------------
// GameシーンのScore関連処理
// 作成者：kasai tatushi
// 最終更新：2014/10/20
//---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------
// ライブラリ宣言
//--------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using System;

//==========================================================================
// ScoreManagerクラス
//==========================================================================
public class ScoreManager : MonoBehaviour {
    //----------------------------------
    // 定数
    //----------------------------------
    // fontsize定義
    private int AnimationFontSize = (int)(Screen.width * 0.06f);
    private int ScoreFontSize = (int)(Screen.width * 0.04f);
    private int ScoreLabelFontSize = (int)(Screen.width * 0.04f);

    //----------------------------------
    // 変数
    //----------------------------------
    // GUIText生成
    //public GUIText Score;

    // スコア格納
	private int m_nScore = ScoreData.money;

    // スコア加算、減算確認用
    private int m_nAddOrSub = 0;

    // アニメーションフラグ
    bool m_bScoreAnimaion = false;

    //スコアスプライト
    public GameObject m_goCamera;       //カメラオブジェクト
    public GameObject m_goSuji;         //文字作成のためのオブジェクト
    private SpriteRenderer[] m_srDraw = new SpriteRenderer[14];     //表記させるスクリプト
    private SpriteRenderer[] m_srNumber = new SpriteRenderer[12];   //素材スクリプト 10->マイナススクリプト　11->コンマ
    private Vector3 INIT_POS = new Vector3(-7.0f, 3.9f, 3);     //表記スクリプトの初期位置
    private Vector3 INIT_SCALE = new Vector3(2.0f, 2.5f, 1.0f); //表記スクリプトの初期大きさ
    private Vector3 ADD_NUMBER_SR = new Vector3(0.7f, 0, 0);    //数字をずらす値
    private Vector3 ADD_KONMA_SR = new Vector3(0.35f, 0, 0);    //コンマをずらす値

    //=====================================
    // 初期化
    //=====================================
	void Start () 
    {
        // スコア
        m_nScore = 0;

        // アニメーションフラグ
        m_bScoreAnimaion = false;

        // 加算、減算チェック
        m_nAddOrSub = 0;

        //スプライト初期化
        //数字スクリプト
        for (int i = 0; i < 10;i++ )
        {
            Vector3 pos = new Vector3(0, 0, 10);
            GameObject obj = Instantiate(m_goSuji) as GameObject;

            // 子に設定する
            obj.transform.parent = transform;
            obj.transform.localPosition = pos;
            obj.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            obj.transform.name = "number_" + i;

            CharName sc = obj.GetComponent<CharName>();
            m_srNumber[i] = sc.SetNumber(i);
        }
        //マイナススクリプト
        {
            Vector3 pos = new Vector3(0, 0, 10);
            GameObject obj = Instantiate(m_goSuji) as GameObject;

            // 子に設定する
            obj.transform.parent = transform;
            obj.transform.localPosition = pos;
            obj.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            obj.transform.name = "mainasu";

            CharName sc = obj.GetComponent<CharName>();
            m_srNumber[10] = sc.SetNumber(10);
        }
        //コンマスクリプト
        {
            Vector3 pos = new Vector3(0, 0, 10);
            GameObject obj = Instantiate(m_goSuji) as GameObject;

            // 子に設定する
            obj.transform.parent = transform;
            obj.transform.localPosition = pos;
            obj.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            obj.transform.name = "konma";

            CharName sc = obj.GetComponent<CharName>();
            m_srNumber[11] = sc.SetChar(28);
        }

        //表示スクリプト
        char[] sScore = string.Format("{0:#,##0}", m_nScore).ToCharArray();
        int nCount = 0;
        Vector3 posD = INIT_POS;
        for (int i = 0; i < m_srDraw.Length; i++)
        {
            GameObject obj = Instantiate(m_goSuji) as GameObject;

            // 子に設定する
            obj.transform.parent = m_goCamera.transform;
            obj.transform.localScale = INIT_SCALE;
            obj.transform.name = "score_" + i;

            if (nCount < sScore.Length)
            {
                if (sScore[nCount] == ',')
                {
                    posD -= new Vector3(0.1f, 0, 0);
                    obj.transform.localPosition = posD;
                    CharName sc = obj.GetComponent<CharName>();
                    m_srDraw[i] = sc.SetChar(28);
                    posD += ADD_KONMA_SR;
                }
                else
                {
                    obj.transform.localPosition = posD;
                    CharName sc = obj.GetComponent<CharName>();
                    m_srDraw[i] = sc.SetNumber(int.Parse(sScore[nCount].ToString()));
                    posD += ADD_NUMBER_SR;
                }
                nCount++;
            }
            else
            {
                obj.transform.localPosition = posD;
                CharName sc = obj.GetComponent<CharName>();
                m_srDraw[i] = sc.SetNumber(11);
                posD += ADD_NUMBER_SR;
            }
            m_srDraw[i].color = new Color(1.0f,1.0f,0.0f,1.0f);
        }
	}

    //=====================================
	// 更新
    //=====================================
	void Update () 
    {
        // 前回の金額保存
        int nOldScore = m_nScore;
		m_nScore = ScoreData.money;

        // デバッグ用スコア加算
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            m_nScore += 100;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            m_nScore -= 100;
        }

        // スコアが加算されれば
        if (m_nScore > nOldScore)
        {
            m_bScoreAnimaion = true;
            m_nAddOrSub = 0;
        }
        // スコアが減算されれば
        else if (m_nScore < nOldScore)
        {
            m_bScoreAnimaion = true;
            m_nAddOrSub++;
        }

        // アニメーション関数呼び出し
        if (m_bScoreAnimaion)
        {
            ScoreAnimation(Math.Abs(m_nScore - nOldScore));
            UpdateScript();
            m_bScoreAnimaion = false;
        }

        // 何かの条件でスコアの処理↓
		ScoreData.money = m_nScore;
	}

    //=====================================
    // 描画
    //=====================================
    void OnGUI()
    {
    }

    //=====================================
    // スコアアニメーション
    //=====================================
    void ScoreAnimation(int nMoney)
    {
        // スクリプト呼び出し
        scoreAnimation script = GetComponent<scoreAnimation>();

        /*　--完成したら削除するコメント--
            正式の場合はScoreに送る値は、加算値と減算値
            今は所持金を送っている
        */
        // 加算、減算で設定変更
        if (m_nAddOrSub == 0)
        {
            // アニメーション開始
            script.AnimationStart(new Vector3(0.35f, 0.95f, 0.0f), AnimationFontSize, new Color(1.0f, 1.0f, 0.0f, 1.0f), nMoney);
        }
        else
        {
            // アニメーション開始
            script.AnimationStart(new Vector3(0.35f, 0.95f, 0.0f), AnimationFontSize, new Color(1.0f, 0.0f, 0.0f, 1.0f), nMoney);
        }
    }

    //=====================================
    // スコアスクリプト
    //=====================================
    private void UpdateScript()
    {
        char[] sScore = string.Format("{0:#,##0}", m_nScore).ToCharArray();
        int nCount = 0;
        bool bFlag = false;
        //マイナス表記するか？
        if (sScore[0] == '-')
        {
            bFlag = true;
        }

        Vector3 posD = INIT_POS;
        for (int i = 0; i < m_srDraw.Length; i++)
        {
            //sScoreの配列インデックスがエラーを起こさないように。
            if (nCount < sScore.Length)
            {
                if (sScore[nCount] == '-')
                {
                    m_srDraw[i].transform.localPosition = posD;
                    m_srDraw[i].sprite = m_srNumber[10].sprite;
                    posD += ADD_NUMBER_SR;
                }
                else if (sScore[nCount] == ',')
                {
                    posD -= new Vector3(0.15f, 0, 0);
                    m_srDraw[i].transform.localPosition = posD;
                    m_srDraw[i].sprite = m_srNumber[11].sprite;
                    posD += ADD_KONMA_SR;
                }
                else
                {
                    m_srDraw[i].transform.localPosition = posD;
                    m_srDraw[i].sprite = m_srNumber[int.Parse(sScore[nCount].ToString())].sprite;
                    posD += ADD_NUMBER_SR;
                }
                nCount++;
            }
            else
            {
                //表記させる文字がないなら、画面外に飛ばす
                m_srDraw[i].transform.localPosition = new Vector3(-10, -10, -10);
            }
            //スコアがマイナスなら赤に、プラスなら黄色に
            if (bFlag)
            {
                m_srDraw[i].color = new Color(1.0f, 0.0f, 0.0f, 1.0f);
            }
            else
            {
                m_srDraw[i].color = new Color(1.0f, 1.0f, 0.0f, 1.0f);
            }
        }
    }
}
