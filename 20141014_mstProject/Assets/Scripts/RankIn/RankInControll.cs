﻿//---------------------------------------------------------------------------------------
// 
// 作成者：
// 最終更新：
//---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------
// ライブラリ宣言
//--------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

//==========================================================================
// RankInControllクラス
//==========================================================================
public class RankInControll : MonoBehaviour {
    //定数
    public const int MAX_NAME = 5;  //入力文字数最大値
    public Vector2 CENTER_POINT;    //円の中心位置
    public Vector2 INIT_POS;        //初期の文字の位置

    //変数
    public SpriteRenderer[] moji;   //選択表示文字（円環状のやつ）
    public SpriteRenderer m_srWaku; //枠
    public GameObject nameObject;   //選択文字表示オブジェ
    public FadeControll fade;       //フェード
    public static int m_nScore;     //スコア（得点）
	public SEPlayer sePlayer;		// SE再生するやつ
	public VirtualInput input;

    private float m_fGoalAngle;     //最終的に回したい角度
    private float m_fAngle;         //現在の角度
    private int m_nNumber;          //現在選択されている文字
    private int m_nNameNum;         //現在入力された文字数
    private bool[] m_bKeyFlag = new bool[2];        //入力フラグ
    private int[] m_nKeyCount = new int[2];         //入力フラグカウント
    
    private int[] m_anNameNum = new int[MAX_NAME];  //入力された文字番号を格納する
    public SpriteRenderer[] m_spUnder = new SpriteRenderer[MAX_NAME];   //アンダーバースプライト
    private bool m_bUnderFade;      //アンダーバースプライトの点滅フラグ
    //文字列
    private string[] m_asMoji = { "A", "B", "C", "D", "E", "F", "G",
                                    "H", "I", "J", "K", "L", "M", "N",
                                    "O", "P", "Q", "R", "S", "T", "U", 
                                    "V","W","X","Y","Z" };

    //=====================================
	// Use this for initialization
    //=====================================
	void Start () {
        //選択候補文字を円環状で表示させる
        float fAngle = 360.0f / (float)moji.Length;
        for (int i = 0; i < moji.Length;i++ )
        {
            Vector3 initPos = new Vector3(INIT_POS.x, INIT_POS.y, 2);
            float x, y;
            x = (initPos.x - CENTER_POINT.x) * (float)Mathf.Cos((float)i * fAngle * (2.0f * Mathf.PI / 360.0f)) - (initPos.y - CENTER_POINT.y) * (float)Mathf.Sin((float)i * fAngle * (2.0f * Mathf.PI / 360.0f)) + CENTER_POINT.x;
            y = (initPos.x - CENTER_POINT.x) * (float)Mathf.Sin((float)i * fAngle * (2.0f * Mathf.PI / 360.0f)) + (initPos.y - CENTER_POINT.y) * (float)Mathf.Cos((float)i * fAngle * (2.0f * Mathf.PI / 360.0f)) + CENTER_POINT.y;
            moji[i].transform.localPosition = new Vector3(x, y, 2);
        }
        //枠の位置調整
        m_srWaku.transform.localPosition = new Vector3(INIT_POS.x,INIT_POS.y,3);
        //回転処理初期化
        m_fGoalAngle = 0;
        m_fAngle = 0;
        m_nNumber = 0;
        m_nNameNum = 0;

        //名前番号初期化
        for (int i = 0; i < m_anNameNum.Length; i++)
        {
            m_anNameNum[i] = -1;
        }

        //入力関係初期化
        for(int i = 0;i < 2;i++)
        {
            m_bKeyFlag[i] = false;
            m_nKeyCount[i] = 0;
        }

        m_bUnderFade = true;

	}

    //=====================================
	// Update is called once per frame
    //=====================================
	void Update () {
        //回転処理
        float fAngle = (m_fGoalAngle - m_fAngle)*0.1f;
        for (int i = 0; i < moji.Length; i++)
        {
            float x, y;
            x = (moji[i].transform.localPosition.x - CENTER_POINT.x) * (float)Mathf.Cos(fAngle * (2.0f * Mathf.PI / 360.0f)) - (moji[i].transform.localPosition.y - CENTER_POINT.y) * (float)Mathf.Sin(fAngle * (2.0f * Mathf.PI / 360.0f)) + CENTER_POINT.x;
            y = (moji[i].transform.localPosition.x - CENTER_POINT.x) * (float)Mathf.Sin(fAngle * (2.0f * Mathf.PI / 360.0f)) + (moji[i].transform.localPosition.y - CENTER_POINT.y) * (float)Mathf.Cos(fAngle * (2.0f * Mathf.PI / 360.0f)) + CENTER_POINT.y;
            moji[i].transform.localPosition = new Vector3(x, y, 2);
        }
        m_fAngle += fAngle;

        if (Input.GetKeyDown("up"))
        {
            Revolution(1);
			sePlayer.Play(SEPlayer.SE.CurMove);
        }
        if (Input.GetKeyDown("down"))
        {
            Revolution(-1);
			sePlayer.Play(SEPlayer.SE.CurMove);
        }
     /*   if (Input.GetKey("right"))
        {
            Revolution(-1);
        }
        if (Input.GetKey("left"))
        {
            Revolution(1);
        }*/
        KeyCheck();
        if (m_bKeyFlag[0])
        {
            Revolution(-1);
        }
        if (m_bKeyFlag[1])
        {
            Revolution(1);
        }
//		if (Input.GetKeyDown(KeyCode.Return))
		if (input.IsTriggerKey(VirtualInput.KEY.KEY_0))
		{
            Decide();
			sePlayer.Play(SEPlayer.SE.AirshipSale);
        }
		if (input.IsTriggerKey(VirtualInput.KEY.KEY_3))
        {
            Delete();
        }

        //アンダーバー点滅処理
        UpdateUnder();
    }

    //=====================================
    //選択変更
    //=====================================
    public void Revolution(int nNum)
    {
        m_fGoalAngle -= (float)nNum * (360.0f / (float)moji.Length);
        m_nNumber += nNum;
        
        if (m_nNumber < 0)
        {
            m_nNumber = moji.Length - 1;
        }
        m_nNumber %= moji.Length;
    }

    //=====================================
    //決定
    //=====================================
    public void Decide()
    {
        //終わりを決定したとき
        if (m_nNumber == (moji.Length - 1))
        {
            GoNextScene();
            return;
        }
        //文字が５文字以上入力できないようにする処理
        if (m_nNameNum > 4)
        {
            return;
        }
       

        Vector3 pos = new Vector3(-1.8f + (float)m_nNameNum * 0.9f, 0.0f, 1);
        GameObject obj = Instantiate(nameObject) as GameObject;
        
        // 子に設定する
        obj.transform.parent = transform;
        obj.transform.localPosition = pos;
        obj.transform.localScale = new Vector3(3, 3, 1);
        obj.transform.name = "name_" + m_nNameNum;

        CharName sc = obj.GetComponent<CharName>();
        sc.SetChar(m_nNumber);
        m_anNameNum[m_nNameNum] = m_nNumber;

        //下線点滅処理
        m_spUnder[m_nNameNum].color = new Color(m_spUnder[m_nNameNum].color.r, m_spUnder[m_nNameNum].color.g, m_spUnder[m_nNameNum].color.b, 1.0f);
        m_nNameNum++;

        //「終了」に移動
        //回転方向を計算
        if (m_nNameNum > 4)
        {
            if (m_nNumber > (moji.Length/2))
            {
                Revolution(moji.Length - m_nNumber -1);
            }
            else
            {
                Revolution(-m_nNumber-1);
            }
        }
    }

    //=====================================
    //文字削除
    //=====================================
    public void Delete()
    {
        if (m_nNameNum < 0)
        {
            return;
        }
        GameObject obj = GameObject.Find("name_" + (m_nNameNum-1));

        if (obj != null)
        {
            Destroy(obj);
            if (m_nNameNum < MAX_NAME)
            {
                m_spUnder[m_nNameNum].color = new Color(m_spUnder[m_nNameNum].color.r, m_spUnder[m_nNameNum].color.g, m_spUnder[m_nNameNum].color.b, 1.0f);
            }
            m_nNameNum--;
            m_anNameNum[m_nNameNum] = -1;
        }
    }

    //=====================================
    //ランキングシーンへ行く前の処理
    //=====================================
    private void GoNextScene()
    {
        //ファイルに保存
        //データ全部呼び出し
        // StreamReader の新しいインスタンスを生成する
        System.IO.StreamReader cReader = (
            new System.IO.StreamReader(@"ranking.csv", System.Text.Encoding.Default)
        );

        // 読み込んだ結果をすべて格納するための変数を宣言する
        string stResult = string.Empty;
        RankingData[] data = new RankingData[10];

        // 読み込みできる文字がなくなるまで繰り返す
        int i = 0;
        while (cReader.Peek() >= 0)
        {
            // ファイルを 1 行ずつ読み込む
            string[] stBuffer = cReader.ReadLine().Split(',');
            // 読み込んだものを
            data[i].sName = stBuffer[0];
            data[i].nScore = int.Parse(stBuffer[1]);
            
            //名前（画像用）
            data[i].aName = new int[5];
            for (int j = 0; j < 5; j++)
            {
                data[i].aName[j] = int.Parse(stBuffer[j + 2]);
            }

            //スコア（画像用）
            data[i].aScore = new int[10];
            for (int j = 0; j < 10; j++)
            {
                data[i].aScore[j] = int.Parse(stBuffer[j + 7]);
            }
            i++;
        }

        // cReader を閉じる (正しくは オブジェクトの破棄を保証する を参照)
        cReader.Close();

        //どこに入るのかを検討
        int nRank = -1;
        for (int j = 0; j < 10; j++)
        {
            if (data[j].nScore < m_nScore)
            {
                nRank = j;
                break;
            }
        }

        if (nRank == -1)
        {
            //ランキングシーンへ移行
            RankingControll.m_nRankIn = nRank;
            fade.StartChangeScene("Ranking");
            return;
        }

        //新たにランクインするデータを挿入
        //データをずらす
        for (int j = 9; j > nRank; j--)
        {
            data[j] = data[j - 1];
        }
        //Debug.Log(nRank);
        //名前
        data[nRank].sName = "";
        for (int j = 0; j < MAX_NAME; j++)
        {
            if (m_anNameNum[j] != -1)
            {
                data[nRank].sName += m_asMoji[m_anNameNum[j]];
            }
        }
        //スコア
        data[nRank].nScore = m_nScore;
        //名前（描画用数字）
        data[nRank].aName = new int[MAX_NAME];
        data[nRank].aName = m_anNameNum;
        //スコア（描画用数字）
        data[nRank].aScore = new int[10];
        int nScore = m_nScore;
        for (int j = 9; j >= 0;j-- )
        {
            data[nRank].aScore[j] = nScore % 10;
            nScore /= 10;
        }
        //00009000等の上位桁の0を表示させないための処理
        for (int j = 0; j < RankingControll.MAX_SCORENUM; j++)
        {
            if (data[nRank].aScore[j] == 0)
            {
                data[nRank].aScore[j] = -1;
            }
            else
            {
                break;
            }
        }

        //ファイルに書き込み
        //ファイルを上書きし、Shift JISで書き込む
        System.IO.StreamWriter sw = new System.IO.StreamWriter(
            @"ranking.csv",
            false,
            System.Text.Encoding.GetEncoding("shift_jis"));
        //書き込み
        for (int j = 0; j < 10; j++)
        {
            string sBuff = "";
            sBuff += data[j].sName;
            sBuff += ",";
            sBuff += data[j].nScore.ToString();
            sBuff += ",";
            for (int k = 0; k < MAX_NAME; k++)
            {
                sBuff += data[j].aName[k].ToString();
                sBuff += ",";
            }
            for (int k = 0; k < 10; k++)
            {
                sBuff += data[j].aScore[k].ToString();
                sBuff += ",";
            }
            sw.WriteLine(sBuff);
        }
        //閉じる
        sw.Close();

        //ランキングシーンへ移行
        RankingControll.m_nRankIn = nRank;
        fade.StartChangeScene("Ranking");
    }

    //=====================================
    //キー入力処理
    //=====================================
    private void KeyCheck()
    {
		if (input.IsPushAxis(VirtualInput.AXIS_STICK_CODE.X_PLUS))
        {
            if (m_nKeyCount[0] > 90)
            {
                if (m_nKeyCount[0] % 10 == 0)
                {
                    m_bKeyFlag[0] = true;
					sePlayer.Play(SEPlayer.SE.CurMove);
                }
                else
                {
                    m_bKeyFlag[0] = false;
                }
            }
            else if (m_nKeyCount[0] % 30 == 0)
            {
				sePlayer.Play(SEPlayer.SE.CurMove);
                m_bKeyFlag[0] = true;
            }
            else
            {
                m_bKeyFlag[0] = false;
            }
            m_nKeyCount[0]++;
        }
        else
        {
            m_nKeyCount[0] = 0;
            m_bKeyFlag[0] = false;
        }

		if (input.IsPushAxis(VirtualInput.AXIS_STICK_CODE.X_MINUS))
        {
            if (m_nKeyCount[1] > 90)
            {
                if (m_nKeyCount[1] % 10 == 0)
                {
                    m_bKeyFlag[1] = true;
                }
                else
                {
                    m_bKeyFlag[1] = false;
                }
            }
            else if (m_nKeyCount[1] % 30 == 0)
            {
                m_bKeyFlag[1] = true;
				sePlayer.Play(SEPlayer.SE.CurMove);
            }
            else
            {
                m_bKeyFlag[1] = false;
            }
            m_nKeyCount[1]++;
        }
        else
        {
            m_nKeyCount[1] = 0;
            m_bKeyFlag[1] = false;
        }
    }

    //=====================================
    //アンダーバー点滅処理
    //=====================================
    private void UpdateUnder()
    {
        //文字入力がMAX以下なら
        if (m_nNameNum < MAX_NAME)
        {
            //点滅処理
            //フェードインかアウトか
            if (m_bUnderFade)
            {
                m_spUnder[m_nNameNum].color -= new Color(0.0f, 0.0f, 0.0f, 0.025f);
                if (m_spUnder[m_nNameNum].color.a <= 0.0f)
                {
                    m_spUnder[m_nNameNum].color = new Color(m_spUnder[m_nNameNum].color.r,m_spUnder[m_nNameNum].color.g,m_spUnder[m_nNameNum].color.b,0.0f);
                    m_bUnderFade = false;
                }
            }
            else
            {
                m_spUnder[m_nNameNum].color += new Color(0.0f, 0.0f, 0.0f, 0.025f);
                if (m_spUnder[m_nNameNum].color.a >= 1.0f)
                {
                    m_spUnder[m_nNameNum].color = new Color(m_spUnder[m_nNameNum].color.r, m_spUnder[m_nNameNum].color.g, m_spUnder[m_nNameNum].color.b, 1.0f);
                    m_bUnderFade = true;
                }
            }
            
        }
    }
}
