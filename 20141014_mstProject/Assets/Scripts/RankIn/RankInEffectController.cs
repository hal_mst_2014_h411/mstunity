﻿//---------------------------------------------------------------------------------------
// 
// 作成者：
// 最終更新：
//---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------
// ライブラリ宣言
//--------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

//==========================================================================
// RankInEffectControllerクラス
//==========================================================================
public class RankInEffectController : MonoBehaviour {
	private int countTime = 0;
	private int countCreate = 0;
	public int maxObj;
	public int createOnce;
	public int createSpan;

	public Transform coins;

    //=====================================
	// Use this for initialization
    //=====================================
	void Start () {
		countTime = 0;
		countCreate = 0;
	}

    //=====================================
	// Update is called once per frame
    //=====================================
	void Update () {
		if (countCreate < maxObj)
		{
			if (countTime % createSpan == 0)
			{
				for (int i = 0; i < createOnce; i++)
				{
					// プレハブからインスタンスを生成
					Instantiate(coins, new Vector3(	Random.Range(-2.0f, 2.0f) + transform.position.x,
													Random.Range(2.0f, 4.0f) + transform.position.y,
													Random.Range(-2.0f, 2.0f) + transform.position.z), Quaternion.identity);
					countCreate++;
				}
				countTime = 0;
			}
			countTime++;
		}
	}
}
