﻿using UnityEngine;
using System.Collections;

using Collections;//リスト構造を使うための宣言

//===============================================
//構造体
//===============================================
//
struct messData
{
    public string sMessage;
    public int nSpeed;
    public bool bFlag;
}
public class FieldMessage : MonoBehaviour {
    //定数
    public const int MESS_NONE = 0;         //メッセージ処理番号
    public const int MESS_IN = 1;           //メッセージ処理番号
    public const int MESS_OUT = 2;          //メッセージ処理番号
    public const int MESS_FLOW = 3;         //メッセージ処理番号
    public const int MESS_INITPOS = 450;    //メッセージ出現初期位置
    private const int MAX_MESS = 10;        //メッセージストック最大数

    //変数
    private static int m_nState;        //メッセージの現在の処理番号
    private static string m_sMessage;   //メッセージ文字
    private static messData[] m_atagMessage = new messData[MAX_MESS];   //メッセージをストックする変数
    private static int m_nStockNum;     //現在ストックされている最後の配列番号
    private static int m_nFlowNum;      //今現在流れているメッセージの配列番号
    public SpriteRenderer sprite;       //メッセージを表示させる帯
    public float m_fAddAlpha = 0.1f;    //フェード用のアルファ値
    private GUIStyle m_tagStyle;        //表示させるメッセージの文字（フォントや大きさ）設定変数
    private static int m_nMessPosX;     //メッセージの位置
    private static int m_nMessSpeed;    //メッセージのスピード

    private const float NORMAL_WIDTH = 404.0f;  //スクリーンの基本大き（横）
    private const float NORMAL_HEIGHT = 269.0f; //スクリーンの基本大き（縦）
    private static float m_fScaleW;             //スクリーンの大きさ倍率（横）
    private static float m_fScaleH;             //スクリーンの大きさ倍率（縦）

	// Use this for initialization
	void Start () {
        m_nState = MESS_NONE;
        sprite = GetComponent<SpriteRenderer>();
        sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, 0.0f);
        m_tagStyle = new GUIStyle();
        m_tagStyle.normal.textColor = new Color(Color.black.r,Color.black.g,Color.black.b,0.5f);
        m_nMessPosX = MESS_INITPOS;
        m_nMessSpeed = 1;
        m_nStockNum = 0;
        m_nFlowNum = 0;
        for (int i = 0; i < MAX_MESS;i++ )
        {
            m_atagMessage[i].bFlag = false;
        }

        m_fScaleW = Screen.width / NORMAL_WIDTH;
        m_fScaleH = Screen.height / NORMAL_HEIGHT;
        m_tagStyle.fontSize = 20;
        m_tagStyle.fontSize = (int)((float)m_tagStyle.fontSize * m_fScaleH);
        m_nMessPosX = (int)((float)m_nMessPosX * m_fScaleW);
        m_nMessSpeed = (int)((float)m_nMessSpeed * m_fScaleW);
	}
	
	// Update is called once per frame
	void Update () {
        switch (m_nState)
        {
            case MESS_NONE:
                break;

            case MESS_OUT:
                sprite.color -= new Color(0.0f, 0.0f, 0.0f, m_fAddAlpha);
                if (sprite.color.a <= 0.0f)
                {
                    sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, 0.0f);
                    m_nState = MESS_NONE;
                }
                break;

            case MESS_IN:
                sprite.color += new Color(0.0f, 0.0f, 0.0f, m_fAddAlpha);
                if (sprite.color.a >= 0.5f)
                {
                    sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, 0.5f);
                    m_nState = MESS_FLOW;
                }
                break;
            case MESS_FLOW:
                m_nMessPosX -= m_atagMessage[m_nFlowNum].nSpeed;
                if (m_nMessPosX < -((float)(m_atagMessage[m_nFlowNum].sMessage.Length * 16 + (m_atagMessage[m_nFlowNum].sMessage.Length-1)*6)*m_fScaleH))
                {
                    m_atagMessage[m_nFlowNum].bFlag = false;
                    if (m_atagMessage[(m_nFlowNum + 1) % MAX_MESS].bFlag)
                    {
                        m_nFlowNum = (m_nFlowNum + 1) % MAX_MESS;
                        m_nMessPosX = (int)((float)MESS_INITPOS * m_fScaleW);
                    }
                    else
                    {
                        m_nState = MESS_OUT;
                    }
                }
                /*if(Input.GetKeyDown(KeyCode.N))
                {
                    m_nMessPosX++;
                }
                if (Input.GetKeyDown(KeyCode.B))
                {
                    m_nMessPosX--;
                }*/
                break;
        }
	}

    public static bool SetMessage(string sMessage,int nSpeed)
    {
        //メッセージがもう入らないとき
        if (m_atagMessage[m_nStockNum].bFlag)
        {
            //表示できません
            return false;
        }

        //メッセージがストックされてあって、メッセージが表示されていいなければ・・・
        if (!m_atagMessage[m_nStockNum].bFlag && (m_nState == MESS_NONE || m_nState == MESS_OUT))
        {
            //表示させる
            m_nState = MESS_IN;
            m_atagMessage[m_nStockNum].sMessage = sMessage;
            m_atagMessage[m_nStockNum].nSpeed = (int)((float)nSpeed * m_fScaleW);
            m_atagMessage[m_nStockNum].bFlag = true;
            m_nFlowNum = m_nStockNum;
            m_nStockNum++;
            m_nStockNum %= MAX_MESS;
            m_nMessPosX = (int)((float)MESS_INITPOS * m_fScaleW);
        }
        else
        {
            //ストックさせるだけ
            m_atagMessage[m_nStockNum].sMessage = sMessage;
            m_atagMessage[m_nStockNum].nSpeed = (int)((float)nSpeed * m_fScaleW); ;
            m_atagMessage[m_nStockNum].bFlag = true;
            m_nStockNum++;
            m_nStockNum %= MAX_MESS;
        }

        return true;
    }

    //表示//10intで約1文字、文字間3int
    public void OnGUI()
    {
    }
}
