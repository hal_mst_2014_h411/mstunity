﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour {
	
	public GameObject airship;
	public int price = 0;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		transform.position += ((airship.transform.position - transform.position) * 2.0f) * Time.deltaTime;
		transform.localEulerAngles += new Vector3(360.0f, 360.0f, 0.0f) * Time.deltaTime;
	}
}