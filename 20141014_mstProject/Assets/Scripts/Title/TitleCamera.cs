﻿//---------------------------------------------------------------------------------------
// 
// 作成者：
// 最終更新：
//---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------
// ライブラリ宣言
//--------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

//==========================================================================
// TitleCameraクラス
//==========================================================================
public class TitleCamera : MonoBehaviour {
    //----------------------------------
    // 定数
    //----------------------------------
    private const float FADE_IN_LIMIT = 5.0f;

    //----------------------------------
    // 変数
    //----------------------------------
    private float nCount = 0;

    //=====================================
	// 初期化
    //=====================================
	void Start () 
    {
        nCount = FADE_IN_LIMIT;
	}

    //=====================================
	// 更新
    //=====================================
	void Update () 
    {
        Vector3 pos;
        
        // カメラの座標変更
        pos = GameObject.Find("Main Camera").transform.position;
        pos.z -= 30.0f * Time.deltaTime;
        GameObject.Find("Main Camera").transform.position = pos;

        // 時間調整
        if (nCount > 0.0f && GameObject.Find("title").GetComponent<TitleRogo>().GetRogoStartFlag() != true)
        {
            nCount -= Time.deltaTime;
        }

        // 一定の時間が立ったらフェードインする
        if (nCount <= 0.0f &&
            GameObject.Find("title").GetComponent<TitleRogo>().GetRogoStartFlag() != true)
        {
            GameObject.Find("title").GetComponent<TitleRogo>().SetRogoStartFlag(true);
            GameObject.Find("EnyKey").GetComponent<TitleEnyKey>().SetEnyKeyStartFlag(true);
            GameObject.Find("EnyKeyBack").GetComponent<EnyKeyBack>().SetEnyKeyBackStartFlag(true);
        }
	}
}
