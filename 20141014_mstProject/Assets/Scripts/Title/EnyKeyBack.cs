﻿//---------------------------------------------------------------------------------------
// 
// 作成者：
// 最終更新：
//---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------
// ライブラリ宣言
//--------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

//==========================================================================
// EnyKeyBackクラス
//==========================================================================
public class EnyKeyBack : MonoBehaviour {

    //----------------------------------
    // 変数
    //----------------------------------
    private bool bStartEnyKeyBackFlag = false;

    //=====================================
    // 初期化
    //=====================================
	void Start () 
    {
        bStartEnyKeyBackFlag = false;
	}

    //=====================================
    // 更新
    //=====================================
	void Update () 
    {
        if (bStartEnyKeyBackFlag)
        {
            if (this.GetComponent<SpriteRenderer>().color.a != 1.0f)
            {
                Color color;
                color = this.GetComponent<SpriteRenderer>().color;
                color.a += 0.5f * Time.deltaTime;
                this.GetComponent<SpriteRenderer>().color = color;
            }
        }
	}

    //=====================================
    // フェードスタートフラグ設定
    //=====================================
    public void SetEnyKeyBackStartFlag(bool flag)
    {
        bStartEnyKeyBackFlag = flag;
    }

    //=====================================
    // フェードスタートフラグ取得
    //=====================================
    public bool GetEnyKeyBackSStartFlag()
    {
        return (bStartEnyKeyBackFlag);
    }
}
