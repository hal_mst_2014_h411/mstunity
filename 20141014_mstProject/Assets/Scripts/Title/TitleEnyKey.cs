﻿//---------------------------------------------------------------------------------------
// 
// 作成者：
// 最終更新：
//---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------
// ライブラリ宣言
//--------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

//==========================================================================
// TitleEnyKeyクラス
//==========================================================================
public class TitleEnyKey : MonoBehaviour {

    //----------------------------------
    // 定数
    //----------------------------------

    //----------------------------------
    // 変数
    //----------------------------------
    // color値をいじるためにSpriteRenderer取得
    private SpriteRenderer EnyKey;
    private Color Color;
    private bool AlphaChange;
    private bool StartEnyKeyFlag;

    //=====================================
	// 初期化
    //=====================================
	void Start () 
    {
        EnyKey = GetComponent<SpriteRenderer>();
        Color = EnyKey.color;
        AlphaChange = false;

        StartEnyKeyFlag = false;
    }

    //=====================================
	// 更新
    //=====================================
	void Update () 
    {
        if (StartEnyKeyFlag)
        {
            Flashing();
        }
	}

    //=====================================
    // 点滅処理
    //=====================================
    private void Flashing()
    {
        if (AlphaChange)
        {
            if (Color.a >= 1.0f)
            {
                AlphaChange = false;
            }
            else
            {
                Color.a += 0.5f * Time.deltaTime;
                EnyKey.color = Color;
            }
        }
        else
        {
            if (Color.a <= 0.0f)
            {
                AlphaChange = true;
            }
            else
            {
                Color.a -= 0.5f * Time.deltaTime;
                EnyKey.color = Color;
            }
        }
    }

    //=====================================
    // フェードスタートフラグ設定
    //=====================================
    public void SetEnyKeyStartFlag(bool flag)
    {
        StartEnyKeyFlag = flag;
    }

    //=====================================
    // フェードスタートフラグ取得
    //=====================================
    public bool GetEnyKeyStartFlag()
    {
        return (StartEnyKeyFlag);
    }
}
