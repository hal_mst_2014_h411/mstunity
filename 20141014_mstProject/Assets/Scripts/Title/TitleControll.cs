﻿//---------------------------------------------------------------------------------------
// 
// 作成者：
// 最終更新：
//---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------
// ライブラリ宣言
//--------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

//==========================================================================
// TitleControllクラス
//==========================================================================
public class TitleControll : MonoBehaviour {
    //----------------------------------
    // 定数
    //----------------------------------
    private const float fStopTimeLimit = 15.0f;

    //----------------------------------
    // 変数
    //----------------------------------
    // オブジェクト生成
    public FadeControll fade;
	public AudioSource audiosource;
	public VirtualInput input;
    private GameObject Fade;

    // 待機時間
    private float m_fStopTime = 0.0f;
    private static int nChangeScene = 0;

    //=====================================
	// 初期化
    //=====================================
	void Start () 
    {
        m_fStopTime = fStopTimeLimit;	
	}

    //=====================================
	// 更新
    //=====================================
	void Update () 
    {
        // 待機時間処理
        StopTimeCount();
	
		//	シーンの変更
		if (input.IsPushAnyKey())
        {
			if(fade.state == FadeControll.STATE_NONE){
				audiosource.PlayOneShot(audiosource.clip);
			}
			SceneChange("GAME");
		}
        if (Input.GetKeyDown(KeyCode.Q))
        {
            SceneChange("RANKING");
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            SceneChange("TUTORIAL");
        }
	}

    //=====================================
    // 待機時間処理
    //=====================================
    void StopTimeCount()
    {
        // 待機時間カウント
        if (m_fStopTime > 0.0f)
        {
            m_fStopTime -= Time.deltaTime;
        }
        // 待機時間による遷移処理
        else if (m_fStopTime <= 0.0f)
        {
            // 変数の値によって遷移変化
            if (nChangeScene == 0)
            {
                nChangeScene++;
                SceneChange("DEMO");
            }
            else
            {
                nChangeScene = 0;
                SceneChange("RANKING");
            }
        }
    }

    //=====================================
    // 遷移処理
    //=====================================
    void SceneChange(string SceneName)
    {
        // 引数の値ごとに対応する画面へ遷移
        if (SceneName == "GAME")
        {
            fade.StartChangeScene("Game");
        }
        else if (SceneName == "TUTORIAL")
        {
            fade.StartChangeScene("Tutorial");
        }
        else if (SceneName == "RANKING")
        {
            fade.StartChangeScene("Ranking");
        }
        else if (SceneName == "DEMO")
        {
            fade.StartChangeScene("Demo");
        }
    }
}
