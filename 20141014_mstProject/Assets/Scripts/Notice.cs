﻿//---------------------------------------------------------------------------------------
// 
// 作成者：
// 最終更新：
//---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------
// ライブラリ宣言
//--------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

//==========================================================================
// Noticeクラス
//==========================================================================
public class Notice : MonoBehaviour {

    //----------------------------------
    // 定数
    //----------------------------------
    // 通知最大値
    private int NOTICE_MAX = 4;

    // 文字最大値
    private const int MOJI_MAX = 10;

    // 段落数
    private const int BACK_HEIGHT = 3;

    // 文字ずらす_X座標
    // 最大値文字 * 全角分 - 誤差
    private const float STRING_SLIDE_X = MOJI_MAX * 0.2f - 0.2f;

    // 文字ずらす_Y座標
    // 最大値文字 * 全角分 - 誤差
    private const float STRING_SLIDE_Y = BACK_HEIGHT * 0.2f + 0.2f;

    // 通知メッセージ初期値X
    private const float NOTICE_DEFALUT_X = 10010.0f;

    // 通知メッセージ初期値Y
    private const float NOTICE_DEFALUT_Y = 10003.5f;

    // 上方向への移動量の制限値
    private const float NOTICE_UPMOVE_LIMIT = 2.0f;

    // 左方向の座標制限値
    private const float NOTICE_LIFT_LIMIT = 10007.0f;

    // 上方向の座標制限値
    private const float NOTICE_UP_LIMIT = 10005.5f;

    // 上方向の移動速度
    private const float NOTICE_UP_MOVE_SPEED = 0.5f;

    // 左方向の移動速度
    private const float NOTICE_LIFT_MOVE_SPEED = 0.5f;

    // 全体破棄の待ち時間
    private const float ALL_DESTORY_TIME = 4.0f;

    //----------------------------------
    // 構造体
    //----------------------------------
    struct NoticeData
    {
        public GameObject NoticeTexture;    // 背景用
        public GameObject NoticeChar;       // 文字
        public bool UpMoveFlag;             // 上方向に移動するか確認
        public float UpMove;                // 移動した量
    }

    //----------------------------------
    // 変数
    //----------------------------------
    // オブジェクト関連
    private NoticeData[] NoticeObj;
    private GameObject CameraObj;

    // プレハブ
    public GameObject PopUpTexture;
    public GameObject PopUpChar;

    // 生成した通知数
    private int m_NoticeCount = 0;

    // 残留時間
    private float m_Timer = 0.0f;

    // 破棄フラグ
    private bool m_bAllDestoryFlag = false;

    //=====================================
	// 初期化
    //=====================================
	void Start () 
    {
        // 生成された通知数のカウント
        m_NoticeCount = 0;

        // 残留時間初期化
        m_Timer = 0;

        //
        m_bAllDestoryFlag = false;

        // 生成
        NoticeObj = new NoticeData[NOTICE_MAX];

        // 上方向へ移動する際に使用する変数を初期化
        for (int nCnt = 0; nCnt < NOTICE_MAX; nCnt++)
        {
            NoticeObj[nCnt].UpMove = 0;
            NoticeObj[nCnt].UpMoveFlag = false;
        }
	}

    //=====================================
	// 更新
    //=====================================
	void Update () 
    {
        // デバッグ用関数
        NoticeDebug();

        if (m_bAllDestoryFlag == true)
        {
            // 全て破棄
            NoticeAllDestory();
        }
        else
        {
            // 通知アニメーション
            NoticeAnimation();
        }
	}

    //=====================================
    // 通知生成
    //=====================================
    void NoticeCreate(string NoticeMessage)
    {
        // 生成されていないものを探索
        for (int nCnt = 0; nCnt < NOTICE_MAX; nCnt++)
        {
            // どちらも生成されていなければ処理
            if (NoticeObj[nCnt].NoticeTexture == null &&
                NoticeObj[nCnt].NoticeChar == null)
            {
                // 生成
                NoticeObj[nCnt].NoticeTexture = (GameObject)Instantiate(PopUpTexture, new Vector3(NOTICE_DEFALUT_X, NOTICE_DEFALUT_Y, 1.0f), new Quaternion(0.0f, 0.0f, 0.0f, 1.0f));
                NoticeObj[nCnt].NoticeChar = (GameObject)Instantiate(PopUpChar);

                // 親代入
                NoticeObj[nCnt].NoticeTexture.transform.parent = this.transform.parent;
                NoticeObj[nCnt].NoticeChar.transform.parent = this.transform.parent;

                // 座標設定
                Vector3 pos, pos2;

                // 背景の座標
                pos = NoticeObj[nCnt].NoticeTexture.transform.position;

                // 通知が最大値まで生成されていなければ間隔を空ける
                if (m_NoticeCount < (NOTICE_MAX - 1))
                {
                    pos.y -= 2.0f * m_NoticeCount;
                    m_NoticeCount++;
                }
                // 通知が最大数までいっていれば一番下の方に座標を設定
                else if (m_NoticeCount >= (NOTICE_MAX - 1))
                {
                    pos.y -= 2.0f * (m_NoticeCount - 1);

                    // 古い通知を上方に移動する処理するためにフラグを立てる
                    for (int nCount = 0; nCount < NOTICE_MAX; nCount++)
                    {
                        // 新しく生成したものでなければ
                        if (nCount != nCnt)
                        {
                            NoticeObj[nCount].UpMoveFlag = true;
                        }
                    }
                }

                NoticeObj[nCnt].NoticeTexture.transform.position = pos;

                // 文字の座標
                pos2 = NoticeObj[nCnt].NoticeTexture.transform.position;

                pos2.x -= STRING_SLIDE_X;
                pos2.y += STRING_SLIDE_Y;
                pos2.z = 0.0f;

                NoticeObj[nCnt].NoticeChar.transform.position = pos2;

                // 通知内容設定
                // 3DTextの文字変更用
                TextMesh Message = (TextMesh)NoticeObj[nCnt].NoticeChar.GetComponent(typeof(TextMesh));

                // 文字代入
                Message.text = NoticeMessage + "\n" + "が売れました!";

                m_Timer = 0;
                m_bAllDestoryFlag = false;

                break;
            }
        }
    }

    //=====================================
    // 通知を破棄
    //=====================================
    void NoticeDestory(int nNumber)
    {
        // 破棄処理
        Destroy(NoticeObj[nNumber].NoticeTexture);
        Destroy(NoticeObj[nNumber].NoticeChar);

        // null初期化
        NoticeObj[nNumber].NoticeTexture = null;
        NoticeObj[nNumber].NoticeChar = null;

        // 上方向関連の変数初期化
        NoticeObj[nNumber].UpMove = 0;
        NoticeObj[nNumber].UpMoveFlag = false;
    }


    //=====================================
    // 全通知の破棄確認、破棄処理
    //=====================================
    void NoticeAllDestory()
    {
        int nDestoryCount = 0;

        for (int nCnt = 0; nCnt < NOTICE_MAX; nCnt++)
        {
            if (NoticeObj[nCnt].NoticeTexture != null &&
                NoticeObj[nCnt].NoticeChar != null)
            {
                // 上方への移動処理
                NoticeUpMove(nCnt, 0.2f);

                // 上方向の画面外に流れたら破棄
                if (NoticeObj[nCnt].NoticeTexture.transform.position.y >= NOTICE_UP_LIMIT)
                {
                    NoticeDestory(nCnt);
                }
            }
            else
            {
                nDestoryCount++;
            }
        }

        if (nDestoryCount >= NOTICE_MAX)
        {
            m_bAllDestoryFlag = false;
            m_Timer = 0;
            m_NoticeCount = 0;
        }
    }

    //=====================================
    // 通知アニメーション
    //=====================================
    void NoticeAnimation()
    {
        for (int nCnt = 0; nCnt < NOTICE_MAX; nCnt++)
        {
            // 生成されていれば処理
            if (NoticeObj[nCnt].NoticeTexture != null &&
                NoticeObj[nCnt].NoticeChar != null)
            {
                // 左にスライド
                if (NoticeObj[nCnt].NoticeTexture.transform.position.x > NOTICE_LIFT_LIMIT)
                {
                    // 左方向への移動処理
                    NoticeLeftMove(nCnt, NOTICE_LIFT_MOVE_SPEED);
                }
                // 上に流れる
                else if (NoticeObj[nCnt].UpMove < 2.0f && NoticeObj[nCnt].UpMoveFlag == true)
                {
                    // 上方への移動処理
                    NoticeUpMove(nCnt, NOTICE_UP_MOVE_SPEED);

                    // 上方向の画面外に流れたら破棄
                    if (NoticeObj[nCnt].NoticeTexture.transform.position.y >= NOTICE_UP_LIMIT)
                    {
                        // 破棄処理
                        NoticeDestory(nCnt);
                    }
                    // 一定の移動量になったら上方向への移動処理をやめる 
                    else if (NoticeObj[nCnt].UpMove == NOTICE_UPMOVE_LIMIT)
                    {
                        NoticeObj[nCnt].UpMove = 0;
                        NoticeObj[nCnt].UpMoveFlag = false;
                    }
                }
                else if (m_Timer <= ALL_DESTORY_TIME)
                {
                    m_Timer += Time.deltaTime;

                    if (m_Timer >= ALL_DESTORY_TIME)
                    {
                        m_bAllDestoryFlag = true;
                    }
                }
            }
        }
    }

    //=====================================
    // 通知左方向移動処理
    // 引数：オブジェクト番号、移動速度
    // 戻り値：なし
    //=====================================
    void NoticeLeftMove(int nNumber, float fMoveSpeed)
    {
        Vector3 pos, pos2;

        // 背景の座標
        pos2 = NoticeObj[nNumber].NoticeTexture.transform.position;

        pos2.x -= fMoveSpeed;

        NoticeObj[nNumber].NoticeTexture.transform.position = pos2;

        // 文字の座標
        pos = NoticeObj[nNumber].NoticeTexture.transform.position;

        pos.x -= STRING_SLIDE_X;
        pos.y += STRING_SLIDE_Y;
        pos.z = 0.0f;

        NoticeObj[nNumber].NoticeChar.transform.position = pos;
    }

    //=====================================
    // 通知上方向移動処理
    // 引数：オブジェクト番号、移動速度
    // 戻り値：なし
    //=====================================
    void NoticeUpMove(int nNumber, float fMoveSpeed)
    {
        Vector3 pos, pos2;

        // 背景の座標
        pos2 = NoticeObj[nNumber].NoticeTexture.transform.position;

        NoticeObj[nNumber].UpMove += fMoveSpeed;
        pos2.y += fMoveSpeed;

        NoticeObj[nNumber].NoticeTexture.transform.position = pos2;

        pos = NoticeObj[nNumber].NoticeTexture.transform.position;

        pos.x -= STRING_SLIDE_X;
        pos.y += STRING_SLIDE_Y;
        pos.z = 0.0f;

        NoticeObj[nNumber].NoticeChar.transform.position = pos;
    }

    //=====================================
    // デバッグ
    //=====================================
    void NoticeDebug()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            NoticeCreate("東京タワー");
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            NoticeCreate("テスト");
        }
    }
}
