//---------------------------------------------------------------------------------------
// 
// 作成者：
// 最終更新：
//---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------
// ライブラリ宣言
//--------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

//==========================================================================
// StageSelectControllクラス
//==========================================================================
public class StageSelectControll : MonoBehaviour 
{
    //----------------------------------
    // 定数定義
    //----------------------------------
    enum SELECT_STAGE              // 選択ステージ
    {
        STAGE_000 = 0,
        STAGE_001,
        STAGE_002,
        STAGE_MAX
    }
    enum STAGESELECT_STATE          // ステージセレクトシーンの状態
    {
        STATE_CANCEL = 0,
        STATE_SELECT,
        STATE_GO,
        STATE_MAX
    }
    const float REACTION_AXIS = 0.5f;   // 選択時に反応する傾き度合い

    //----------------------------------
    // メンバ変数
    //----------------------------------
    public FadeControll fade;       // フェード

    public GameObject m_stage000;   // ステージ０
    public GameObject m_stage001;   // ステージ１
    public GameObject m_stage002;   // ステージ２

    public GameObject m_cancel;     // 戻る
    public GameObject m_go;         // ＧＯ！

    public GameObject m_frame;      // 項目選択の枠

    public int m_nSelectStage;      // 選択中のステージ
    public int m_nState;            // ステージセレクトシーンの状態

    public Color m_frameColor;      // 枠の色情報

    public int m_nBlinkFlag;        // 枠点滅フラグ

    public GUIStyle style = new GUIStyle();

    public float m_fOldAxis;        // 傾きトリガー用

    // 枠移動のパターン
    public int m_nPattern;
    public float m_fDiffRate;
    public float m_fAddColor;

    public static int m_nBuff = 2;

    public static int GetBuff()
    {
        return m_nBuff;
    }
    //==================================
    // 初期化処理
    //==================================
    void Start()
    {
        // 選択ステージを初期化
        m_nSelectStage = (int)SELECT_STAGE.STAGE_000;
        // 状態の初期化
        m_nState = (int)STAGESELECT_STATE.STATE_SELECT;

        m_frameColor = new Color(1.0f, 0.0f, 0.0f, 1.0f);
        m_frame.renderer.material.color = m_frameColor;

        m_fOldAxis = 0.0f;

        m_nPattern = 0;
        m_fDiffRate = 0.5f;
        m_nBlinkFlag = 0;
        m_fAddColor = 0.01f;
    }

    //==================================
    // 更新処理
    //==================================
    void Update()
    {
        // 枠移動パターン
        if (Input.GetKeyDown("1"))
        {
            m_nPattern = 0;
        }
        if (Input.GetKeyDown("2"))
        {
            m_nPattern = 1;
        }
        // 枠移動の間隔を詰める係数
        Vector3 diff;
        // 係数の変更
        if (Input.GetKeyDown("3"))
        {
            m_fDiffRate -= 0.05f;
        }
        if (Input.GetKeyDown("4"))
        {
            m_fDiffRate += 0.05f;
        }
        // 色の加算値の変更
        if (Input.GetKeyDown("5"))
        {
            m_fAddColor -= 0.01f;
        }
        if (Input.GetKeyDown("6"))
        {
            m_fAddColor += 0.01f;
        }
        // 点滅フラグ
        if (m_nBlinkFlag == 0)
        {
            // 色をじわじわ変更
            m_frame.renderer.material.color += new Color(0.0f, m_fAddColor, m_fAddColor, 0.0f);
            // 白くなりきった場合
            if (1.0f <= m_frame.renderer.material.color.b)
            {
                // 適正な値にする
                m_frame.renderer.material.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
                // 点滅フラグ変更
                m_nBlinkFlag = 1;
            }
        }
        else if(m_nBlinkFlag == 1)
        {
            // 色をじわじわ変更
            m_frame.renderer.material.color -= new Color(0.0f, m_fAddColor, m_fAddColor, 0.0f);
            // 赤くなりきった場合
            if (m_frame.renderer.material.color.b <= 0.0f)
            {
                // 適正な値にする
                m_frame.renderer.material.color = new Color(1.0f, 0.0f, 0.0f, 1.0f);
                // 点滅フラグ変更
                m_nBlinkFlag = 0;
            }
        }

        if (m_nPattern == 0)
        {
            // ステージセレクトシーンの状態
            switch (m_nState)
            {
                case (int)STAGESELECT_STATE.STATE_CANCEL:

                    // 決定ボタンを押した場合
                    if (Input.GetKeyDown(KeyCode.Return) | Input.GetAxis("x") == 1)
                    {
                        // タイトルに戻る
                        fade.StartChangeScene("Title");
                    }
                    // 戻るボタンを押した場合
                    else if (Input.GetKeyDown(KeyCode.Backspace) | Input.GetAxis("△") == 1)
                    {
                        // 状態を変更
                        m_nState = (int)STAGESELECT_STATE.STATE_SELECT;
                    }
                    break;

                case (int)STAGESELECT_STATE.STATE_SELECT:

                    // 右キーを押した場合
                    if (Input.GetKeyDown("right") | (REACTION_AXIS < Input.GetAxis("axis3") & m_fOldAxis <= REACTION_AXIS))
                    {
                        // 選択ステージ変更
                        m_nSelectStage++;
                        if ((int)SELECT_STAGE.STAGE_MAX <= m_nSelectStage)
                        {
                            m_nSelectStage = (int)SELECT_STAGE.STAGE_MAX - 1;
                        }
                    }
                    // 左キーを押した場合
                    else if (Input.GetKeyDown("left") | (-REACTION_AXIS > Input.GetAxis("axis3") & m_fOldAxis >= -REACTION_AXIS))
                    {
                        // 選択ステージ変更
                        m_nSelectStage--;
                        if (m_nSelectStage <= -1)
                        {
                            m_nSelectStage = ((int)SELECT_STAGE.STAGE_000);
                        }
                    }

                    // 選んでいるステージに枠を移動
                    switch (m_nSelectStage)
                    {
                        case (int)SELECT_STAGE.STAGE_000:

                            m_frame.transform.position = m_stage000.transform.position;
                            m_frame.transform.localScale = m_stage000.transform.localScale;

                            break;

                        case (int)SELECT_STAGE.STAGE_001:

                            m_frame.transform.position = m_stage001.transform.position;
                            m_frame.transform.localScale = m_stage001.transform.localScale;

                            break;

                        case (int)SELECT_STAGE.STAGE_002:

                            m_frame.transform.position = m_stage002.transform.position;
                            m_frame.transform.localScale = m_stage002.transform.localScale;

                            break;

                        default:
                            break;
                    }

                    // 決定ボタンを押した場合
                    if (Input.GetKeyDown(KeyCode.Return) | Input.GetAxis("x") == 1)
                    {
                        // 状態を変更
                        m_nState = (int)STAGESELECT_STATE.STATE_GO;

                        // 枠を移動
                        m_frame.transform.position = m_go.transform.position;
                        // 枠の大きさを変更
                        m_frame.transform.localScale = new Vector3(1.0f, 0.5f, 1.0f);
                    }
                    // 戻るボタンを押した場合
                    else if (Input.GetKeyDown(KeyCode.Backspace) | Input.GetAxis("△") == 1)
                    {
                        // 状態を変更
                        m_nState = (int)STAGESELECT_STATE.STATE_CANCEL;

                        // 枠を移動
                        m_frame.transform.position = m_cancel.transform.position;
                        // 枠の大きさを変更
                        m_frame.transform.localScale = new Vector3(1.0f, 0.5f, 1.0f);
                    }

                    break;

                case (int)STAGESELECT_STATE.STATE_GO:

                    // 決定ボタンを押した場合
                    if (Input.GetKeyDown(KeyCode.Return) | Input.GetAxis("x") == 1)
                    {
                        switch (m_nSelectStage)
                        {
                            case (int)SELECT_STAGE.STAGE_000:
                            case (int)SELECT_STAGE.STAGE_001:
                            case (int)SELECT_STAGE.STAGE_002:
                                fade.StartChangeScene("Game");
                                break;
                            default:
                                break;
                        }
                    }
                    // 戻るボタンを押した場合
                    else if (Input.GetKeyDown(KeyCode.Backspace) | Input.GetAxis("△") == 1)
                    {
                        // 状態を変更
                        m_nState = (int)STAGESELECT_STATE.STATE_SELECT;
                    }
                    break;

                default:
                    break;
            }
        }
        else if (m_nPattern == 1)
        {
            // ステージセレクトシーンの状態
            switch (m_nState)
            {
                case (int)STAGESELECT_STATE.STATE_CANCEL:

                    // 決定ボタンを押した場合
                    if (Input.GetKeyDown(KeyCode.Return) | Input.GetAxis("x") == 1)
                    {
                        // タイトルに戻る
                        fade.StartChangeScene("Title");
                    }
                    // 戻るボタンを押した場合
                    else if (Input.GetKeyDown(KeyCode.Backspace) | Input.GetAxis("△") == 1)
                    {
                        // 状態を変更
                        m_nState = (int)STAGESELECT_STATE.STATE_SELECT;
                    }

                    // 枠をじわじわ寄せる
                    diff = m_cancel.transform.position - m_frame.transform.position;
                    diff *= m_fDiffRate;
                    m_frame.transform.position += diff;

                    break;

                case (int)STAGESELECT_STATE.STATE_SELECT:

                    // 右キーを押した場合
                    if (Input.GetKeyDown("right") | (REACTION_AXIS < Input.GetAxis("axis3") & m_fOldAxis <= REACTION_AXIS))
                    {
                        // 選択ステージ変更
                        m_nSelectStage++;
                        if ((int)SELECT_STAGE.STAGE_MAX <= m_nSelectStage)
                        {
                            m_nSelectStage = (int)SELECT_STAGE.STAGE_MAX - 1;
                        }
                    }
                    // 左キーを押した場合
                    else if (Input.GetKeyDown("left") | (-REACTION_AXIS > Input.GetAxis("axis3") & m_fOldAxis >= -REACTION_AXIS))
                    {
                        // 選択ステージ変更
                        m_nSelectStage--;
                        if (m_nSelectStage <= -1)
                        {
                            m_nSelectStage = ((int)SELECT_STAGE.STAGE_000);
                        }
                    }

                    // 選んでいるステージに枠を移動
                    switch (m_nSelectStage)
                    {
                        case (int)SELECT_STAGE.STAGE_000:

                            // 大きさを変更
                            m_frame.transform.localScale = m_stage000.transform.localScale;
                            // 枠をじわじわ寄せる
                            diff = m_stage000.transform.position - m_frame.transform.position;
                            diff *= m_fDiffRate;
                            m_frame.transform.position += diff;

                            break;

                        case (int)SELECT_STAGE.STAGE_001:

                            // 大きさを変更
                            m_frame.transform.localScale = m_stage001.transform.localScale;
                            // 枠をじわじわ寄せる
                            diff = m_stage001.transform.position - m_frame.transform.position;
                            diff *= m_fDiffRate;
                            m_frame.transform.position += diff;

                            break;

                        case (int)SELECT_STAGE.STAGE_002:

                            // 大きさを変更
                            m_frame.transform.localScale = m_stage002.transform.localScale;
                            // 枠をじわじわ寄せる
                            diff = m_stage002.transform.position - m_frame.transform.position;
                            diff *= m_fDiffRate;
                            m_frame.transform.position += diff;

                            break;

                        default:
                            break;
                    }

                    // 決定ボタンを押した場合
                    if (Input.GetKeyDown(KeyCode.Return) | Input.GetAxis("x") == 1)
                    {
                        // 状態を変更
                        m_nState = (int)STAGESELECT_STATE.STATE_GO;

                        // 枠の大きさを変更
                        m_frame.transform.localScale = new Vector3(1.0f, 0.5f, 1.0f);
                    }
                    // 戻るボタンを押した場合
                    else if (Input.GetKeyDown(KeyCode.Backspace) | Input.GetAxis("△") == 1)
                    {
                        // 状態を変更
                        m_nState = (int)STAGESELECT_STATE.STATE_CANCEL;

                        // 枠の大きさを変更
                        m_frame.transform.localScale = new Vector3(1.0f, 0.5f, 1.0f);
                    }

                    break;

                case (int)STAGESELECT_STATE.STATE_GO:

                    // 決定ボタンを押した場合
                    if (Input.GetKeyDown(KeyCode.Return) | Input.GetAxis("x") == 1)
                    {
                        switch (m_nSelectStage)
                        {
                            case (int)SELECT_STAGE.STAGE_000:
                            case (int)SELECT_STAGE.STAGE_001:
                            case (int)SELECT_STAGE.STAGE_002:
                                fade.StartChangeScene("Game");
                                break;
                            default:
                                break;
                        }
                    }
                    // 戻るボタンを押した場合
                    else if (Input.GetKeyDown(KeyCode.Backspace) | Input.GetAxis("△") == 1)
                    {
                        // 状態を変更
                        m_nState = (int)STAGESELECT_STATE.STATE_SELECT;
                    }

                    // 枠をじわじわ寄せる
                    diff = m_go.transform.position - m_frame.transform.position;
                    diff *= m_fDiffRate;
                    m_frame.transform.position += diff;

                    break;

                default:
                    break;
            }
        }

        // 今回の傾きを控える
        m_fOldAxis = Input.GetAxis("axis3");
	}

    //==================================
    // GUI描画
    //==================================
    void OnGUI()
    {
        style.normal.textColor = new Color(0.5f, 0.5f, 1.0f, 1.0f);
        style.fontSize = 10;
        // 選択しているステージを表示
        GUI.Label(new Rect(0, 0, 120, 10), "選択ステージ：" + m_nSelectStage, style);
        // デバッグ用
        GUI.Label(new Rect(0, 10, 200, 20), "枠移動の仕方(1,2で変更)：" + m_nPattern, style);
        GUI.Label(new Rect(0, 20, 200, 30), "枠移動の係数(3,4で変更)：" + m_fDiffRate, style);
        GUI.Label(new Rect(0, 30, 300, 40), "色の加算値　(5,6で変更)：" + m_fAddColor, style);
    }
}
