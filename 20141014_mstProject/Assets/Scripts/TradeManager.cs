﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TradeManager : MonoBehaviour {

	public enum State{
		Select = 0,		//	バイヤー選択
		Trade,			//	搬入
		TradeEnd,		//	搬入完了
		End,
	}

	private static int BuyerNum = 3;

	public GameControll game;
	public GameFade		tradeFade;			//	売買用フェード
	public ItemManager	itemManager;		//	アイテム管理
	public State state;
	public Buyer buyerBase;

	public GameObject buyerSelect;
	public GameObject tradeMain;
	public GameObject tradeResult;
	public TextMesh itemBase;				//	搬入商品のベース
	public GameObject talkBaloon;
	public TextMesh[] resultNumLabels;		//	アイテムの売上の数
	public TextMesh buyNumLabel;				//	売上
	public TextMesh saleNumLabel;				//
	public Town nowTown;					//	現在の町

	private List<TextMesh> itemLabelList = new List<TextMesh>();
	private int buyerIdx;		//	選択中のバイヤー
	private int selectBuyerId;	//	選択したバイヤーの番号
	private int endCount;
	private int saveSale;
	private float nowSale;
	private int[] saveBuys = new int[ItemManager.ItemMax];
	private float[] nowBuys = new float[ItemManager.ItemMax];	private int tradeEndState = 0;
	private Buyer[] buyerArray = new Buyer[ItemManager.ItemMax];
	private bool tradeFrag = false;

	// スプライト
	public Sprite[] iconSprite;
	public Sprite[] buyerSprite;

	void Start(){
		ChangeState(State.Select);
	}

	void Awake(){
		//	バイヤーを生成
		for(int i = 0; i < BuyerNum; i++){
			Vector3 basePos = buyerBase.transform.localPosition;
			buyerArray[i] = Instantiate(buyerBase) as Buyer;
			buyerArray[i].transform.parent = buyerSelect.transform;
			buyerArray[i].transform.localScale = Vector3.one;
			buyerArray[i].transform.localPosition = new Vector3(basePos.x + (i*4.5f), basePos.y, basePos.z);
			buyerArray[i].Init();
		}
		
		buyerBase.gameObject.SetActive(false);
	}

	// Update is called once per frame
	void Update () {
		//	バイヤー選択
		if(state == State.Select){
			SelectUpdate();
		}
		//	売買
		else if(state == State.Trade){
			TradeUpdate();
		}
		//	売買結果
		else if(state == State.TradeEnd){
			TradeEndUpdate();
		}
		//	演出ステート
		else if(state == State.End){
			EndUpdate();
		}

		endCount ++;
	}

	//	アクティブになった時
	void OnEnable(){
		//	ステートの初期化
		ChangeState(State.Select);
		buyerIdx = 0;
		endCount = 0;

		buyNumLabel.text = "$0";
		saleNumLabel.text = "$0";
		nowSale = 0;
		saveSale = 0;
		tradeFrag = false;

		//	結果の初期化
		for(int i = 0; i < resultNumLabels.Length; i++){
			resultNumLabels[i].text = "x 0";
			saveBuys[i] = 0;
			nowBuys[i] = 0;
		}

		//	かぶらないようにする処理
		for(int i = 0; i < BuyerNum; i++){
			int cnt = 0;
			Buyer.Type type = (Buyer.Type)Random.Range((float)Buyer.Type.Buyer00, (float)Buyer.Type.MAX);
			
			while(cnt < i){
				if(buyerArray[cnt].type != type){
					cnt ++;
					continue;
				}
				
				type = (Buyer.Type)Random.Range((float)Buyer.Type.Buyer00, (float)Buyer.Type.MAX);
				cnt = 0;
			}

			buyerArray[i].gameObject.SetActive(true);
			buyerArray[i].Reset(type);
		}
	}
	
	private void SelectUpdate(){
		//	キー入力
		if(Input.GetKeyDown(KeyCode.LeftArrow)){
			buyerIdx = (buyerIdx - 1 + BuyerNum) % BuyerNum;
		}
		if(Input.GetKeyDown(KeyCode.RightArrow)){
			buyerIdx = (buyerIdx + 1) % BuyerNum;
		}
		
		if(Input.GetKeyDown(KeyCode.Return)){
			selectBuyerId = (int)buyerArray[buyerIdx].type;
			ChangeState(State.Trade);
		}

		//	バイヤーの更新
		for(int i = 0; i < BuyerNum; i++){
			if(buyerIdx == i){
				buyerArray[i].sprite.color = new Color(1.0f, 0.0f, 0.0f, 1.0f);
			}
			else{
				buyerArray[i].sprite.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
			}
		}
	}

	private void TradeUpdate(){
		if(!tradeFrag){
			int saleMoney = 0;
			for(int i = 0; i < ItemManager.ItemMax; i++){

				int level = Buyer.saleRank[selectBuyerId][i];				// バイヤーのそのアイテムのレベル
				int salNum = 100 * level * Buyer.Status[selectBuyerId][0];	// 売る数(基本個数＊得意な販売物＊販売係数)
				

				while((salNum > 0) && (itemManager.itemNumArray[i] > 0)){
					itemManager.itemNumArray[i] --;
					salNum --;


					int money = 100;
					ResultData.sales += money;
					ScoreData.money += 	money;
					saleMoney += 		money;
				}
			}
			//	値の保存
			saveSale = saleMoney;

			//	バイヤーから買い取る処理
			for(int i = 0; i < ItemManager.ItemMax; i++){
				//	買う個数(基本個数＊得意な商品＊購入係数)
				int buyNum = 100 * Buyer.saleRank[selectBuyerId][i] * Buyer.Status[selectBuyerId][1];

				//	そのバイヤーが売るアイテムを買い取る
				itemManager.itemNumArray[i] += buyNum;
				ResultData.pay += buyNum * 100;
				ScoreData.money -= buyNum * 100;
				//	値の保存
				saveBuys[i] = buyNum;
			}

			tradeFrag = true;
		}

		if(endCount % 30 == 0){
			talkBaloon.transform.localScale = new Vector3(talkBaloon.transform.localScale.x * -1.0f, 1.0f, 1.0f);
		}
		if(endCount >= 180){
			ChangeState(State.TradeEnd);
		}
	}

	private void TradeEndUpdate(){
		if(tradeEndState == 0){
			nowSale += (float)saveSale / 60.0f;
			saleNumLabel.text = Common.WithComma((int)nowSale);

			if(endCount >= 60){
				tradeEndState = 1;
				saleNumLabel.text = Common.WithComma(saveSale);
			}
		}
		else if(tradeEndState == 1){
			for(int i = 0; i < ItemManager.ItemMax; i++){
				if(saveBuys[i] > nowBuys[i]){
					nowBuys[i] += (float)saveBuys[i] / 60.0f;
					resultNumLabels[i].text = "x " + ((int)nowBuys[i]).ToString();
				}
			}
			if(endCount >= 120){
				for(int i = 0; i < ItemManager.ItemMax; i++){
					resultNumLabels[i].text = "x " + (saveBuys[i]).ToString();
					tradeEndState = 2;
				}
			}
		}
		else if(tradeEndState == 2){
			int payMoney = saveBuys[0] * 100 + saveBuys[1] * 100 + saveBuys[2] * 100;
			buyNumLabel.text = Common.WithComma(saveSale - payMoney);	//アンカー（宮）		
            if(endCount >= 180){
				ChangeState(State.End);
			}
		}
	}

	private void EndUpdate(){
		if(tradeFade.state == GameFade.State.In){
			game.ChangeGameScene(GameControll.Scene.Main);
		}
	}

	private void ChangeState(State s){
		state = s;
		endCount = 0;

		if(s == State.Select){
			itemLabelList.Clear();
			buyerSelect.SetActive(true);
			tradeMain.SetActive(false);
			tradeResult.SetActive(false);
		}
		else if(s == State.Trade){
			buyerSelect.SetActive(false);
			tradeMain.SetActive(true);
			tradeResult.SetActive(false);
		}
		else if(s == State.TradeEnd){
			tradeEndState = 0;
			buyerSelect.SetActive(false);
			tradeMain.SetActive(false);
			tradeResult.SetActive(true);
		}
		else if(s == State.End){
			tradeFade.SetFade();

			//	アイテムラベルリストを破棄する
			TextMesh[] array = itemLabelList.ToArray();
			for(int i = 0; i < array.Length; i++){
				Destroy(array[i].gameObject);
			}
			itemLabelList.Clear();
			tradeResult.SetActive(false);
		}
	}
}
