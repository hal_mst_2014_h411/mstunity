﻿using UnityEngine;
using System.Collections;

public class BuyerItemRank : MonoBehaviour {

	static public int StarMax = 3;

	public TradeManager tradeManager;
	public SpriteRenderer itemIcon;
	public SpriteRenderer[] ranks;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Reset(int type, int itemRank){
		//	星の表示
		for(int i = 0; i < StarMax; i++){
			if(itemRank >= i){
				ranks[i].gameObject.SetActive(true);
			}
			else{
				ranks[i].gameObject.SetActive(false);
			}
		}

		//	アイテムの種類変更
		itemIcon.sprite = tradeManager.iconSprite[type];
	}
}
