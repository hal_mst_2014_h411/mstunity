﻿using UnityEngine;
using System.Collections;

public class Gauge : MonoBehaviour {
    //定数
    public const float MAX_GAGE = 100; //ゲージの最高値

    //変数
    private float m_fGauge;         //現在のフラグ
    private float m_fUseGauge;      //ゲージを使用している時の実際のゲージ値
    private float m_fNowGauge;      //ゲージをなめらかに増やすための仮のゲージ値
    private bool m_bUse;            //ゲージ使用中か否か
    private float m_fInitScale;     //スケールの初期の大きさ
    public SpriteRenderer m_srWaku; //枠のスプライト
    public GameObject m_goMAX;      //「MAX」文字
    private Vector3 m_v3BuffColor;
    private int m_nCount;           //カウント

	// Use this for initialization
	void Start () {
        m_fGauge = 0;
        m_bUse = false;
        m_fInitScale = transform.localScale.x;
        transform.localScale = new Vector3(m_fInitScale * (m_fGauge / MAX_GAGE),
                                            transform.localScale.y,
                                            transform.localScale.z);
        //m_v3BuffColor = new Vector3(1.0f,1.0f,1.0f);
        m_nCount = 0;
        m_goMAX.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        if (m_bUse)
        {
            m_fUseGauge -= 1.0f;
            m_srWaku.color = new Color(m_v3BuffColor.x, m_v3BuffColor.y, m_v3BuffColor.z, 1.0f);
            if (m_fUseGauge < 0.0f)
            {
                m_fGauge = 0.0f;
                m_bUse = false;
                m_v3BuffColor = new Vector3(1.0f, 1.0f, 1.0f);
                //m_srWaku.color = new Color(m_v3BuffColor.x, m_v3BuffColor.y, m_v3BuffColor.z, 1.0f);
                m_srWaku.color = new Color(1, 1, 1, 1.0f);
                m_goMAX.SetActive(false);
            }
            /*{
                float buff = 0.01f;

                if (buff > 0)
                {
                    m_v3BuffColor.x -= buff;
                    if (m_v3BuffColor.x < 0)
                    {
                        buff = -m_v3BuffColor.x;
                        m_v3BuffColor.x = 0.0f;
                    }
                    else
                    {
                        buff = 0;
                    }
                }
                if (buff > 0)
                {
                    m_v3BuffColor.y -= buff;
                    if (m_v3BuffColor.y < 0)
                    {
                        buff = -m_v3BuffColor.y;
                        m_v3BuffColor.y = 0.0f;
                    }
                    else
                    {
                        buff = 0;
                    }
                }
                if (buff > 0)
                {
                    m_v3BuffColor.z -= buff;
                    if (m_v3BuffColor.z < 0)
                    {
                        buff = -m_v3BuffColor.z;
                        m_v3BuffColor.z = 0.0f;
                    }
                    else
                    {
                        buff = 0;
                    }
                }
            }*/
            { 
                switch(m_nCount%5)
                {
                    case 0:
                        m_srWaku.color = Color.white;
                        break;
                    case 1:
                        m_srWaku.color = Color.green;
                        break;
                    case 2:
                        m_srWaku.color = Color.red;
                        break;
                    case 3:
                        m_srWaku.color = Color.blue;
                        break;
                    case 4:
                        m_srWaku.color = Color.black;
                        break;
                }
                m_nCount++;
            }

        }

        //ゲージ緩やかに増える処理
        if (m_fGauge - m_fNowGauge > 0.5f)
        {
            m_fNowGauge += 0.5f;
            transform.localScale = new Vector3(m_fInitScale * (m_fNowGauge / MAX_GAGE),
                                                transform.localScale.y,
                                                transform.localScale.z);
        }
        else
        {
            transform.localScale = new Vector3(m_fInitScale * (m_fGauge / MAX_GAGE),
                                                transform.localScale.y,
                                                transform.localScale.z);
            m_fNowGauge = m_fGauge;
        }
	}

    ///////////////////////////
    //
    /// <summary>
    /// 現在のゲージの値を返す
    /// </summary>
    /// <returns>現在のゲージ</returns>
    //////////////////////////////
    public float GetGauge()
    {
        return m_fGauge;
    }

    ///////////////////////////////
    /// <summary>
    /// ゲージの値をセットする
    /// </summary>
    /// <param name="fGauge">セットするゲージ値</param>
    /// /////////////////////////
    public void SetGauge(float fGauge)
    {
        m_fGauge = fGauge;
    }

    /// <summary>
    /// ゲージ値を増やす
    /// </summary>
    /// <param name="fAdd">増やす値</param>
    public void AddGauge(float fAdd)
    {
        //ゲージ使用を使用していなければ・・・
        if (!m_bUse)
        {
            //増やします
            m_fGauge += fAdd;
            if (m_fGauge > MAX_GAGE)
            {
                m_fGauge = MAX_GAGE;
                m_goMAX.SetActive(true);
            }
            if (m_fGauge < 0.0f)
            {
                m_fGauge = 0.0f;
            }
        }
    }

    /// <summary>
    /// 現在ゲージを使用しているかどうか
    /// </summary>
    /// <returns>ゲージ使用フラグ</returns>
    public bool GetUseFlag()
    {
        return m_bUse;
    }

    /// <summary>
    /// フラグをセットする
    /// </summary>
    /// <param name="bFlag">ゲージを使う(true)か使わないか(false)</param>
    public void SetUseFlag(bool bFlag)
    {
        m_bUse = bFlag;
        if (bFlag)
        {
            m_fUseGauge = m_fGauge;
            m_nCount = 0;
        }
    }
}
