using UnityEngine;
using System.Collections;

public class BalloonManager : MonoBehaviour
{
    //----------------------------------
    // クラス定義
    //----------------------------------
    public class VillageInfo
    {
        public string m_name;              // 村の名前
        public int m_nPopulation;          // 人口
        public int m_nAveAnnualIncome;     // 平均年収
        public Vector3 m_pos;              // 座標
    }
    //----------------------------------
    // 定数定義
    //----------------------------------
    const float SCREEN_BASE_WIDTH = 1920.0f;
    const float SCREEN_BASE_HEIGHT = 1080.0f;
    const int NUM_VILLAGE = 5;              // 村数
    //----------------------------------
    // メンバ変数
    //----------------------------------
    public bool m_debugFlag;
    public GameObject m_balloon;        // フキダシ
    // フキダシの座標指定に使用
    public GameObject m_mainCamera;
    public Vector3 m_screenPoint;
    // 文字の表現に使用
    public GUIStyle style;
    // フキダシ描画フラグ
    public bool m_drawBalloon;
    // 表示する情報の添え字
    public int m_idxInfo;

    // 村情報
    VillageInfo[] m_info = new VillageInfo[NUM_VILLAGE];

    public Vector2 m_balloonSize;
    public int m_fontSize;
    public Vector2 m_fontPosOffset;
    public float m_balloonDrawRange;
    public float m_fontHeightInterval;

    public GUIStyle debugStyle;

    public float m_addScaleBalloonRate;
    public float m_addAlphaBalloonRate;
    public float m_addAlphaStringRate;

    public int m_alphaTimer;
    public int m_timeLag;
    public Color[] fontColor = new Color[3];

    public float m_cityBalloonPaddingY;

    //==================================
    // 初期化処理
    //==================================
    void Start()
    {
        m_drawBalloon = false;
        m_idxInfo = 0;

        for (int nCnt = 0; nCnt < NUM_VILLAGE; nCnt++)
        {
            m_info[nCnt] = new VillageInfo();
        }
        // 村情報格納
        m_info[0].m_name = "日本";
        m_info[0].m_nPopulation = 127080000;
        m_info[0].m_nAveAnnualIncome = 5500000;
        m_info[0].m_pos = new Vector3(158, 10.0f, 158.0f);

        m_info[1].m_name = "シンガポール";
        m_info[1].m_nPopulation = 3102000;
        m_info[1].m_nAveAnnualIncome = 7000000;
        m_info[1].m_pos = new Vector3(-33.0f, 10.0f, -54.0f);

        m_info[2].m_name = "中国";
        m_info[2].m_nPopulation = 1357000000;
        m_info[2].m_nAveAnnualIncome = 1270000;
        m_info[2].m_pos = new Vector3(-41.0f, 10.0f, 145.0f);

        m_info[3].m_name = "オーストラリア";
        m_info[3].m_nPopulation = 23130000;
        m_info[3].m_nAveAnnualIncome = 5500000;
        m_info[3].m_pos = new Vector3(132.0f, 10.0f, -159.0f);

        m_info[4].m_name = "ドバイ";
        m_info[4].m_nPopulation = 2106000;
        m_info[4].m_nAveAnnualIncome = 40000000;
        m_info[4].m_pos = new Vector3(-239.0f, 10.0f, 96.0f);

        // もし村オブジェクトがあったら座標を取ってくる
        string[] name = { 
                            "japan",
                            "singapore",
                            "china",
                            "australia",
                            "dubai",
                        };
        GameObject ValueObject;
        for(int nCnt = 0; nCnt < NUM_VILLAGE; nCnt++)
        {
            ValueObject = GameObject.Find(name[nCnt]);
            if (ValueObject)
            {
                m_info[nCnt].m_pos = ValueObject.transform.position;
                m_info[nCnt].m_pos.y += m_cityBalloonPaddingY;  // ちょっと上にずらす
            }
        }
    }
    //==================================
    // 更新処理
    //==================================
    void Update()
    {
        // とびま～とと村の当たり判定
        Vector3 playerPos = new Vector3(0.0f, 0.0f, 0.0f);
        GameObject airShip = GameObject.Find("AirShip");
        // とびま～とが存在した場合
        if (airShip)
        {
            // とびま～との座標取得
            playerPos = airShip.transform.position;
        }
        for (int nCnt = 0; nCnt < NUM_VILLAGE; nCnt++)
        {
            // 村のワールド座標をスクリーン座標として取得
            m_screenPoint = m_mainCamera.camera.WorldToScreenPoint(m_info[nCnt].m_pos);
            // とびま～とと村が接近して
            if (IsHitCircle(new Vector2(playerPos.x, playerPos.z), 1.0f, new Vector2(m_info[nCnt].m_pos.x, m_info[nCnt].m_pos.z), m_balloonDrawRange))
            {
                // 村が画面裏に回ってない場合
                if (0.0f < m_screenPoint.z)
                {
                    m_drawBalloon = true;

                    // フキダシにスクリーン座標を反映
                    m_balloon.transform.position = new Vector3(m_screenPoint.x - Screen.width / 2, m_screenPoint.y - Screen.height / 2, 1.0f);
                    // スクリーンサイズに応じてフキダシ座標を修正
                    m_balloon.transform.position = new Vector3(
                        m_balloon.transform.position.x * (SCREEN_BASE_WIDTH / Screen.width),
                        m_balloon.transform.position.y * (SCREEN_BASE_HEIGHT / Screen.height), 1);

                    m_idxInfo = nCnt;

                    break;
                }
            }
            else
            {
                m_drawBalloon = false;
            }
        }

        if (m_debugFlag)
        {
            if (Input.GetKey("i"))
            {
                m_fontPosOffset.y += -1.0f;
            }
            if (Input.GetKey("k"))
            {
                m_fontPosOffset.y += 1.0f;
            }
            if (Input.GetKey("j"))
            {
                m_fontPosOffset.x += 1.0f;
            }
            if (Input.GetKey("l"))
            {
                m_fontPosOffset.x += -1.0f;
            }

            if (Input.GetKey("t"))
            {
                m_fontSize += 1;
            }
            if (Input.GetKey("g"))
            {
                m_fontSize += -1;
            }

            if (Input.GetKey("y"))
            {
                m_balloonSize.y += 1.0f;
            }
            if (Input.GetKey("h"))
            {
                m_balloonSize.y += -1.0f;
            }
            if (Input.GetKey("b"))
            {
                m_balloonSize.x += -1.0f;
            }
            if (Input.GetKey("n"))
            {
                m_balloonSize.x += 1.0f;
            }

            if (Input.GetKey("r"))
            {
                m_balloonDrawRange += 1.0f;
            }
            if (Input.GetKey("f"))
            {
                m_balloonDrawRange += -1.0f;
            }

            if (Input.GetKey("e"))
            {
                m_fontHeightInterval += 1.0f;
            }
            if (Input.GetKey("d"))
            {
                m_fontHeightInterval += -1.0f;
            }
        }

        if (m_drawBalloon)
        {
            // 大きくする
            m_balloon.transform.localScale += new Vector3(
                (m_balloonSize.x - m_balloon.transform.localScale.x) * m_addScaleBalloonRate,
                (m_balloonSize.y - m_balloon.transform.localScale.y) * m_addScaleBalloonRate,
                1.0f);
            // 濃くする
            m_balloon.renderer.material.color += new Color(0.0f, 0.0f, 0.0f,
                (1.0f - m_balloon.renderer.material.color.a) * m_addAlphaBalloonRate);
        }
        else
        {
            // 小さくする
            m_balloon.transform.localScale += new Vector3(
                (0.0f - m_balloon.transform.localScale.x) * m_addScaleBalloonRate,
                (0.0f - m_balloon.transform.localScale.y) * m_addScaleBalloonRate,
                1.0f);
            // 薄くする
            m_balloon.renderer.material.color += new Color(0.0f, 0.0f, 0.0f,
                (0.0f - m_balloon.renderer.material.color.a) * m_addAlphaBalloonRate);
        }
    }
    //==================================
    // GUI描画
    //==================================
    void OnGUI()
    {
        // デバッグ表示
        if (m_debugFlag)
        {
            string[] debug = {
                                 "文字大きさ　　　　(t,g)：" + m_fontSize,
                                 "フキダシ大きさ　　(y,h,b,n)：" + m_balloonSize.x + "," + m_balloonSize.y,
                                 "文字位置オフセット(i,k,j,l)：" + m_fontPosOffset.x + "," + m_fontPosOffset.y,
                                 "表示範囲　　　　　(r,f)：" + m_balloonDrawRange,
                                 "文字間の高さ　　　(e,d)：" + m_fontHeightInterval,
                             };
            for (int nCnt = 0; nCnt < 5; nCnt++)
            {
                GUI.Label(new Rect(0, 60 + (15 * nCnt), 300, 75 + (15 * nCnt)), debug[nCnt], debugStyle);
            }
        }

        if (m_drawBalloon)
        {
            m_alphaTimer = (m_alphaTimer + 1) % 6000;

            // 文字を濃くする
            if (m_timeLag < m_alphaTimer)
            {
                fontColor[0] += new Color(0.0f, 0.0f, 0.0f, (1.0f - fontColor[0].a) * m_addAlphaStringRate);
                if ((m_timeLag * 2) < m_alphaTimer)
                {
                    fontColor[1] += new Color(0.0f, 0.0f, 0.0f, (1.0f - fontColor[1].a) * m_addAlphaStringRate);
                    if ((m_timeLag * 3) < m_alphaTimer)
                    {
                        fontColor[2] += new Color(0.0f, 0.0f, 0.0f, (1.0f - fontColor[2].a) * m_addAlphaStringRate);
                    }
                }
            }
        }
        else
        {
            m_alphaTimer = 0;
            // 文字を薄くする
            fontColor[0] += new Color(0.0f, 0.0f, 0.0f, (0.0f - fontColor[0].a) * m_addAlphaStringRate);
            fontColor[1] += new Color(0.0f, 0.0f, 0.0f, (0.0f - fontColor[1].a) * m_addAlphaStringRate);
            fontColor[2] += new Color(0.0f, 0.0f, 0.0f, (0.0f - fontColor[2].a) * m_addAlphaStringRate);
        }

        // 文字位置を画面サイズに合わせるのに使う比率
        float fResizeX = (Screen.width / SCREEN_BASE_WIDTH);
        float fResizeY = (Screen.height / SCREEN_BASE_HEIGHT);

        // 文字の大きさを決定
        style.fontSize = (int)(m_fontSize * fResizeY);

        // フキダシに表示する情報
        string[] cityInfo = { 
                            m_info[m_idxInfo].m_name,
                            "人口　　:" + string.Format("{0:#,#}", m_info[m_idxInfo].m_nPopulation),
                            "平均年収:" + string.Format("{0:#,#}", m_info[m_idxInfo].m_nAveAnnualIncome),
                            };

        // フキダシに文字を表示
        for (int nCnt = 0; nCnt < 3; nCnt++)
        {
            style.normal.textColor = fontColor[nCnt];

            GUI.Label(new Rect(m_screenPoint.x - (m_fontPosOffset.x * fResizeX),
                               Screen.height - m_screenPoint.y + ((m_fontPosOffset.y + (style.fontSize * nCnt) + (m_fontHeightInterval * nCnt)) * fResizeY),
                               m_screenPoint.x - ((m_fontPosOffset.x + 512) * fResizeX),
                               Screen.height - m_screenPoint.y + ((m_fontPosOffset.y + (style.fontSize * (nCnt + 1)) + (m_fontHeightInterval * nCnt)) * fResizeY)),
                               cityInfo[nCnt], style);
        }
    }
    //==================================
    // 円同士の衝突判定
    //==================================
    public bool IsHitCircle(Vector2 pos0, float r0, Vector2 pos1, float r1)
    {
        float intervalLength = Mathf.Sqrt((pos0.x - pos1.x) * (pos0.x - pos1.x)
                                        + (pos0.y - pos1.y) * (pos0.y - pos1.y));

        if (intervalLength < (r0 + r1))
        {
            return true;
        }

        return false;
    }
}
