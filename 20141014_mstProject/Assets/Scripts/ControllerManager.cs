﻿//---------------------------------------------------------------------------------------
// コントローラー関連の処理
// 作成者：kasai tatushi
// 最終更新：2014/12/06
// 補足：使い方は他のクラスでControllerManager.関数名という形でつかえます
//       enumはControllerManager.宣言名で呼べます
//---------------------------------------------------------------------------------------
//--------------------------------------------------------------------------
// ライブラリ宣言
//--------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

//==========================================================================
// ControllerManagerクラス
//==========================================================================
public class ControllerManager : MonoBehaviour {
    //----------------------------------
    // 定数
    //----------------------------------
    /* CheckAxis関数の引数で制限値を設定しているがのちに使うなら使用
    // 左右
    private const float AxisXLimit_Left = -0.03f;
    private const float AxisXLimit_Right = 0.03f;

    // 上下
    private const float AxisYLimit_Up = 0.03f;
    private const float AxisYLimit_Down = -0.03f;
    */

    //----------------------------------
    // 列挙体
    //----------------------------------
    public enum KEY : int
    {
        MARU = 0,
        BATU = 1,
        SANKAKU = 2,
        SIKAKU = 3
    };

    public enum AXIS_STICK : int
    {
        X = 0,
        Y = 1
    }

    //=====================================
	// 初期化
    //=====================================
	void Start () 
    {
	}

    //=====================================
	// 更新
    //=====================================
	void Update () 
    {
	}

    //=====================================
    // ボタン入力確認
    //=====================================
    public static bool CheckKey(KEY Key)
    {
        //--------------------------------
        // ボタン関連
        //--------------------------------
        // ○ボタン
        if (Key == KEY.MARU && Input.GetAxis("○") == 1)
        {
            return (true);
        }

        // ×ボタン
        if (Key == KEY.BATU && Input.GetAxis("x") == 1)
        {
            return (true);
        }

        // △ボタン
        if (Key == KEY.SANKAKU && Input.GetAxis("△") == 1)
        {
            return (true);
        }

        // □ボタン
        if (Key == KEY.SIKAKU && Input.GetAxis("□") == 1)
        {
            return (true);
        }

        return (false);
    }


    //=====================================
    // Over軸入力確認
    // 引数：軸、制限値
    // 戻り値：true：入力されている false：入力されていない
    //=====================================
    public static bool CheckOverAxis(AXIS_STICK AxisStick, float Limit)
    {
        if (AxisStick == AXIS_STICK.X && Input.GetAxis("stickX") >= Limit)
        {
            return (true);
        }

        if (AxisStick == AXIS_STICK.Y && Input.GetAxis("stickY") >= Limit)
        {
            return (true);
        }

        return (false);
    }

    //=====================================
    // Under軸入力確認
    // 引数：軸、制限値
    // 戻り値：true：入力されている false：入力されていない
    //=====================================
    public static bool CheckUnderAxis(AXIS_STICK AxisStick, float Limit)
    {
        if (AxisStick == AXIS_STICK.X && Input.GetAxis("stickX") <= -Limit)
        {
            return (true);
        }

        if (AxisStick == AXIS_STICK.Y && Input.GetAxis("stickY") <= -Limit)
        {
            return (true);
        }

        return (false);
    }

    //=====================================
    // 軸値取得
    // 引数：軸
    // 戻り値：軸値
    //=====================================
    public static float GetAxis(AXIS_STICK AxisStick)
    {
        if (AxisStick == AXIS_STICK.X)
        {
            return (Input.GetAxis("stickX"));
        }
        else if (AxisStick == AXIS_STICK.Y)
        {
            return (Input.GetAxis("stickY"));
        }

        // どれも当てはまらなければ0を返す
        return (0);
    }
    
}
